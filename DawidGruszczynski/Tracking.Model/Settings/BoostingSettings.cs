﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracking.Model.Settings
{
    public class BoostingSettings : ITrackerSettings
    {
        public int numClassifiers { get; set; }

        public float samplerOverlap { get; set; }

        public float samplerSearchFactor { get; set; }

        public int iterationInit { get; set; }

        public int featureSetNumFeatures { get; set; }
    }
}
