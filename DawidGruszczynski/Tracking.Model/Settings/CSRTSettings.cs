﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracking.Model.Settings
{
    public class CSRTSettings : ITrackerSettings
    {
        public bool useHog { get; set; }
        public bool useColorNames { get; set; }
        public bool useGray { get; set; }
        public bool useRgb { get; set; }
        public bool useChannelWeights { get; set; }
        public bool useSegmentation { get; set; }
        public string windowFunction { get; set; }
        public float kaiserAlpha { get; set; }
        public float chebAttenuation { get; set; }
        public float templateSize { get; set; }
        public float gslSigma { get; set; }
        public float hogOrientations { get; set; }
        public float hogClip { get; set; }
        public float padding { get; set; }
        public float filterLr { get; set; }
        public float weightsLr { get; set; }
        public int numHogChannelsUsed { get; set; }
        public int admmIterations { get; set; }
        public int histogramBins { get; set; }
        public float histogramLr { get; set; }
        public int backgroundRatio { get; set; }
        public int numberOfScales { get; set; }
        public float scaleSigmaFactor { get; set; }
        public float scaleModelMaxArea { get; set; }
        public float scaleLr { get; set; }
        public float scaleStep { get; set; }
    }
}
