﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracking.Model.Settings
{
    public class MOTldSettings : ITrackerSettings
    {
        public int InternalWidth { get; set; }
        public int InternalHeight { get; set; }
        public float SureThreshold { get; set; }
        public float UnsureThreshold { get; set; }

        public static MOTldSettings Default
        {
            get
            {
                var ret = new MOTldSettings();
                ret.InternalWidth = 640;
                ret.InternalHeight = 480;
                ret.SureThreshold = 0.7f;
                ret.UnsureThreshold = 0.35f;

                return ret;
            }
        }
    }
}
