﻿using Emgu.CV.Tracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Emgu.CV.FileStorage;

namespace Tracking.Model.Settings
{
    public class KCFSettings : ITrackerSettings
    {
        public float DetectThresh { get; set; }

        public float Sigma { get; set; }

        public float Lambda { get; set; }

        public float InterpFactor { get; set; }

        public float OutputSigmaFactor { get; set; }

        public float PcaLearningRate { get; set; }

        public bool Resize { get; set; }

        public bool SplitCoeff { get; set; }

        public bool WrapKernel { get; set; }

        public bool CompressFeature { get; set; }

        public int MaxPatchSize { get; set; }

        public int CompressedSize { get; set; }
        public TrackerKCF.Mode DescPca { get; set; }
        public TrackerKCF.Mode DescNpca { get; set; }
    }
}
