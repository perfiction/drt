﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracking.Model.Settings
{
    public class MILSettings : ITrackerSettings
    {
        public float SamplerInitInRadius { get; set; }

        public int SamplerInitMaxNegNum { get; set; }

        public float SamplerSearchWinSize { get; set; }

        public float SamplerTrackInRadius { get; set; }

        public int SamplerTrackMaxPosNum { get; set; }

        public int SamplerTrackMaxNegNum { get; set; }

        public int FeatureSetNumFeatures { get; set; }
    }
}
