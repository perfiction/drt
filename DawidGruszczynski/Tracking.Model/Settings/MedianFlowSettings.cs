﻿using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracking.Model.Settings
{
    public class MedianFlowSettings : ITrackerSettings
    {
        public int pointsInGrid { get; set; }
        public Size winSize { get; set; }
        public int maxLevel { get; set; }
        public MCvTermCriteria termCriteria { get; set; }
        public Size winSizeNCC { get; set; }
        public double maxMedianLengthOfDisplacementDifference { get; set; }
    }
}
