﻿using Emgu.CV;
using Emgu.CV.Tracking;
using Platform.Collections;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tracking.Model.Settings;

namespace Tracking.Model
{
    public interface IObjectModel
    {
    
        int InitFrameId { get; set; }
        string Name { get; }
        Tracker TrackerForward { get; set; }
        Tracker TrackerBackward { get; set; }
        Rectangle InitBoundingBox { get; set; }
        Rectangle CurrentBoundingBox { get; set; }
        Rectangle FeatureMatching { get; set; }
        ITrackerSettings TrackerSettings { get; set; }
        int CorrectRectangles { get; set; }
        int MaxTemplates { get; set; }
        CircularFifoBuffer<Mat> Templates { get; set; }
        Mat InitFrame { get; set; }
     
        Dictionary<long, Rectangle> BoundingBoxes { get; set; }
        void Restart();
        void ReinitTracker(Rectangle newInitBoundingBox, int initFrameId, Mat initFrame,bool forward);
    }
}
