﻿using Emgu.CV;
using Emgu.CV.Tracking;
using Platform.Collections;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tracking;
using Tracking.Model.Settings;

namespace Tracking.Model
{
    public class ObjectModel : IObjectModel
    {
        public string Name { get; private set; }
        public int InitFrameId { get; set; }
        public Tracker TrackerForward { get; set; }
        public Tracker TrackerBackward { get; set; }
        public int MaxTemplates { get; set; }
        public CircularFifoBuffer<Mat> Templates { get; set; }
        public Mat InitFrame { get; set; }
        public int CorrectRectangles { get; set; } = 1;
        public Rectangle InitBoundingBox { get; set; }
        public Rectangle CurrentBoundingBox { get; set; }
        public Rectangle FeatureMatching { get; set; } = new Rectangle();
        public Dictionary<long, Rectangle> BoundingBoxes { get; set; } = new Dictionary<long, Rectangle>();
        
        public ITrackerSettings TrackerSettings { get; set; }

        public void Restart()
        {
            CreateTracker(this.TrackerSettings, true, true);
            if(TrackerForward != null) TrackerForward.Init(InitFrame, InitBoundingBox);
            if(TrackerBackward != null) TrackerBackward.Init(InitFrame, InitBoundingBox);
            BoundingBoxes = new Dictionary<long, Rectangle>();
            FeatureMatching  = new Rectangle();
        }
        public ObjectModel(int frameId, Rectangle initBoundingBox, Mat template, Mat initFrame, string name, ITrackerSettings trackerSettings)
        {
            InitFrameId = frameId;
            InitBoundingBox = initBoundingBox;
            MaxTemplates = 16;
            Templates = new CircularFifoBuffer<Mat>(MaxTemplates);
            Templates.Add(template);
            InitFrame = initFrame;
            Name = name;
            TrackerSettings = trackerSettings;
            CreateTracker(trackerSettings, true, true);
        }
        public void ReinitTracker(Rectangle newInitBoundingBox, int initFrameId, Mat initFrame, bool forward)
        {
            CreateTracker(TrackerSettings,forward,!forward);

            //InitBoundingBox = newInitBoundingBox;
            //InitFrame = initFrame;
            if (forward)
                TrackerForward.Init(initFrame, newInitBoundingBox);
            else
                TrackerBackward.Init(initFrame, newInitBoundingBox);
        }
        private void CreateTracker(ITrackerSettings settings, bool forwrd, bool backward)
        {
            switch (settings)
            {
                case KCFSettings s:
                    {
                        if (forwrd)
                            TrackerForward = new TrackerKCF(s.DetectThresh, s.Sigma, s.Lambda, s.InterpFactor, s.OutputSigmaFactor, s.PcaLearningRate, s.Resize, s.SplitCoeff, s.WrapKernel, s.CompressFeature, s.MaxPatchSize, s.CompressedSize, s.DescPca, s.DescNpca);
                        if (backward)
                            TrackerBackward = new TrackerKCF(s.DetectThresh, s.Sigma, s.Lambda, s.InterpFactor, s.OutputSigmaFactor, s.PcaLearningRate, s.Resize, s.SplitCoeff, s.WrapKernel, s.CompressFeature, s.MaxPatchSize, s.CompressedSize, s.DescPca, s.DescNpca);
                    }
                    break;
                case BoostingSettings s:
                    {
                        if (forwrd)
                            TrackerForward = new TrackerBoosting(s.numClassifiers, s.samplerOverlap, s.samplerSearchFactor, s.iterationInit, s.featureSetNumFeatures);
                        if (backward)
                            TrackerBackward = new TrackerBoosting(s.numClassifiers, s.samplerOverlap, s.samplerSearchFactor, s.iterationInit, s.featureSetNumFeatures);
                    }
                    break;
                case GOTRUNSettings s:
                    {
                        if (forwrd)
                            TrackerForward = new TrackerGOTURN();
                        if (backward)
                            TrackerBackward = new TrackerGOTURN();
                    }
                    break;
                case MedianFlowSettings s:
                    {
                        if (forwrd)
                            TrackerForward = new TrackerMedianFlow(s.pointsInGrid, s.winSize, s.maxLevel, s.termCriteria, s.winSizeNCC, s.maxMedianLengthOfDisplacementDifference);
                        if (backward)
                            TrackerBackward = new TrackerMedianFlow(s.pointsInGrid, s.winSize, s.maxLevel, s.termCriteria, s.winSizeNCC, s.maxMedianLengthOfDisplacementDifference);
                    }
                    break;
                case MILSettings s:
                    {
                        if (forwrd)
                            TrackerForward = new TrackerMIL(s.SamplerInitInRadius, s.SamplerInitMaxNegNum, s.SamplerSearchWinSize, s.SamplerTrackInRadius, s.SamplerTrackMaxPosNum, s.SamplerTrackMaxNegNum, s.FeatureSetNumFeatures);
                        if (backward)
                            TrackerBackward = new TrackerMIL(s.SamplerInitInRadius, s.SamplerInitMaxNegNum, s.SamplerSearchWinSize, s.SamplerTrackInRadius, s.SamplerTrackMaxPosNum, s.SamplerTrackMaxNegNum, s.FeatureSetNumFeatures);
                    }
                    break;
                case MOSSESettings s:
                    {
                        if (forwrd)
                            TrackerForward = new TrackerMOSSE();
                        if (backward)
                            TrackerBackward = new TrackerMOSSE();
                    }
                    break;
                case TLDSettings s:
                    {
                        if (forwrd)
                            TrackerForward = new TrackerTLD();
                        if (backward)
                            TrackerBackward = new TrackerTLD();
                    }
                    break;
                case CSRTSettings s:
                    {
                        if (forwrd)
                            TrackerForward = new TrackerCSRT(s.useHog, s.useColorNames, s.useGray, s.useRgb, s.useChannelWeights, s.useSegmentation, s.windowFunction, s.kaiserAlpha, s.chebAttenuation, s.templateSize, s.gslSigma, s.hogOrientations, s.hogClip, s.padding, s.filterLr, s.weightsLr, s.numHogChannelsUsed, s.admmIterations, s.histogramBins, s.histogramLr, s.backgroundRatio, s.numberOfScales, s.scaleSigmaFactor, s.scaleModelMaxArea, s.scaleLr, s.scaleStep);
                        if (backward)
                            TrackerBackward = new TrackerCSRT(s.useHog, s.useColorNames, s.useGray, s.useRgb, s.useChannelWeights, s.useSegmentation, s.windowFunction, s.kaiserAlpha, s.chebAttenuation, s.templateSize, s.gslSigma, s.hogOrientations, s.hogClip, s.padding, s.filterLr, s.weightsLr, s.numHogChannelsUsed, s.admmIterations, s.histogramBins, s.histogramLr, s.backgroundRatio, s.numberOfScales, s.scaleSigmaFactor, s.scaleModelMaxArea, s.scaleLr, s.scaleStep);
                    }
                    break;
            }
        }

      
    }
}
