﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using Emgu.CV;
using TldSharpWrapper;
using Tracking.Helpers;
using Tracking.Model;
using Tracking.Model.Settings;
using Tracking.Services.Abstract;

namespace Tracking.Services.Concrete
{
    public class MOTldService : ITrackerService
    {
        public bool Allocated { get; set; } = false;

        private MOTldWrapper _tld;
        private Dictionary<string, int> _objectMap;
        private Dictionary<string, Rectangle> _initMotldRect;

        public MOTldService(IObjectDetectService objectDetectService) { }

        public void Init(Mat frame, ITrackerSettings settings)
        {
            if (_tld != null) _tld.Dispose();

            MOTldSettings s = (MOTldSettings)settings;
            _tld = new MOTldWrapper(s.InternalWidth, s.InternalHeight);
            _tld.UpdateTolerances(s.SureThreshold, s.UnsureThreshold);
            _objectMap = new Dictionary<string, int>();
            _initMotldRect = new Dictionary<string, Rectangle>();

            _tld.ProcessFrame(frame);
            Allocated = true;
        }

        public void Stop()
        {
            Allocated = false;
        }

        public void AddObject(Rectangle rectangle, string name)
        {
            int id = _tld.AddObject(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
            _objectMap.Add(name, id);
            _initMotldRect.Add(name, BoxToRect(_tld.GetObjectBox(id)));
        }

        public void FixObject(Rectangle rectangle, string name)
        {
            int id = _objectMap[name];
            _tld.FixObject(id, rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
        }

        public void UpdateRegionsForward(List<IObjectModel> objects, Mat m, int frame)
        {
            _tld.ProcessFrame(m);
            var newBoxes = _tld.GetObjectBoxes();

            Parallel.For(0, objects.Count, (i) =>
            {
                if (frame >= objects[i].InitFrameId)
                {
                    TldStatus status = _tld.GetStatus(_objectMap[objects[i].Name]);
                    Rectangle rect = new Rectangle(0, 0, 0, 0);
                    if (status != TldStatus.Lost)
                    {
                        objects[i].CorrectRectangles++;
                        ObjectBox box = newBoxes.Single(x => x.objectId == _objectMap[objects[i].Name]);
                        rect = new Rectangle(Convert.ToInt32(box.x), Convert.ToInt32(box.y), Convert.ToInt32(box.width), Convert.ToInt32(box.height));
                        rect = FixCoordinates(objects[i].Name, objects[i].InitBoundingBox, rect);
                    }
                    else objects[i].CorrectRectangles = 0;

                    objects[i].BoundingBoxes.AddSafe(frame, rect);
                    objects[i].CurrentBoundingBox = rect;
                }
            });
        }

        public void UpdateRegionsBackward(List<IObjectModel> objects, Mat m, int frame)
        {
            _tld.ProcessFrame(m);
            var newBoxes = _tld.GetObjectBoxes();

            Parallel.For(0, objects.Count, (i) =>
            {
                if (frame <= objects[i].InitFrameId)
                {
                    TldStatus status = _tld.GetStatus(_objectMap[objects[i].Name]);
                    Rectangle rect = new Rectangle(0, 0, 0, 0);
                    if (status != TldStatus.Lost)
                    {
                        objects[i].CorrectRectangles++;
                        ObjectBox box = newBoxes.Single(x => x.objectId == _objectMap[objects[i].Name]);
                        rect = new Rectangle(Convert.ToInt32(box.x), Convert.ToInt32(box.y), Convert.ToInt32(box.width), Convert.ToInt32(box.height));
                        rect = FixCoordinates(objects[i].Name, objects[i].InitBoundingBox, rect);
                    }
                    else objects[i].CorrectRectangles = 0;

                    objects[i].BoundingBoxes.AddSafe(frame, rect);
                    objects[i].CurrentBoundingBox = rect;
                }
            });
        }

        private static Rectangle BoxToRect(ObjectBox b)
        {
            return new Rectangle(Convert.ToInt32(b.x), Convert.ToInt32(b.y), Convert.ToInt32(b.width), Convert.ToInt32(b.height));
        }

        private Rectangle FixCoordinates(string name, Rectangle initial, Rectangle updated)
        {
            if (_initMotldRect.Count == 1) return updated;

            int x = initial.X + (_initMotldRect[name].X - updated.X);
            int y = initial.Y + (_initMotldRect[name].Y - updated.Y);
            int w = initial.Width + (_initMotldRect[name].Width - updated.Width);
            int h = initial.Height + (_initMotldRect[name].Height - updated.Height);

            return new Rectangle(x, y, w, h);
        }
    }
}
