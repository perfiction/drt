﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Emgu.CV;
using Tracking.Services.Abstract;
using Platform.Collections;
using Emgu.CV.CvEnum;

namespace Tracking.Services.Concrete
{
    public class TemplateDetectService : IObjectDetectService
    {
        public Rectangle FindObject(CircularFifoBuffer<Mat> models, Mat inputImage)
        {
            return FindTemplate(models, inputImage);
        }

        public Rectangle FindTemplate(CircularFifoBuffer<Mat> modelImages, Mat observedImage)
        {
            double threshold = 0.75;
            SortedDictionary<double, Rectangle> foundTemplates = new SortedDictionary<double, Rectangle>();

            foreach (var item in modelImages)
            {
                int w = observedImage.Width - item.Width + 1;
                int h = observedImage.Height - item.Height + 1;
                Mat result = new Mat(w, h, DepthType.Cv32F, 1);
                CvInvoke.MatchTemplate(observedImage, item, result, TemplateMatchingType.CcoeffNormed);
                result.MinMax(out double[] minValues, out double[] maxValues, out Point[] minLocations, out Point[] maxLocations);
                if (maxValues[0] >= threshold)
                    foundTemplates.Add(maxValues[0], new Rectangle(maxLocations[0], item.Size));
            }

            return foundTemplates.Count != 0 ? foundTemplates.Last().Value : new Rectangle(0, 0, 0, 0);
        }
    }
}
