﻿using Emgu.CV;
using Emgu.CV.Tracking;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tracking.Helpers;
using Tracking.Model;
using Tracking.Services.Abstract;
using Platform.Collections;

namespace Tracking.Services.Concrete
{
    public class TrackerService : ITrackerService
    {
        IObjectDetectService _objectDetectService;
        public float FeatureMatchingMaxDifference { get; set; } = 0.5f;
        public TrackerService(IObjectDetectService objectDetectService)
        {
            _objectDetectService = objectDetectService;
        }
        public void UpdateRegionsForward(List<IObjectModel> objects, Mat m, int frame)
        {
            Parallel.For(0, objects.Count, (i) =>
            {
                if (frame >= objects[i].InitFrameId)
                {
                    Rectangle rectangle = new Rectangle();
                    if (objects[i].TrackerForward.Update(m, out rectangle))
                    {
                        // (LB) Use rectangle intersection to prevent out of bounds exception
                        Rectangle roi = new Rectangle(rectangle.Location, rectangle.Size);
                        roi.Intersect(new Rectangle(0, 0, m.Width, m.Height));

                        Mat newTemplate = new Mat(m, roi);
                        objects[i].Templates.Add(newTemplate);
                    }
                    else if (_objectDetectService != null)
                    {
                        rectangle = _objectDetectService.FindObject(objects[i].Templates, m);
                        if (ComputeTruePositive(rectangle, objects[i].FeatureMatching) && rectangle.Height > 0 && rectangle.X >=0 && rectangle.Y >= 0)
                        {
                            objects[i].CorrectRectangles++;
                            objects[i].FeatureMatching = rectangle;
                            if (objects[i].CorrectRectangles == 2)
                                objects[i].ReinitTracker(rectangle, frame, m, true);
                        }
                        else
                        {
                            objects[i].FeatureMatching = rectangle;
                            rectangle = new Rectangle();
                            objects[i].CorrectRectangles = 1;
                        }
                        

                    }
                    objects[i].BoundingBoxes.AddSafe(frame, rectangle);
                    objects[i].CurrentBoundingBox = rectangle;
                   

                }
            });
        }

        public void UpdateRegionsBackward(List<IObjectModel> objects, Mat m, int frame)
        {
            Parallel.For(0, objects.Count, (i) =>
            {
                if (frame <= objects[i].InitFrameId)
                {
                    Rectangle rectangle = new Rectangle();
                    if (objects[i].TrackerBackward.Update(m, out rectangle))
                    {
                        // (LB) Use rectangle intersection to prevent out of bounds exception
                        Rectangle roi = new Rectangle(rectangle.Location, rectangle.Size);
                        roi.Intersect(new Rectangle(0, 0, m.Width, m.Height));

                        Mat newTemplate = new Mat(m, roi);
                        objects[i].Templates.Add(newTemplate);
                    }
                    else if(_objectDetectService != null)
                    {
                        rectangle = _objectDetectService.FindObject(objects[i].Templates, m);
                        if (ComputeCenterDistance(rectangle, objects[i].FeatureMatching) && rectangle.Height > 0 && rectangle.X >= 0 && rectangle.Y >= 0)
                        {
                            objects[i].CorrectRectangles++;
                            objects[i].FeatureMatching = rectangle;
                            if (objects[i].CorrectRectangles == 2)
                                objects[i].ReinitTracker(rectangle, frame, m, true);
                        }
                        else
                        {
                            objects[i].FeatureMatching = rectangle;
                            rectangle = new Rectangle();
                            objects[i].CorrectRectangles = 1;
                        }
                    }

                    objects[i].BoundingBoxes.AddSafe(frame, rectangle);
                    objects[i].CurrentBoundingBox = rectangle;

                }
            });
        }

        public bool  ComputeTruePositive(Rectangle r1, Rectangle r2)
        {
            RectangleF rect = RectangleF.Intersect(r1, r2);
            float intersection = rect.Width * rect.Height;

            float union = r1.Width * r1.Height + r2.Width * r2.Height - intersection;

            var value =  intersection / union;
            if (value >= FeatureMatchingMaxDifference)
                return true;
            else
                return false;
        }
        private bool ComputeCenterDistance(Rectangle r1, Rectangle r2)
        {
            PointF r1Center = new PointF(), r2Center = new PointF();

            r2Center.X = r2.X + (r2.Width / 2);
            r2Center.Y = r2.Y + (r2.Height / 2);

            r1Center.X = r1.X + (r1.Width / 2);
            r1Center.Y = r1.Y + (r1.Height / 2);


            var value = (float)Math.Sqrt(Math.Pow((r2Center.X - r1Center.X), 2) + Math.Pow((r2Center.Y - r1Center.Y), 2));
            if (value <= FeatureMatchingMaxDifference)
                return true;
            else
                return false;

        }


    }
}
