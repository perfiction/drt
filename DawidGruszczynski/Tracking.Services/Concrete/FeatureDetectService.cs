﻿using System;
using System.Drawing;
using System.Linq;
using Emgu.CV;
using Tracking.Services.Abstract;
using Platform.Collections;
using Emgu.CV.Util;
using Emgu.CV.Features2D;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

namespace Tracking.Services.Concrete
{
    public class FeatureDetectService : IObjectDetectService
    {
        public Rectangle FindObject(CircularFifoBuffer<Mat> models, Mat inputImage)
        {
            Mat homography;
            using (VectorOfVectorOfDMatch matches = new VectorOfVectorOfDMatch())
            {
                Rectangle result = new Rectangle(0, 0, 0, 0);
                FindMatch(models, inputImage, matches, out homography);

                if (homography != null)
                {
                    Rectangle rect = new Rectangle(Point.Empty, models[0].Size);
                    PointF[] pts = new PointF[]
                    {
                            new PointF(rect.Left, rect.Bottom),
                            new PointF(rect.Right, rect.Bottom),
                            new PointF(rect.Right, rect.Top),
                            new PointF(rect.Left, rect.Top)
                    };
                    pts = CvInvoke.PerspectiveTransform(pts, homography);

                    Point[] points = Array.ConvertAll<PointF, Point>(pts, Point.Round);
                    int minX = points.Min(x => x.X);
                    int minY = points.Min(x => x.Y);
                    int maxX = points.Max(x => x.X);
                    int maxY = points.Max(x => x.Y);
                    result = new Rectangle(minX, minY, maxX - minX, maxY - minY);

                }

                return result;
            }
        }

        public void FindMatch(CircularFifoBuffer<Mat> modelImages, Mat observedImage, VectorOfVectorOfDMatch matches, out Mat homography)
        {
            int k = 2;
            double uniquenessThreshold = 0.80;

            homography = null;
            VectorOfKeyPoint list = new VectorOfKeyPoint();
            Mat mask;
            VectorOfKeyPoint modelKeyPoints = new VectorOfKeyPoint();
            VectorOfKeyPoint observedKeyPoints = new VectorOfKeyPoint();
            BFMatcher matcher = new BFMatcher(DistanceType.Hamming);

            Brisk brisk = new Brisk();
            foreach (var item in modelImages)
            {
                Mat modelDescriptors = new Mat();
                modelKeyPoints = new VectorOfKeyPoint();

                brisk.DetectAndCompute(item, null, modelKeyPoints, modelDescriptors, false);
                if (modelKeyPoints.Size != 0)
                {
                    matcher.Add(modelDescriptors);
                    list.Push(modelKeyPoints);
                }
            }

            Mat observedDescriptors = new Mat();
            brisk.DetectAndCompute(observedImage, null, observedKeyPoints, observedDescriptors, false);

            matcher.KnnMatch(observedDescriptors, matches, k, null);

            mask = new Mat(matches.Size, 1, DepthType.Cv8U, 1);
            mask.SetTo(new MCvScalar(255));
            Features2DToolbox.VoteForUniqueness(matches, uniquenessThreshold, mask);

            int nonZeroCount = CvInvoke.CountNonZero(mask);
            if (nonZeroCount >= 4)
            {
                nonZeroCount = Features2DToolbox.VoteForSizeAndOrientation(list, observedKeyPoints,
                   matches, mask, 1.5, 20);
                if (nonZeroCount >= 4)
                    homography = Features2DToolbox.GetHomographyMatrixFromMatchedFeatures(list,
                       observedKeyPoints, matches, mask, 2);
            }
        }
    }
}