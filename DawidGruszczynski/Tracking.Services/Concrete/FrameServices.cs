﻿using Emgu.CV;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;

namespace Tracking.Services
{
    public class FrameServices : IFrameService
    {
        public string FileName { get; private set; }
        public Size Resolution { get; private set; }
        public double TimeInMs { get; private set; }

        VideoCapture _videoCaptureForward = null;
        VideoCapture _videoCaptureBackward = null;
        List<string> _imagesCapture;
        int _forwardIterator = 0;
        bool finish = false;

        int _buforSize = 50;
        bool finishForward = false;
        bool finishBackward = false;

        BackgroundWorker backgroundWorkerBackward;
        BackgroundWorker backgroundWorkerForward;
        ConcurrentQueue<Mat> _bufferForward;
        ConcurrentQueue<Mat> _bufferBackward;
        ManualResetEvent resetEventForward;
        ManualResetEvent resetEventBackward;

        public FrameServices()
        {


        }

        private void BackgroundWorkerBackward(object sender, DoWorkEventArgs e)
        {
            int iter = (int)e.Argument;
            int position;

            if (_videoCaptureBackward != null)
            {                
                while (true)
                {
                    int counter = _buforSize - _bufferBackward.Count();
                    Mat[] tmpList = new Mat[counter];
                    position = iter - counter;
                    if (position < 0)
                    {
                        position = 0;
                        counter = iter;
                        finishBackward = true;
                    }
                    _videoCaptureBackward.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.PosFrames, position);
                    while (counter > 0)
                    {             
                        Mat nextFrame = new Mat();
                        if (finish) return;
                        _videoCaptureBackward.Read(nextFrame);

                        if (nextFrame.Bitmap == null)
                        {
                            finishBackward = true;
                            break;
                        }

                        tmpList[counter-1] = nextFrame;
                        counter--;
                        iter--;
                    }
                    for (int i = 0; i < tmpList.Count(); i++)
                    {
                        if(tmpList[i] != null)
                        _bufferBackward.Enqueue(tmpList[i]);
                    }
                    if (finishBackward)
                        return;
                    if (_bufferBackward.Count < _buforSize)
                        continue;
                    resetEventBackward.Reset();
                    resetEventBackward.WaitOne();

                }
            }         
            else
            {
                
                while (true)
                {
                    while (_bufferBackward.Count < _buforSize - 1)
                    {
                        if (finish) return;
                        Mat nextFrame = GetFrame(iter);

                        if (nextFrame == null)
                        {
                            finishBackward = true;
                            return;
                        }
                        _bufferBackward.Enqueue(nextFrame);
                        iter--;

                    }
                    resetEventBackward.Reset();
                    resetEventBackward.WaitOne();
                }
            }


        }

        private void BackgroundWorkerForward(object sender, DoWorkEventArgs e)
        {

            _bufferForward.Enqueue(GetFrame((int)e.Argument));
            _forwardIterator = (int)e.Argument;
            while (true)
            {
                while (_bufferForward.Count < _buforSize - 1)
                {
                    if (finish) return;
                    Mat nextFrame = NextFrame();
                    if (nextFrame == null || nextFrame.Bitmap == null) //zalezy czy video czy obrazki
                    {
                        finishForward = true;
                        return;
                    }
                    _bufferForward.Enqueue(nextFrame);
                }
                resetEventForward.Reset();
                resetEventForward.WaitOne();
            }
        }

       
        public void StartBuffering(int frameForward, int frameBackward, int bufforsize = 10)
        {
            finish = false;
            finishForward = false;
            finishBackward = false;
            resetEventForward = new ManualResetEvent(true);
            resetEventBackward = new ManualResetEvent(true);
            backgroundWorkerBackward = new BackgroundWorker();
            backgroundWorkerForward = new BackgroundWorker();
            backgroundWorkerForward.WorkerSupportsCancellation = true;
            backgroundWorkerBackward.WorkerSupportsCancellation = true;
           
            backgroundWorkerForward.DoWork += BackgroundWorkerForward;
            backgroundWorkerBackward.DoWork += BackgroundWorkerBackward;
            _buforSize = bufforsize;
            _bufferBackward = new ConcurrentQueue<Mat>();
            _bufferForward = new ConcurrentQueue<Mat>();
            backgroundWorkerBackward.RunWorkerAsync(frameBackward);
            backgroundWorkerForward.RunWorkerAsync(frameForward);
            


        }
        public void StopBuffering()
        {
            // backgroundWorkerBackward.CancelAsync();
            // backgroundWorkerForward.CancelAsync();
            finish = true;
        }
        public Mat GetNextMat()
        {
            if (finishForward && _bufferForward.Count == 0)
                return null;
            Mat result = null;
            bool ok = false;
            while (ok == false)
                ok = _bufferForward.TryDequeue(out result);
            if (_bufferForward.Count <= _buforSize / 2 && !finishForward)
                resetEventForward.Set();
            return result;
        }
        public Mat GetPreviousMat()
        {
            if (finishBackward && _bufferBackward.Count == 0)
                return null;

            Mat result = null;
            bool ok = false;
            while (ok == false)
                ok = _bufferBackward.TryDequeue(out result);

            if(_bufferBackward.Count <= _buforSize/2 && !finishBackward)
            resetEventBackward.Set();

            return result;
        }
        private Mat NextFrame()
        {
            if (_videoCaptureForward != null)
            {
                Mat mat = new Mat();
                _videoCaptureForward.Read(mat);
                return mat;
            }
            if (_imagesCapture != null && _forwardIterator + 1 <= _imagesCapture.Count - 1)
            {
                _forwardIterator++;
                return new Mat(_imagesCapture[_forwardIterator]);
            }
            return null;

        }
        public Mat GetFrame(int frame)
        {
            if (_videoCaptureForward != null)
            {
                Mat mat = new Mat();
                _videoCaptureForward.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.PosFrames, frame);
                _videoCaptureForward.Read(mat);
                return mat;
            }
            if (_imagesCapture != null && _imagesCapture.Count - 1 >= frame && frame >= 0)
            {
                return new Mat(_imagesCapture[frame]);
            }

            return null;
        }
        public int GetNumberFrames()
        {
            if (_videoCaptureForward != null)
            {
                return (int)_videoCaptureForward.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameCount);
            }
            if (_imagesCapture != null)
            {
                return _imagesCapture.Count;
            }
            return -1;
        }
        public double GetFPS()
        {
            if (_videoCaptureForward != null)
            {
                return _videoCaptureForward.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.Fps);
            }
            return 25;
        }

        public Mat loadVideo(string path)
        {
            _videoCaptureForward = null;
            _videoCaptureBackward = null;
            _imagesCapture = null;
            finishForward = false;
            finishBackward = false;

            if (string.IsNullOrEmpty(path) == true)
            {
                return null;
            }
            Mat mat = new Mat();
            try
            {
                _videoCaptureForward = new VideoCapture(path);
                _videoCaptureBackward = new VideoCapture(path);
                _videoCaptureForward.Read(mat);
                Mat s = new Mat();
                _videoCaptureBackward.Read(s);
                FileName = Path.GetFileName(path);
                Resolution = new Size((int)_videoCaptureForward.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth),(int)_videoCaptureForward.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight));
                TimeInMs = _videoCaptureForward.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameCount) / GetFPS();
                return mat;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Mat loadImages(string path)
        {
            _imagesCapture = null;
            _videoCaptureForward = null;
            _videoCaptureBackward = null;
            finishForward = false;
            finishBackward = false;
            if (string.IsNullOrEmpty(path))
            {
                return null;
            }
            _imagesCapture = GetFiles(path, true);

            if (_imagesCapture != null && _imagesCapture.Count() > 0)
            {
                _forwardIterator = 0;
                Mat mat = new Mat(_imagesCapture[_forwardIterator]);
                FileName = Path.GetFileName(path);
                return mat;
            }
            return null;
        }

        private List<string> GetFiles(string path, bool isRecursive)
        {
            try
            {
                path = new FileInfo(path).DirectoryName;
            }
            catch (Exception ex)
            {
                return null;
            }
            var searchOption = isRecursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;

            List<string> foundFiles = new List<String>();
            foreach (var filter in Helpers.Helper.SupportedImageFormats)
            {
                foundFiles.AddRange(Directory.GetFiles(path, String.Format("*{0}", filter), searchOption));
            }
            return foundFiles;
        }
    }
}
