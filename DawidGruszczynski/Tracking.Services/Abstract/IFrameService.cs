﻿using Emgu.CV;
using Emgu.CV.Tracking;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tracking.Helpers;

namespace Tracking.Services
{
    public interface IFrameService
    {
        Mat loadVideo(string path);
        Mat loadImages(string path);
        Mat GetFrame(int frame);
        void StopBuffering();
        int GetNumberFrames();
        void StartBuffering(int frameForward, int frameBackward, int bufforsize );
        Mat GetNextMat();
        Mat GetPreviousMat();
        double GetFPS();
        string FileName { get;  }
        Size Resolution { get; }
        double TimeInMs { get; }
    }
}
