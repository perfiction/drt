﻿using Emgu.CV;
using Emgu.CV.Tracking;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tracking.Model;
using Tracking.Services.Concrete;

namespace Tracking.Services.Abstract
{
   public interface ITrackerService
    {      
        void UpdateRegionsForward(List<IObjectModel> objects, Mat m, int frame);
        void UpdateRegionsBackward(List<IObjectModel> objects, Mat m, int frame);
    }
}
