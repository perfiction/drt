﻿using Emgu.CV;
using Emgu.CV.Structure;
using Platform.Collections;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tracking.Services.Concrete;

namespace Tracking.Services.Abstract
{
   public interface IObjectDetectService
    {
        Rectangle FindObject(CircularFifoBuffer<Mat> models, Mat inputImage);
    }
}
