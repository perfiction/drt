﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace Tracking.Helpers
{
    public static class Helper
    {
        public static string[] SupportedImageFormats { get; } = new string[] { ".sr", ".ras", ".ppm", ".pgm", ".pbm", ".jpg", ".jpe", ".jp2", ".jpeg", ".png", ".gif", ".tif", ".tiff", ".bmp" };
        public static void AddSafe(this Dictionary<long, Rectangle> dictionary, long key, Rectangle value)
        {
            if (!dictionary.ContainsKey(key))
                dictionary.Add(key, value);
        }
        public  class CSVArgs
        {
            public bool X { get; set; }
            public bool Y { get; set; }
            public bool Time { get; set; }
            public bool Width { get; set; }
            public bool Height { get; set; }
            public bool Header { get; set; }
            public bool Frame { get; set; }
            public string Path { get; set; }
            public char Delimeter { get; set; }
            public double TimeOffset { get; set; }
        
        }
        public class AdditionalInformation
        {
            public string FileName { get; set; }
            public double FPS { get; set; }
            public Size Resolution { get; set; }
            public double TimeInMs { get; set; }
        }

        public static void SaveToCSV(Dictionary<long, Rectangle> data, string regionName, CSVArgs args, AdditionalInformation addInfo, out string filePath)
        {
            var sorted =  data.OrderBy(xx => xx.Key);
            double dt = 1000.0 / addInfo.FPS;

            /*
            using (var writer = File.CreateText(args.Path + "\\" + fileName + ".csv"))
            using (var csvWriter = new CsvWriter(writer))
            {
                csvWriter.Configuration.Delimiter = args.Delimeter;

                csvWriter.WriteField($"#Name:{addInfo.FileName} Resolution:{addInfo.Resolution} Time:{addInfo.TimeInMs} FPS:{addInfo.FPS} ");
                csvWriter.NextRecord();


                if (args.Header)
                {
                    if (args.Time)
                        csvWriter.WriteField("Start time");
                    if (args.Frame)
                        csvWriter.WriteField("Frame");
                    if (args.X)
                        csvWriter.WriteField("X");
                    if (args.Y)
                        csvWriter.WriteField("Y");
                    if (args.Width)
                        csvWriter.WriteField("Width");
                    if (args.Height)
                        csvWriter.WriteField("Height");
                    csvWriter.NextRecord();
                }

                foreach (var item in sorted)
                {
                    if (args.Time)
                        csvWriter.WriteField(item.Key*offset + args.TimeOffset);
                    if (args.Frame)
                        csvWriter.WriteField(item.Key);
                    if (args.X)
                        csvWriter.WriteField(item.Value.X);
                    if (args.Y)
                        csvWriter.WriteField(item.Value.Y);
                    if (args.Width)
                        csvWriter.WriteField(item.Value.Width);
                    if (args.Height)
                        csvWriter.WriteField(item.Value.Height);
                    csvWriter.NextRecord();
                }
                writer.Flush();
            }
            */

            //zmiana: JM
            IFormatProvider formatProvider = System.Globalization.CultureInfo.InvariantCulture;
            char separator = args.Delimeter;

            string comment = "File name: " + addInfo.FileName.ToString(formatProvider) + separator + "Resolution: " + addInfo.Resolution.Width.ToString(formatProvider) + " x " + addInfo.Resolution.Height.ToString(formatProvider) + separator + addInfo.TimeInMs.ToString(formatProvider) + separator + "FPS:" + addInfo.FPS.ToString(formatProvider);
            string header = "#StartTimeMiliseconds;FrameIndex;Left;Top;Width;Height";

            List<string> lines = new List<string>();
            lines.Add(comment);
            lines.Add(header);
            foreach (KeyValuePair<long, Rectangle> pair in sorted)
            {
                long frameIndex = pair.Key;
                double startTimeMiliseconds = frameIndex * dt + args.TimeOffset;
                int left = pair.Value.X;
                int top = pair.Value.Y;
                int width = pair.Value.Width;
                int height = pair.Value.Height;
                lines.Add(startTimeMiliseconds.ToString(formatProvider) + separator + frameIndex.ToString(formatProvider) + separator + left.ToString(formatProvider) + separator + top.ToString(formatProvider) + separator + width.ToString(formatProvider) + separator + height.ToString(formatProvider));
            }

            string fileName = regionName.Trim() + ".csv";
            filePath = Path.Combine(args.Path, fileName);
            File.WriteAllLines(filePath, lines.ToArray());
        }
    }
}
