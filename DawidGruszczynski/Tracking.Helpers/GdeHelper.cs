﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Tracking.Helpers
{
    using GameLab.Eyetracking.Data.DynamicRegions;
    using static Tracking.Helpers.Helper;

    public static class GdeHelper
    {
        public static List<DynamicRegion> ConvertToDynamicRegions(this List<Model.IObjectModel> trackedObjects, double fps, double timeOffset)
        {
            double dt = 1000.0 / fps;
            List<DynamicRegion> dynamicRegions = new List<DynamicRegion>();
            foreach (Model.IObjectModel _object in trackedObjects)
            {
                string regionName = _object.Name;
                List<DynamicRegionFrame> regionFrames = new List<DynamicRegionFrame>();

                // (LB) Fix to include starting frame in output .csv
                if(!_object.BoundingBoxes.ContainsKey(_object.InitFrameId))
                    IncludeInitBoundingBox(regionFrames, _object, fps, timeOffset);

                foreach (KeyValuePair<long, Rectangle> pair in _object.BoundingBoxes.OrderBy(p => p.Key))
                {
                    Rectangle box = pair.Value;
                    int centerPositionX = box.X + box.Width / 2;
                    int centerPositionY = box.Y + box.Height / 2;
                    GameLab.Eyetracking.Geometry.Point centerPosition = new GameLab.Eyetracking.Geometry.Point(centerPositionX, centerPositionY);
                    GameLab.Eyetracking.Geometry.Size size = new GameLab.Eyetracking.Geometry.Size(box.Width, box.Height);
                    DynamicRegionFrame frame = new DynamicRegionFrame(centerPosition, size);

                    long frameIndex = pair.Key;
                    long startTimeMicroseconds = (long)(frameIndex * dt + timeOffset);
                    long endTimeMicroseconds = (long)(startTimeMicroseconds + dt);
                    frame.SetInterval(startTimeMicroseconds, endTimeMicroseconds);
                    regionFrames.Add(frame);
                }
                DynamicRegion dynamicRegion = new RectangularDynamicRegion(regionName, regionFrames.ToArray());
                dynamicRegions.Add(dynamicRegion);
            }
            return dynamicRegions;
        }

        private static void IncludeInitBoundingBox(List<DynamicRegionFrame> regionFrames, Model.IObjectModel obj, double fps, double timeOffset)
        {
            Rectangle box = obj.InitBoundingBox;

            double dt = 1000.0 / fps;

            int centerPositionX = box.X + box.Width / 2;
            int centerPositionY = box.Y + box.Height / 2;
            GameLab.Eyetracking.Geometry.Point centerPosition = new GameLab.Eyetracking.Geometry.Point(centerPositionX, centerPositionY);
            GameLab.Eyetracking.Geometry.Size size = new GameLab.Eyetracking.Geometry.Size(box.Width, box.Height);
            DynamicRegionFrame frame = new DynamicRegionFrame(centerPosition, size);
            long frameIndex = obj.InitFrameId;
            long startTimeMicroseconds = (long)(frameIndex * dt + timeOffset);
            long endTimeMicroseconds = (long)(startTimeMicroseconds + dt);
            frame.SetInterval(startTimeMicroseconds, endTimeMicroseconds);
            regionFrames.Add(frame);
        }

        public static void SaveDynamicRegionsToGdeFormat(List<Model.IObjectModel> trackedObjects, string outputFilePath, CSVArgs args, AdditionalInformation addInfo, IFormatProvider formatProvider = null)
        {
            char separator = args.Delimeter;
            string comment = "Exported from Gaze Dynamic Regions Tracker" + separator + " Movie file name: " + addInfo.FileName.ToString(formatProvider) + separator + "Resolution: " + addInfo.Resolution.Width.ToString(formatProvider) + " x " + addInfo.Resolution.Height.ToString(formatProvider) + separator + addInfo.TimeInMs.ToString(formatProvider) + separator + "FPS:" + addInfo.FPS.ToString(formatProvider);
            if (formatProvider == null) formatProvider = System.Globalization.CultureInfo.InvariantCulture;
            List<DynamicRegion> dynamicRegions = ConvertToDynamicRegions(trackedObjects, addInfo.FPS, args.TimeOffset);
            GazeDynamicRegionsHelper.SaveDynamicRegionsToFile(outputFilePath, separator, dynamicRegions, formatProvider);
        }
    }
}
