﻿using Eyetracker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tracking.Model;
using Tracking.Services;
using System.Drawing;
using Emgu.CV.Structure;
using System.IO;
using System.Windows;
using Emgu.CV.Tracking;
using static Tracking.Helpers.Helper;
using Tracking.Services.Abstract;
using Emgu.CV;
using System.Threading;
using Eyetracker.Presenters.Abstarct;
using Tracking.Model.Settings;
using Tracking.Services.Concrete;
using Eyetracker.Presenters.Concrete;
using Eyetracker.Views;
using Eyetracker.Views.Concrete;

namespace Tracking.Presenter
{
    public class Presenter : IPresenter
    {
        public IMainView MainView => _view;

        IMainView _view;
        ITypeObjectNamePresenter _typeObjectNamePresenter;
        IFrameService _frameService;
        IObjectDetectService _objectDetectService;
        ITrackerService _trackerService;
        ISaveResultPresenter _saveResultPresenter;
        ISelectObjectPresenter _selectObjectPresenter;

        List<IObjectModel> _objects = new List<IObjectModel>();

        bool _play = false;
        bool _forward = true;
        ManualResetEvent _resetEvent;
        readonly Size _imageSize = new Size(769, 406);
        float _scaleX;
        float _scaleY;
        Mat _currentFrame;
        int _frameInterator = 0;
        Thread playbackThread;

        public Presenter()
        {
            _view = new MainView();
            _frameService = new FrameServices();
            _objectDetectService = null;
            _trackerService = new TrackerService(_objectDetectService);
            _typeObjectNamePresenter = new TypeObjectNamePresenter(new TypeObjectNameView());
            _saveResultPresenter = new SaveResultPresenter(new SaveResultView());
            _selectObjectPresenter = new SelectObjectPresenter(new SelectObjectView());

            AddViewActions();
        }

        public Presenter(IMainView view, IFrameService frameService, IObjectDetectService objectDetect, ITrackerService trackerService, ITypeObjectNamePresenter typeObjectNamePresenter, ISaveResultPresenter saveResultPresenter, ISelectObjectPresenter selectObjectPresenter)
        {
            _view = view;
            _frameService = frameService;
            _objectDetectService = objectDetect;
            _trackerService = trackerService;
            _typeObjectNamePresenter = typeObjectNamePresenter;
            _saveResultPresenter = saveResultPresenter;
            _selectObjectPresenter = selectObjectPresenter;

            AddViewActions();
        }

        private void AddViewActions()
        {
            _view.Open += _view_Open;
            _view.SelectedFrameChanged += _view_SelectedFrameChanged;
            _view.AddObject += _view_AddObject;
            _view.Play += _view_Play;
            _view.Stop += _view_Stop;
            _view.Pause += _view_Pause;
            _view.RemoveObject += _view_RemoveObject;
            _view.Fix += _view_Fix;
            _view.AddModel += _view_AddAdditionalInformation;
            _view.DetectionMethodChanged += _view_DetectionMethodChanged;
            _view.ChangeTrackerService += _view_ChangeTrackerService;
        }

        private void _view_AddAdditionalInformation(Rectangle rectangle)
        {
            string name = _selectObjectPresenter.SelectedObject(_objects.Select(x => x.Name).ToList());
            if (_selectObjectPresenter.Cancel == false)
            {
                var result = _objects.Where(x => x.Name == name).FirstOrDefault();
                if (result != null)
                {
                    rectangle = ScaleToOriginal(rectangle);
                    result.Templates.Add(new Mat(_currentFrame, rectangle));
                }
            }
        }

        private void _view_Fix(Rectangle rectangle)
        {
            string name = _selectObjectPresenter.SelectedObject(_objects.Select(x => x.Name).ToList());
            if (_selectObjectPresenter.Cancel == false)
            {
                var result = _objects.Where(x => x.Name == name).FirstOrDefault();
                if (result != null)
                {
                    rectangle = ScaleToOriginal(rectangle);
                    result.ReinitTracker(rectangle, _frameInterator, _currentFrame, _forward);

                    //TODO: Workaround for MOTLD (LB)
                    if(_trackerService.GetType() == typeof(MOTldService))
                        ((MOTldService)_trackerService).FixObject(rectangle, name);
                }
            }
            _resetEvent.Set();
        }
        private Rectangle ScaleToOriginal(Rectangle rectangle)
        {
            float scaleX = (float)_view.Bitmap.Width / (float)_imageSize.Width;
            float scaleY = (float)_view.Bitmap.Height / (float)_imageSize.Height;

            rectangle.X = (int)(scaleX * rectangle.X);
            rectangle.Y = (int)(scaleY * rectangle.Y);
            rectangle.Width = (int)(scaleX * rectangle.Width);
            rectangle.Height = (int)(scaleY * rectangle.Height);
            return rectangle;
        }
        private void _view_Pause()
        {
            _resetEvent.Reset();
        }

        private void _view_RemoveObject(string obj)
        {
            var obToDelete = _objects.Where(x => x.Name == obj).FirstOrDefault();
            if (obToDelete != null)
            {
                _objects.Remove(obToDelete);
                _view.TrackedObjects.Remove(obj);
                _view.BoundingBoxes = ResizeRectangle(_objects.Select(x => x.CurrentBoundingBox).ToList());
            }
        }


        private void _view_Stop()
        {
            _play = false;
            foreach (var item in _objects)
            {
                item.Restart();
            }

            //TODO: Workaround for MOTLD (LB)
            if (_trackerService.GetType() == typeof(MOTldService))
                ((MOTldService) _trackerService).Stop();

            if(playbackThread != null) {
                playbackThread.Abort();
                playbackThread = null;
            }
        }

        private void _view_Play(System.Windows.Forms.Form form)
        {
            playbackThread = new Thread(() =>
            {
                try
                {
                    _play = true;
                    _forward = true;
                    if (_objects.Count == 0)
                        return;

                    _frameInterator = _objects.Min(x => x.InitFrameId) + 1;
                    var back = _objects.Max(x => x.InitFrameId) - 1;
                    _frameService.StartBuffering(_frameInterator, back, 30);
                    _currentFrame = _frameService.GetNextMat();

                    while (_currentFrame != null && _play)
                    {
                        _view.Bitmap = _currentFrame.Bitmap;

                        _trackerService.UpdateRegionsForward(_objects, _currentFrame, _frameInterator);
                        _view.BoundingBoxes = ResizeRectangle(_objects.Select(x => x.CurrentBoundingBox).ToList());

                        _resetEvent.WaitOne();
                        _currentFrame = _frameService.GetNextMat();
                        _frameInterator++;
                        GC.Collect();
                    }
                    if (_play)
                    {
                        _forward = false;
                        _frameInterator = _objects.Max(x => x.InitFrameId) - 1;
                        _currentFrame = _frameService.GetPreviousMat();
                    }

                    while (_currentFrame != null && _play)
                    {
                        _view.Bitmap = _currentFrame.Bitmap;

                        _trackerService.UpdateRegionsBackward(_objects, _currentFrame, _frameInterator);
                        _view.BoundingBoxes = ResizeRectangle(_objects.Select(x => x.CurrentBoundingBox).ToList());

                        _resetEvent.WaitOne();
                        _currentFrame = _frameService.GetPreviousMat();
                        _frameInterator--;
                        GC.Collect();
                    }
                    if (_play)
                    {
                        var info = new AdditionalInformation()
                        {
                            FPS = _frameService.GetFPS(),
                            FileName = _frameService.FileName,
                            TimeInMs = _frameService.TimeInMs,
                            Resolution = _frameService.Resolution
                        };
                        _saveResultPresenter.Save(_objects, info, form);
                        _objects.Clear();
                        _view.BoundingBoxes.Clear();
                        _view.TrackedObjects.Clear();
                        _view.ProcessFinish = true;

                    }
                    _frameService.StopBuffering();
                    _currentFrame = _frameService.GetFrame(0);
                    _view.Bitmap = _currentFrame.Bitmap;
                    _view.CurrentFrame = 0;
                }
                catch (ThreadAbortException e)
                {
                    // (LB) Aborting the thread on application stop is expected - ignore this exception
                }
            });

            playbackThread.Start();
        }

        private void _view_AddObject(Rectangle rectangle, long frame, ITrackerSettings settings)
        {
            rectangle = ScaleToOriginal(rectangle);

            _typeObjectNamePresenter.ObjectsName = _objects.Select(x => x.Name).ToList();
            _typeObjectNamePresenter.GetView().ShowView();
            string name = _typeObjectNamePresenter.GetView().ObjectName;

            var newObject = new ObjectModel((int)frame, rectangle, new Mat(_currentFrame, rectangle), _currentFrame, name, settings);
            _objects.Add(newObject);
            _view.TrackedObjects.Add(newObject.Name);

            //TODO: Workaround for MOTLD (LB)
            if (_trackerService.GetType() == typeof(MOTldService))
            {
                MOTldService svc = (MOTldService) _trackerService;
                if(!svc.Allocated) svc.Init(_currentFrame, settings);
                svc.AddObject(rectangle, name);
            } else
            {
                newObject.TrackerForward.Init(_currentFrame, rectangle);
                newObject.TrackerBackward.Init(_currentFrame, rectangle);
            }
        }


        private List<Rectangle> ResizeRectangle(List<Rectangle> rectangles)
        {
            List<Rectangle> rectanglesRes = new List<Rectangle>();

            for (int i = 0; i < rectangles.Count; i++)
            {
                rectanglesRes.Add(new Rectangle((int)(_scaleX * rectangles[i].X), (int)(_scaleY * rectangles[i].Y), (int)(_scaleX * rectangles[i].Width), (int)(_scaleY * rectangles[i].Height)));
            }
            return rectanglesRes;
        }
        private void _view_SelectedFrameChanged(int frame)
        {
            _currentFrame = _frameService.GetFrame(frame);
            if (_currentFrame.Bitmap != null)
                _view.Bitmap = _currentFrame?.Bitmap;
        }

        public IMainView GetMainView()
        {
            return _view;
        }

        private void _view_Open(string path)
        {
            _objects.Clear();
            _view.TrackedObjects.Clear();
            Mat firstMat = null;
            foreach (var item in SupportedImageFormats)
            {
                if (path.Contains(item))
                {
                    firstMat = _frameService.loadImages(path);
                    break;
                }
            }
            if (firstMat == null)
                firstMat = _frameService.loadVideo(path);

            _view.Bitmap = firstMat.Bitmap;
            _resetEvent = new ManualResetEvent(true);
            _view.MaxFrame = _frameService.GetNumberFrames();
            _view.BoundingBoxes.Clear();
            _currentFrame = firstMat.Clone();
            _scaleX = (float)_imageSize.Width / (float)_currentFrame.Bitmap.Width;
            _scaleY = (float)_imageSize.Height / (float)_currentFrame.Bitmap.Height;
            _view.FileName ="Gaze Dynamic Regions Tracker - " + _frameService.FileName + " (FPS: " + _frameService.GetFPS().ToString() + ")";

        }

        private void _view_DetectionMethodChanged(int methodIndex)
        {
            switch (methodIndex)
            {
                case 0:
                    _objectDetectService = null;
                    break;
                case 1:
                    _objectDetectService = new TemplateDetectService();
                    break;
                case 2:
                    _objectDetectService = new FeatureDetectService();
                    break;
            }

            _trackerService = new TrackerService(_objectDetectService);
        }

        private void _view_ChangeTrackerService(object selectedTracker)
        {
            if (selectedTracker.Equals("Multi Object TLD")) {
                MOTldService svc = new MOTldService(_objectDetectService);
                if(_currentFrame != null)
                {
                    svc.Init(_currentFrame, MOTldSettings.Default);
                    if(_objects != null && _objects.Count > 0)
                        foreach(var obj in _objects)
                            svc.AddObject(obj.InitBoundingBox, obj.Name);
                }
                _trackerService = svc;
            }
            else
                _trackerService = new TrackerService(_objectDetectService);
        }
    }
}



