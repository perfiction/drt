﻿using Eyetracker.Presenters.Abstarct;
using Eyetracker.Views.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eyetracker.Presenters.Concrete
{
    public class SelectObjectPresenter : ISelectObjectPresenter
    {
        ISelectObjectView _selectObjectView;
        public SelectObjectPresenter(ISelectObjectView selectObjectView)
        {
            _selectObjectView = selectObjectView;
        }

        public bool Cancel => _selectObjectView.Cancel;

        public string SelectedObject(List<string> list)
        {
            _selectObjectView.Objects = list;
            _selectObjectView.ShowView();
            return _selectObjectView.SelectedObject;
        }
       
    }
}
