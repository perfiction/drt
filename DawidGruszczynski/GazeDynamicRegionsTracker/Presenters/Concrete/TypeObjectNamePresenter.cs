﻿using Eyetracker.Presenters.Abstarct;
using Eyetracker.Views.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eyetracker.Presenters.Concrete
{
   public class TypeObjectNamePresenter: ITypeObjectNamePresenter
    {
        ITypeObjectNameView _objectNameView;
        public TypeObjectNamePresenter( ITypeObjectNameView objectNameView)
        {
            _objectNameView = objectNameView;
        }

        public List<string> ObjectsName { set {_objectNameView.ObjectsName = value ; } }

        public ITypeObjectNameView GetView()
        {
            return _objectNameView;
        }
    }
}
