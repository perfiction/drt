﻿using Eyetracker.Presenters.Abstarct;
using Eyetracker.Views.Abstract;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tracking.Model;
using static Tracking.Helpers.Helper;
using static Tracking.Helpers.GdeHelper;

namespace Eyetracker.Presenters.Concrete
{
    public class SaveResultPresenter : ISaveResultPresenter
    {
        ISaveResultView _saveResultView;
        public SaveResultPresenter(ISaveResultView saveResultView)
        {
            _saveResultView = saveResultView;
        }
        public ISaveResultView GetView()
        {
            return _saveResultView;
        }
        public void Save(List<IObjectModel> objects, AdditionalInformation addInfo, System.Windows.Forms.Form form)
        {
            /*
            _saveResultView.ShowView();

            List<string> filePaths = new List<string>();

            if (_saveResultView.Save)
            {
                foreach (var item in objects)
                {
                    string filePath;
                    Tracking.Helpers.Helper.SaveToCSV(item.BoundingBoxes, item.Name, _saveResultView.CSVArgs,addInfo, out filePath);
                    filePaths.Add(filePath);
                }
            }
            */

            //łamię rozdział MVP i robię pokazanie okna dialogowego w tym miejscu

            Action a = () =>
            {
                System.Windows.Forms.SaveFileDialog saveFileDialog = new System.Windows.Forms.SaveFileDialog();
                saveFileDialog.Title = "Choose dynamic regions file";
                saveFileDialog.Filter = "Dynamic regions file (*.csv)|*.csv|All files (*.*)|*.*";
                if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    try
                    {
                        SaveDynamicRegionsToGdeFormat(objects, saveFileDialog.FileName, _saveResultView.CSVArgs, addInfo);
                    }
                    catch(Exception exc)
                    {
                        System.Windows.Forms.MessageBox.Show("Error while saving dynamic regions file: " + exc.Message, form.Text, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    }
                }
            };
            form.Invoke(a);
        }
    }

}
