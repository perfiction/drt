﻿using Eyetracker.Views.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eyetracker.Presenters.Abstarct
{
   public interface ISelectObjectPresenter
    {
        string SelectedObject(List<string> objects);
        bool Cancel { get; }
    }
}
