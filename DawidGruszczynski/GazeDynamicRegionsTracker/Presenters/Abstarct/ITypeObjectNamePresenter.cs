﻿using Eyetracker.Views.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eyetracker.Presenters.Abstarct
{
   public interface ITypeObjectNamePresenter
    {
        ITypeObjectNameView GetView();
        List<string> ObjectsName { set; }
    }
}
