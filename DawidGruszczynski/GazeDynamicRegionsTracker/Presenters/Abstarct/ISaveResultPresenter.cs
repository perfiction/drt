﻿using Eyetracker.Views.Abstract;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tracking.Model;
using static Tracking.Helpers.Helper;

namespace Eyetracker.Presenters.Abstarct
{
    public interface ISaveResultPresenter
    {
        ISaveResultView GetView();
        void Save(List<IObjectModel> objects, AdditionalInformation addInfo, System.Windows.Forms.Form form);
    }
}
