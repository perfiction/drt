﻿using Emgu.CV.Tracking;
using Eyetracker.Presenters.Abstarct;
using Eyetracker.Presenters.Concrete;
using Eyetracker.Views;
using Eyetracker.Views.Abstract;
using Eyetracker.Views.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tracking.Model;
using Tracking.Presenter;
using Tracking.Services;
using Tracking.Services.Abstract;
using Tracking.Services.Concrete;

namespace Eyetracker.Program
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            IPresenter mainPresenter = new Presenter();

            Application.Run((MainView)mainPresenter.GetMainView());
        }
    }
}
