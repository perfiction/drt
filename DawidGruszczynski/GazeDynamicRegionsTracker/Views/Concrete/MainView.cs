﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Windows.Forms;
using Tracking.Model.Settings;
using Tracking.Services.Concrete;


namespace Eyetracker
{
    enum Mode { AddObject, SelectFace, Default, FixObject, AddAdditionalInformation }
    public partial class MainView : Form, IMainView
    {
        Mode _mode = Mode.Default;
        bool isMouseDropDown = false;

        Point StarPoint;
        Point FinishPoint;
        Rectangle rectangle;

        Pen pen = new Pen(Brushes.Red) { Width = 2 };

        public MainView()
        {
            InitializeComponent();

            comboBox1.SelectedIndex = 0;
            comboBox1.SelectedIndexChanged += ComboBox1_SelectedIndexChanged;

            detectionMethodCbx.SelectedIndex = 0;
            detectionMethodCbx.SelectedIndexChanged += detectionMethodCbx_SelectedIndexChanged;

            TrackedObjects.CollectionChanged += TrackedObjects_CollectionChanged;
        }

        private void TrackedObjects_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() => { Update(); }));
            }
            else
            {
                Update();
            }
            void Update()
            {
                Listbox.Items.Clear();
                foreach (var item in TrackedObjects)
                {
                    Listbox.Items.Add(item);
                }
                selObjLabel.Text = $"{TrackedObjects.Count}";
            }
        }
        public string FileName {
            set
            {
                Invoke(new Action(() => {
                    this.Text = value;
                }));
            }
        }
        public int CurrentFrame
        {
            get => Convert.ToInt32(CurrentFrameTextBox.Text);
            set
            {
                Invoke(new Action(() => {
                    CurrentFrameTextBox.Text = value.ToString();
                    tbFrames.Value = value;
                }));
                
            }
        }
        public int MaxFrame
        {
            set
            {
                NumberOfAllFrameslabel.Text = (value - 1).ToString();
                tbFrames.Maximum = value - 1;
                tbFrames.TickFrequency = tbFrames.Maximum / 10;
            }
        }
        public List<Rectangle> BoundingBoxes { get; set; } = new List<Rectangle>();
        public Bitmap Bitmap
        {
            get { return _bitmap; }
            set { _bitmap = new Bitmap(value); pbMovieFrame.Image = _bitmap; }
        }
        private Bitmap _bitmap;

       
        public ObservableCollection<string> TrackedObjects { get; set; } = new ObservableCollection<string>();
        public bool ProcessFinish
        {
            set
            {
                if (value)
                {
                    Invoke(new Action(() =>
                    {
                        StartBtn.Enabled = false;
                        StopBtn.Enabled = false;
                        fixBtn.Enabled = false;
                        btnAddObjectConfirm.Enabled = true;
                        CurrentFrameTextBox.Enabled = true;
                        tbFrames.Enabled = true;
                        Listbox.Enabled = true;
                        OpenBtn.Enabled = true;
                        detectionMethodCbx.Enabled = true;
                        pbMovieFrame.Refresh();
                    }));

                }
            }
        }

        public event Action<string> Open;
        public event Action<int> SelectedFrameChanged;
        public event Action<Rectangle, long, ITrackerSettings> AddObject;
        public event Action<Form> Play; //TODO: złamanie MVP (JM)
        public event Action Stop;
        public event Action<string> RemoveObject;
        public event Action<Rectangle> Fix;
        public event Action Pause;
        public event Action<Rectangle> AddModel;
        public event Action<int> DetectionMethodChanged;
        public event Action<object> ChangeTrackerService;

        private void StopBtn_Click(object sender, EventArgs e)
        {
            Stop();          
            pbMovieFrame.Refresh();
            if (TrackedObjects.Count > 0)
            {
                StartBtn.Enabled = true;
                button1.Enabled = true;
            }
            StopBtn.Enabled = false;
            fixBtn.Enabled = false;
            
            btnAddObjectConfirm.Enabled = true;
            CurrentFrameTextBox.Enabled = true;
            tbFrames.Enabled = true;
            Listbox.Enabled = true;
            OpenBtn.Enabled = true;
            detectionMethodCbx.Enabled = true;

        }

        private void OpenBtn_Click(object sender, EventArgs e)
        {
            Stop();
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Movie file (*.avi; *.mpg; *.mp4)|*.avi; *.mpg; *.mp4|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                Open(openFileDialog.FileName);

                btnAddObjectConfirm.Enabled = true;
                CurrentFrameTextBox.Enabled = true;
                tbFrames.Enabled = true;
                BoundingBoxes.Clear();
            }
        }

        public void ShowMainView()
        {
            this.Show();
        }

        private void tbFrames_ValueChanged(object sender, EventArgs e)
        {
            TrackBar trackBar = sender as TrackBar;
            CurrentFrameTextBox.Text = trackBar.Value.ToString();
            SelectedFrameChanged(trackBar.Value);
            BoundingBoxes.Clear();
        }

        private void CurrentFrameTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            int value;
            int.TryParse(CurrentFrameTextBox.Text, out value);
            if (e.KeyCode == Keys.Enter && value <= tbFrames.Maximum)
            {
                tbFrames.Value = value;
                SelectedFrameChanged(value);
            }
        }

        private void btnAddObjectConfirm_Click(object sender, EventArgs e)
        {
            if (_mode != Mode.AddObject)
            {                
                _mode = Mode.AddObject;
                StopBtn.Enabled = false;
                OpenBtn.Enabled = false;
                tbFrames.Enabled = false;
                CurrentFrameTextBox.Enabled = false;
                btnAddObjectConfirm.Text = "Confirm";
                pbMovieFrame.Refresh();
            }
            else
            {
                _mode = Mode.Default;
   
                OpenBtn.Enabled = true;
                tbFrames.Enabled = true;
                CurrentFrameTextBox.Enabled = true;
                btnAddObjectConfirm.Text = "Add object";
                if (rectangle.Width > 0 && rectangle.Height > 0)
                {
                    rectangle = ValidateRectangle(rectangle);
                    BoundingBoxes.Add(rectangle);

                    AddObject(rectangle, CurrentFrame, GetTrackerSettings());
                    rectangle = new Rectangle();
                    if (TrackedObjects.Count > 0)
                    {
                        StartBtn.Enabled = true;
                        button1.Enabled = true;
                    }
                    pbMovieFrame.Refresh();
                }
            }

        }

    

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (_mode == Mode.AddObject || _mode == Mode.FixObject || _mode == Mode.AddAdditionalInformation)
            {
                isMouseDropDown = true;
                StarPoint = e.Location;
            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMouseDropDown == true)
            {
                FinishPoint = e.Location;
                pbMovieFrame.Invalidate();
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (isMouseDropDown == true)
            {
                FinishPoint = e.Location;
                isMouseDropDown = false;
            }
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            if (isMouseDropDown == false)
                e.Graphics.DrawRectangle(pen, rectangle);
            else
                e.Graphics.DrawRectangle(pen, GetRectangle());
            if (_mode != Mode.AddObject && _mode != Mode.AddAdditionalInformation)
                foreach (var item in BoundingBoxes)
                {
                    e.Graphics.DrawRectangle(pen, item);
                }
        }
        private Rectangle GetRectangle()
        {
            rectangle = new Rectangle();
            rectangle.X = Math.Min(StarPoint.X, FinishPoint.X);
            rectangle.Y = Math.Min(StarPoint.Y, FinishPoint.Y);
            rectangle.Width = Math.Abs(StarPoint.X - FinishPoint.X);
            rectangle.Height = Math.Abs(StarPoint.Y - FinishPoint.Y);

            return rectangle;
        }

        private void StartBtn_Click(object sender, EventArgs e)
        {
            Play(this); //TODO: złamanie MVP (JM)
            StartBtn.Enabled = false;
            StopBtn.Enabled = true;
            OpenBtn.Enabled = false;
            fixBtn.Enabled = true;
            btnAddObjectConfirm.Enabled = false;
            detectionMethodCbx.Enabled = false;
            tbFrames.Enabled = false;
            button1.Enabled = false;
            CurrentFrameTextBox.Enabled = false;
            Listbox.Enabled = false;
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeTrackerService(comboBox1.SelectedItem);
            kcfSettingsControl1.Visible = false;
            boostSettingsControl1.Visible = false;
            goturnSettingsControl1.Visible = false;
            medianFlowSettingsControl1.Visible = false;
            mosseSettingsControl1.Visible = false;
            milSettingsControl1.Visible = false;
            tldSettingsControl1.Visible = false;
            csrtSettingsControl1.Visible = false;
            moTldSettingsControl1.Visible = false;

            detectionMethodCbx.Enabled = true;

            switch (comboBox1.SelectedItem)
            {
                case "KCF":
                    kcfSettingsControl1.Visible = true;
                    break;
                case "Boosting":
                    boostSettingsControl1.Visible = true;
                    break;
                case "GOTRUN":
                    goturnSettingsControl1.Visible = true;
                    break;
                case "MEDIAN FLOW":
                    medianFlowSettingsControl1.Visible = true;
                    break;
                case "MOSSE":
                    mosseSettingsControl1.Visible = true;
                    break;
                case "MIL":
                    milSettingsControl1.Visible = true;
                    break;
                case "TLD":
                    tldSettingsControl1.Visible = true;
                    break;
                case "CSRT":
                    csrtSettingsControl1.Visible = true;
                    break;
                case "Multi Object TLD":
                    moTldSettingsControl1.Visible = true;
                    detectionMethodCbx.Enabled = false;
                    break;
            }

        }


        private void RemoveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Listbox.SelectedItem == null)
                return;
            if (Listbox.Items != null && Listbox.Items.Count > 0)
                RemoveObject(Listbox.SelectedItem.ToString());
            if (TrackedObjects.Count == 0)
            {
                StartBtn.Enabled = false;
                button1.Enabled = false;
            }
            pbMovieFrame.Refresh(); 
        }

        private void RemoveAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < TrackedObjects.Count; i++)
            {
                RemoveObject(TrackedObjects[i]);
            }
            StartBtn.Enabled = false;
            button1.Enabled = false;
            pbMovieFrame.Refresh();
        }

        private ITrackerSettings GetTrackerSettings()
        {
            ITrackerSettings settings = null;

            switch (comboBox1.SelectedItem)
            {
                case "KCF":
                    {
                        settings = kcfSettingsControl1.GetTrackerSettings();
                    }
                    break;
                case "Boosting":
                    {
                        settings = boostSettingsControl1.GetTrackerSettings();
                    }
                    break;
                case "GOTRUN":
                    {
                        settings = goturnSettingsControl1.GetTrackerSettings();
                    }
                    break;
                case "MEDIAN FLOW":
                    {
                        settings = medianFlowSettingsControl1.GetTrackerSettings();
                    }
                    break;
                case "MIL":
                    {
                        settings = milSettingsControl1.GetTrackerSettings();
                    }
                    break;
                case "MOSSE":
                    {
                        settings = mosseSettingsControl1.GetTrackerSettings();
                    }
                    break;
                case "TLD":
                    {
                        settings = tldSettingsControl1.GetTrackerSettings();
                    }
                    break;
                case "CSRT":
                    {
                        settings = csrtSettingsControl1.GetTrackerSettings();
                    }
                    break;
                    case "Multi Object TLD":
                    {
                        settings = moTldSettingsControl1.GetTrackerSettings();
                    }
                    break;
            }
            return settings;
        }

        private void Button1_Click_2(object sender, EventArgs e)
        {
            if (_mode != Mode.FixObject)
            {
                _mode = Mode.FixObject;
                fixBtn.Text = "Confirm";
                pbMovieFrame.Refresh();
                Pause();
            }
            else
            {
                _mode = Mode.Default;
                fixBtn.Text = "Fix";
                if (rectangle.Width > 0 && rectangle.Height > 0)
                {
                    rectangle = ValidateRectangle(rectangle);
                    Fix(rectangle);
                    rectangle = new Rectangle();

                }
            }

        }
        Rectangle ValidateRectangle(Rectangle rectangle)
        {
            if (rectangle.X < 0)
                rectangle.X = 0;
            if (rectangle.Y < 0)
                rectangle.Y = 0;
            if (rectangle.X + rectangle.Width > pbMovieFrame.Width)
                rectangle.Width = pbMovieFrame.Width - rectangle.X;
            if (rectangle.Y + rectangle.Height > pbMovieFrame.Height)
                rectangle.Height = pbMovieFrame.Height - rectangle.Y;
            return rectangle;
        }

        private void Button1_Click_1(object sender, EventArgs e)
        {
            if (_mode != Mode.AddAdditionalInformation)
            {
                _mode = Mode.AddAdditionalInformation;
                button1.Text = "Confirm";
                pbMovieFrame.Refresh();
                OpenBtn.Enabled = false;
                btnAddObjectConfirm.Enabled = false;
                StartBtn.Enabled = false;
            }
            else
            {
                _mode = Mode.Default;
                button1.Text = "Add template";
                OpenBtn.Enabled = true;
                btnAddObjectConfirm.Enabled = true;
                StartBtn.Enabled = true;
                if (rectangle.Width > 0 && rectangle.Height > 0)
                {
                    AddModel(rectangle);
                    rectangle = new Rectangle();

                }
            }
        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        private void detectionMethodCbx_SelectedIndexChanged(object sender, EventArgs e)
        {
            DetectionMethodChanged(detectionMethodCbx.SelectedIndex);
        }

        private void MainView_FormClosing(object sender, FormClosingEventArgs e)
        {
            Stop();
        }

        private void btnAddObjTxt_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text file (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog.FileName = "init.txt";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string line;
                using (System.IO.StreamReader reader = new System.IO.StreamReader(openFileDialog.FileName))
                {
                    line = reader.ReadLine();
                }

                float scaleX = pbMovieFrame.Width / (float)_bitmap.Width;
                float scaleY = pbMovieFrame.Height / (float)_bitmap.Height;

                string[] coordsStr = line.Split(',');
                int x = int.Parse(coordsStr[0]);
                int y = int.Parse(coordsStr[1]);
                int width = int.Parse(coordsStr[2]) - x;
                int height = int.Parse(coordsStr[3]) - y;

                rectangle = new Rectangle((int)Math.Round(x * scaleX), (int)Math.Round(y * scaleY), (int)Math.Round(width * scaleX), (int)Math.Round(height * scaleY));

                rectangle = ValidateRectangle(rectangle);
                BoundingBoxes.Add(rectangle);

                AddObject(rectangle, CurrentFrame, GetTrackerSettings());
                rectangle = new Rectangle();
                if (TrackedObjects.Count > 0)
                {
                    StartBtn.Enabled = true;
                    button1.Enabled = true;
                }
                pbMovieFrame.Refresh();
            }
        }
    }
}
