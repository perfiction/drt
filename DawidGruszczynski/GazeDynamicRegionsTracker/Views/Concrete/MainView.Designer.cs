﻿namespace Eyetracker
{
    partial class MainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainView));
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.detectionMethodCbx = new System.Windows.Forms.ComboBox();
            this.csrtSettingsControl1 = new Eyetracker.Views.Controls.CSRTSettingsControl();
            this.kcfSettingsControl1 = new Eyetracker.Views.Controls.KCFSettingsControl();
            this.boostSettingsControl1 = new Eyetracker.Views.Controls.BoostingSettingsControl();
            this.goturnSettingsControl1 = new Eyetracker.Views.Controls.GOTURNSettingsControl();
            this.medianFlowSettingsControl1 = new Eyetracker.Views.Controls.MedianFlowSettingsControl();
            this.milSettingsControl1 = new Eyetracker.Views.Controls.MILSettingsControl();
            this.mosseSettingsControl1 = new Eyetracker.Views.Controls.MOSSESettingsControl();
            this.tldSettingsControl1 = new Eyetracker.Views.Controls.TLDSettingsControl();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.NumberOfAllFrameslabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Listbox = new System.Windows.Forms.ListBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selObjLabel = new System.Windows.Forms.Label();
            this.moTldSettingsControl1 = new Eyetracker.Views.Controls.MOTldSettingsControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pbMovieFrame = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.fixBtn = new System.Windows.Forms.Button();
            this.btnAddObjectConfirm = new System.Windows.Forms.Button();
            this.StopBtn = new System.Windows.Forms.Button();
            this.StartBtn = new System.Windows.Forms.Button();
            this.OpenBtn = new System.Windows.Forms.Button();
            this.CurrentFrameTextBox = new System.Windows.Forms.TextBox();
            this.tbFrames = new System.Windows.Forms.TrackBar();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbMovieFrame)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbFrames)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.csrtSettingsControl1);
            this.panel1.Controls.Add(this.kcfSettingsControl1);
            this.panel1.Controls.Add(this.boostSettingsControl1);
            this.panel1.Controls.Add(this.goturnSettingsControl1);
            this.panel1.Controls.Add(this.medianFlowSettingsControl1);
            this.panel1.Controls.Add(this.milSettingsControl1);
            this.panel1.Controls.Add(this.mosseSettingsControl1);
            this.panel1.Controls.Add(this.tldSettingsControl1);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.moTldSettingsControl1);
            this.panel1.Location = new System.Drawing.Point(793, 10);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(271, 528);
            this.panel1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.detectionMethodCbx);
            this.groupBox1.Location = new System.Drawing.Point(4, 357);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(261, 40);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "General Settings";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Detection method:";
            // 
            // detectionMethodCbx
            // 
            this.detectionMethodCbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.detectionMethodCbx.FormattingEnabled = true;
            this.detectionMethodCbx.Items.AddRange(new object[] {
            "None",
            "Template Matching",
            "Feature Matching"});
            this.detectionMethodCbx.Location = new System.Drawing.Point(101, 14);
            this.detectionMethodCbx.Name = "detectionMethodCbx";
            this.detectionMethodCbx.Size = new System.Drawing.Size(155, 21);
            this.detectionMethodCbx.TabIndex = 6;
            // 
            // csrtSettingsControl1
            // 
            this.csrtSettingsControl1.Location = new System.Drawing.Point(3, 28);
            this.csrtSettingsControl1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.csrtSettingsControl1.Name = "csrtSettingsControl1";
            this.csrtSettingsControl1.Size = new System.Drawing.Size(262, 325);
            this.csrtSettingsControl1.TabIndex = 12;
            this.csrtSettingsControl1.Visible = false;
            // 
            // kcfSettingsControl1
            // 
            this.kcfSettingsControl1.Location = new System.Drawing.Point(3, 28);
            this.kcfSettingsControl1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.kcfSettingsControl1.Name = "kcfSettingsControl1";
            this.kcfSettingsControl1.Size = new System.Drawing.Size(262, 325);
            this.kcfSettingsControl1.TabIndex = 11;
            // 
            // boostSettingsControl1
            // 
            this.boostSettingsControl1.Location = new System.Drawing.Point(3, 28);
            this.boostSettingsControl1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.boostSettingsControl1.Name = "boostSettingsControl1";
            this.boostSettingsControl1.Size = new System.Drawing.Size(262, 325);
            this.boostSettingsControl1.TabIndex = 10;
            this.boostSettingsControl1.Visible = false;
            // 
            // goturnSettingsControl1
            // 
            this.goturnSettingsControl1.Location = new System.Drawing.Point(3, 28);
            this.goturnSettingsControl1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.goturnSettingsControl1.Name = "goturnSettingsControl1";
            this.goturnSettingsControl1.Size = new System.Drawing.Size(262, 325);
            this.goturnSettingsControl1.TabIndex = 9;
            this.goturnSettingsControl1.Visible = false;
            // 
            // medianFlowSettingsControl1
            // 
            this.medianFlowSettingsControl1.Location = new System.Drawing.Point(3, 28);
            this.medianFlowSettingsControl1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.medianFlowSettingsControl1.Name = "medianFlowSettingsControl1";
            this.medianFlowSettingsControl1.Size = new System.Drawing.Size(262, 325);
            this.medianFlowSettingsControl1.TabIndex = 8;
            this.medianFlowSettingsControl1.Visible = false;
            // 
            // milSettingsControl1
            // 
            this.milSettingsControl1.Location = new System.Drawing.Point(3, 28);
            this.milSettingsControl1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.milSettingsControl1.Name = "milSettingsControl1";
            this.milSettingsControl1.Size = new System.Drawing.Size(262, 325);
            this.milSettingsControl1.TabIndex = 7;
            this.milSettingsControl1.Visible = false;
            // 
            // mosseSettingsControl1
            // 
            this.mosseSettingsControl1.Location = new System.Drawing.Point(3, 28);
            this.mosseSettingsControl1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.mosseSettingsControl1.Name = "mosseSettingsControl1";
            this.mosseSettingsControl1.Size = new System.Drawing.Size(262, 325);
            this.mosseSettingsControl1.TabIndex = 6;
            this.mosseSettingsControl1.Visible = false;
            // 
            // tldSettingsControl1
            // 
            this.tldSettingsControl1.Location = new System.Drawing.Point(3, 28);
            this.tldSettingsControl1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tldSettingsControl1.Name = "tldSettingsControl1";
            this.tldSettingsControl1.Size = new System.Drawing.Size(262, 325);
            this.tldSettingsControl1.TabIndex = 5;
            this.tldSettingsControl1.Visible = false;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "KCF",
            "Boosting",
            "CSRT",
            "GOTRUN",
            "MEDIAN FLOW",
            "MIL",
            "MOSSE",
            "TLD",
            "Multi Object TLD"});
            this.comboBox1.Location = new System.Drawing.Point(3, 2);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(263, 21);
            this.comboBox1.TabIndex = 4;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.NumberOfAllFrameslabel);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.Listbox);
            this.groupBox4.Controls.Add(this.selObjLabel);
            this.groupBox4.Location = new System.Drawing.Point(4, 401);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox4.Size = new System.Drawing.Size(262, 120);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Summary";
            // 
            // NumberOfAllFrameslabel
            // 
            this.NumberOfAllFrameslabel.AutoSize = true;
            this.NumberOfAllFrameslabel.Location = new System.Drawing.Point(226, 18);
            this.NumberOfAllFrameslabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.NumberOfAllFrameslabel.Name = "NumberOfAllFrameslabel";
            this.NumberOfAllFrameslabel.Size = new System.Drawing.Size(13, 13);
            this.NumberOfAllFrameslabel.TabIndex = 8;
            this.NumberOfAllFrameslabel.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 18);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Number of objects:";
            this.label2.Click += new System.EventHandler(this.Label2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(122, 18);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Number of all frames:";
            // 
            // Listbox
            // 
            this.Listbox.ContextMenuStrip = this.contextMenuStrip1;
            this.Listbox.FormattingEnabled = true;
            this.Listbox.Location = new System.Drawing.Point(5, 34);
            this.Listbox.Margin = new System.Windows.Forms.Padding(2);
            this.Listbox.Name = "Listbox";
            this.Listbox.Size = new System.Drawing.Size(253, 82);
            this.Listbox.TabIndex = 4;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeToolStripMenuItem,
            this.removeAllToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(135, 48);
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.removeToolStripMenuItem.Text = "Remove";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.RemoveToolStripMenuItem_Click);
            // 
            // removeAllToolStripMenuItem
            // 
            this.removeAllToolStripMenuItem.Name = "removeAllToolStripMenuItem";
            this.removeAllToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.removeAllToolStripMenuItem.Text = "Remove All";
            this.removeAllToolStripMenuItem.Click += new System.EventHandler(this.RemoveAllToolStripMenuItem_Click);
            // 
            // selObjLabel
            // 
            this.selObjLabel.AutoSize = true;
            this.selObjLabel.Location = new System.Drawing.Point(98, 18);
            this.selObjLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.selObjLabel.Name = "selObjLabel";
            this.selObjLabel.Size = new System.Drawing.Size(13, 13);
            this.selObjLabel.TabIndex = 0;
            this.selObjLabel.Text = "0";
            // 
            // moTldSettingsControl1
            // 
            this.moTldSettingsControl1.Location = new System.Drawing.Point(3, 28);
            this.moTldSettingsControl1.Name = "moTldSettingsControl1";
            this.moTldSettingsControl1.Size = new System.Drawing.Size(262, 325);
            this.moTldSettingsControl1.TabIndex = 14;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.pbMovieFrame);
            this.panel2.ForeColor = System.Drawing.Color.Red;
            this.panel2.Location = new System.Drawing.Point(9, 10);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(773, 410);
            this.panel2.TabIndex = 1;
            // 
            // pbMovieFrame
            // 
            this.pbMovieFrame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbMovieFrame.Location = new System.Drawing.Point(0, 0);
            this.pbMovieFrame.Margin = new System.Windows.Forms.Padding(2);
            this.pbMovieFrame.Name = "pbMovieFrame";
            this.pbMovieFrame.Size = new System.Drawing.Size(769, 406);
            this.pbMovieFrame.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbMovieFrame.TabIndex = 0;
            this.pbMovieFrame.TabStop = false;
            this.pbMovieFrame.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            this.pbMovieFrame.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pbMovieFrame.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pbMovieFrame.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.button1);
            this.panel3.Controls.Add(this.fixBtn);
            this.panel3.Controls.Add(this.btnAddObjectConfirm);
            this.panel3.Controls.Add(this.StopBtn);
            this.panel3.Controls.Add(this.StartBtn);
            this.panel3.Controls.Add(this.OpenBtn);
            this.panel3.Controls.Add(this.CurrentFrameTextBox);
            this.panel3.Controls.Add(this.tbFrames);
            this.panel3.Location = new System.Drawing.Point(9, 424);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(780, 114);
            this.panel3.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(262, 58);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(124, 41);
            this.button1.TabIndex = 10;
            this.button1.Text = "Add template";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click_1);
            // 
            // fixBtn
            // 
            this.fixBtn.Enabled = false;
            this.fixBtn.Location = new System.Drawing.Point(650, 58);
            this.fixBtn.Margin = new System.Windows.Forms.Padding(2);
            this.fixBtn.Name = "fixBtn";
            this.fixBtn.Size = new System.Drawing.Size(124, 41);
            this.fixBtn.TabIndex = 9;
            this.fixBtn.Text = "Fix";
            this.fixBtn.UseVisualStyleBackColor = true;
            this.fixBtn.Click += new System.EventHandler(this.Button1_Click_2);
            // 
            // btnAddObjectConfirm
            // 
            this.btnAddObjectConfirm.Enabled = false;
            this.btnAddObjectConfirm.Location = new System.Drawing.Point(134, 58);
            this.btnAddObjectConfirm.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddObjectConfirm.Name = "btnAddObjectConfirm";
            this.btnAddObjectConfirm.Size = new System.Drawing.Size(124, 41);
            this.btnAddObjectConfirm.TabIndex = 6;
            this.btnAddObjectConfirm.Text = "Add object";
            this.btnAddObjectConfirm.UseVisualStyleBackColor = true;
            this.btnAddObjectConfirm.Click += new System.EventHandler(this.btnAddObjectConfirm_Click);
            // 
            // StopBtn
            // 
            this.StopBtn.Enabled = false;
            this.StopBtn.Location = new System.Drawing.Point(520, 58);
            this.StopBtn.Margin = new System.Windows.Forms.Padding(2);
            this.StopBtn.Name = "StopBtn";
            this.StopBtn.Size = new System.Drawing.Size(124, 41);
            this.StopBtn.TabIndex = 5;
            this.StopBtn.Text = "Stop";
            this.StopBtn.UseVisualStyleBackColor = true;
            this.StopBtn.Click += new System.EventHandler(this.StopBtn_Click);
            // 
            // StartBtn
            // 
            this.StartBtn.Enabled = false;
            this.StartBtn.Location = new System.Drawing.Point(392, 58);
            this.StartBtn.Margin = new System.Windows.Forms.Padding(2);
            this.StartBtn.Name = "StartBtn";
            this.StartBtn.Size = new System.Drawing.Size(124, 41);
            this.StartBtn.TabIndex = 3;
            this.StartBtn.Text = "Start";
            this.StartBtn.UseVisualStyleBackColor = true;
            this.StartBtn.Click += new System.EventHandler(this.StartBtn_Click);
            // 
            // OpenBtn
            // 
            this.OpenBtn.Location = new System.Drawing.Point(4, 58);
            this.OpenBtn.Margin = new System.Windows.Forms.Padding(2);
            this.OpenBtn.Name = "OpenBtn";
            this.OpenBtn.Size = new System.Drawing.Size(124, 41);
            this.OpenBtn.TabIndex = 2;
            this.OpenBtn.Text = "Open File";
            this.OpenBtn.UseVisualStyleBackColor = true;
            this.OpenBtn.Click += new System.EventHandler(this.OpenBtn_Click);
            // 
            // CurrentFrameTextBox
            // 
            this.CurrentFrameTextBox.Enabled = false;
            this.CurrentFrameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrentFrameTextBox.Location = new System.Drawing.Point(624, 3);
            this.CurrentFrameTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.CurrentFrameTextBox.Name = "CurrentFrameTextBox";
            this.CurrentFrameTextBox.Size = new System.Drawing.Size(151, 41);
            this.CurrentFrameTextBox.TabIndex = 1;
            this.CurrentFrameTextBox.Text = "0";
            this.CurrentFrameTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.CurrentFrameTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CurrentFrameTextBox_KeyDown);
            // 
            // tbFrames
            // 
            this.tbFrames.Enabled = false;
            this.tbFrames.Location = new System.Drawing.Point(3, 3);
            this.tbFrames.Margin = new System.Windows.Forms.Padding(2);
            this.tbFrames.Name = "tbFrames";
            this.tbFrames.Size = new System.Drawing.Size(616, 45);
            this.tbFrames.TabIndex = 0;
            this.tbFrames.ValueChanged += new System.EventHandler(this.tbFrames_ValueChanged);
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1070, 553);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1086, 592);
            this.MinimumSize = new System.Drawing.Size(1027, 592);
            this.Name = "MainView";
            this.Text = "Gaze Dyanamic Regions Tracker";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainView_FormClosing);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbMovieFrame)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbFrames)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox CurrentFrameTextBox;
        private System.Windows.Forms.TrackBar tbFrames;
        private System.Windows.Forms.PictureBox pbMovieFrame;
        private System.Windows.Forms.Button StopBtn;
        private System.Windows.Forms.Button StartBtn;
        private System.Windows.Forms.Button OpenBtn;
        private System.Windows.Forms.Button btnAddObjectConfirm;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label selObjLabel;
        private System.Windows.Forms.ListBox Listbox;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeAllToolStripMenuItem;
        private Views.Controls.TLDSettingsControl tldSettingsControl1;
        private Views.Controls.KCFSettingsControl kcfSettingsControl1;
        private Views.Controls.BoostingSettingsControl boostSettingsControl1;
        private Views.Controls.GOTURNSettingsControl goturnSettingsControl1;
        private Views.Controls.MedianFlowSettingsControl medianFlowSettingsControl1;
        private Views.Controls.MILSettingsControl milSettingsControl1;
        private Views.Controls.MOSSESettingsControl mosseSettingsControl1;
        private System.Windows.Forms.Button fixBtn;
        private Views.Controls.CSRTSettingsControl csrtSettingsControl1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label NumberOfAllFrameslabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox detectionMethodCbx;
        private System.Windows.Forms.Label label3;
        private Views.Controls.MOTldSettingsControl moTldSettingsControl1;
    }
}

