﻿using Eyetracker.Views.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Eyetracker.Views.Concrete
{
    public partial class SelectObjectView : Form, ISelectObjectView
    {
        public SelectObjectView()
        {
            InitializeComponent();
        }

        public List<string> Objects
        {
            set
            {
                comboBox1.Items.Clear();
                comboBox1.Items.AddRange(value.ToArray());
            }
        }

        public string SelectedObject
        {
            get
            {
                if (comboBox1.SelectedItem == null)
                    return "";
                return comboBox1.SelectedItem.ToString();
            }
        }

        public bool Cancel { get; private set; } = true;

        public void ShowView()
        {
            this.ShowDialog();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Cancel = false;
            this.Close();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Cancel = true;
            this.Close();
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            button1.Enabled = true;
        }
    }
}
