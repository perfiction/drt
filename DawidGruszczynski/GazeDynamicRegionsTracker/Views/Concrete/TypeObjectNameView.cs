﻿using Eyetracker.Views.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Eyetracker.Views
{
    public partial class TypeObjectNameView : Form, ITypeObjectNameView
    {
        int iterator = 0;
        public TypeObjectNameView()
        {
            InitializeComponent();
        }

        public string ObjectName { get { return tboxObjectName.Text; } }

        public List<string> ObjectsName { get; set; }

        public void ShowView()
        {
            tboxObjectName.Text = "Object " + iterator;
            this.ShowDialog();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (ObjectsName.Where(x => x == tboxObjectName.Text).Count() == 0)
            {
                iterator++;
                this.Close();

            }
        }
    }
}
