﻿using Eyetracker.Views.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tracking.Helpers;

namespace Eyetracker.Views.Concrete
{
    public partial class SaveResultView : Form, ISaveResultView
    {
        public SaveResultView()
        {
            InitializeComponent();
            textBox1.Text = AppDomain.CurrentDomain.BaseDirectory.ToString();
        }

       
        public bool Save { get; private set; }

        public Helper.CSVArgs CSVArgs { get {return new Helper.CSVArgs() {Delimeter= textBox2.Text[0], Path = textBox1.Text,Frame = chboxFrame.Checked,Header = chboxHeader.Checked,Height = chboxHeight.Checked,Width=chboxWidth.Checked,X=chboxX.Checked,Y=chboxY.Checked, Time = chboxTime.Checked, TimeOffset = (double)numericUpDown1.Value}; } }

        public void ShowView()
        {
            this.ShowDialog();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Thread newThread = new Thread(new ThreadStart(ThreadMethod));
            newThread.SetApartmentState(ApartmentState.STA);
            newThread.Start();

        }
        private void ThreadMethod()
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {                  
                    Invoke((Action)(() => { textBox1.Text = fbd.SelectedPath; }));
                }

            }
        }
        private void Button2_Click(object sender, EventArgs e)
        {
            Save = false;
            this.Close();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            Save = true;
            this.Close();
        }

        private void CheckBox4_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }
    }
}
