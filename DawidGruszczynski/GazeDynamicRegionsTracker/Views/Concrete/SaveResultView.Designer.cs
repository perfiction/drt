﻿namespace Eyetracker.Views.Concrete
{
    partial class SaveResultView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SaveResultView));
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.chboxHeader = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chboxTime = new System.Windows.Forms.CheckBox();
            this.chboxHeight = new System.Windows.Forms.CheckBox();
            this.chboxWidth = new System.Windows.Forms.CheckBox();
            this.chboxY = new System.Windows.Forms.CheckBox();
            this.chboxX = new System.Windows.Forms.CheckBox();
            this.chboxFrame = new System.Windows.Forms.CheckBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(93, 14);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(158, 22);
            this.textBox1.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(257, 14);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(43, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(62, 208);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Discard";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(185, 208);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "Save";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Path";
            this.label2.Click += new System.EventHandler(this.Label2_Click);
            // 
            // chboxHeader
            // 
            this.chboxHeader.AutoSize = true;
            this.chboxHeader.Checked = true;
            this.chboxHeader.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chboxHeader.Location = new System.Drawing.Point(6, 27);
            this.chboxHeader.Name = "chboxHeader";
            this.chboxHeader.Size = new System.Drawing.Size(77, 21);
            this.chboxHeader.TabIndex = 9;
            this.chboxHeader.Text = "Header";
            this.chboxHeader.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chboxTime);
            this.groupBox1.Controls.Add(this.chboxHeight);
            this.groupBox1.Controls.Add(this.chboxWidth);
            this.groupBox1.Controls.Add(this.chboxY);
            this.groupBox1.Controls.Add(this.chboxX);
            this.groupBox1.Controls.Add(this.chboxFrame);
            this.groupBox1.Controls.Add(this.chboxHeader);
            this.groupBox1.Enabled = false;
            this.groupBox1.Location = new System.Drawing.Point(15, 79);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(285, 91);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "CSV Data";
            // 
            // chboxTime
            // 
            this.chboxTime.AutoSize = true;
            this.chboxTime.Checked = true;
            this.chboxTime.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chboxTime.Location = new System.Drawing.Point(224, 27);
            this.chboxTime.Name = "chboxTime";
            this.chboxTime.Size = new System.Drawing.Size(61, 21);
            this.chboxTime.TabIndex = 16;
            this.chboxTime.Text = "Time";
            this.chboxTime.UseVisualStyleBackColor = true;
            // 
            // chboxHeight
            // 
            this.chboxHeight.AutoSize = true;
            this.chboxHeight.Checked = true;
            this.chboxHeight.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chboxHeight.Location = new System.Drawing.Point(88, 62);
            this.chboxHeight.Name = "chboxHeight";
            this.chboxHeight.Size = new System.Drawing.Size(71, 21);
            this.chboxHeight.TabIndex = 15;
            this.chboxHeight.Text = "Height";
            this.chboxHeight.UseVisualStyleBackColor = true;
            // 
            // chboxWidth
            // 
            this.chboxWidth.AutoSize = true;
            this.chboxWidth.Checked = true;
            this.chboxWidth.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chboxWidth.Location = new System.Drawing.Point(6, 62);
            this.chboxWidth.Name = "chboxWidth";
            this.chboxWidth.Size = new System.Drawing.Size(66, 21);
            this.chboxWidth.TabIndex = 14;
            this.chboxWidth.Text = "Width";
            this.chboxWidth.UseVisualStyleBackColor = true;
            // 
            // chboxY
            // 
            this.chboxY.AutoSize = true;
            this.chboxY.Checked = true;
            this.chboxY.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chboxY.Location = new System.Drawing.Point(179, 54);
            this.chboxY.Name = "chboxY";
            this.chboxY.Size = new System.Drawing.Size(39, 21);
            this.chboxY.TabIndex = 13;
            this.chboxY.Text = "Y";
            this.chboxY.UseVisualStyleBackColor = true;
            this.chboxY.CheckedChanged += new System.EventHandler(this.CheckBox4_CheckedChanged);
            // 
            // chboxX
            // 
            this.chboxX.AutoSize = true;
            this.chboxX.Checked = true;
            this.chboxX.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chboxX.Location = new System.Drawing.Point(179, 27);
            this.chboxX.Name = "chboxX";
            this.chboxX.Size = new System.Drawing.Size(39, 21);
            this.chboxX.TabIndex = 12;
            this.chboxX.Text = "X";
            this.chboxX.UseVisualStyleBackColor = true;
            // 
            // chboxFrame
            // 
            this.chboxFrame.AutoSize = true;
            this.chboxFrame.Checked = true;
            this.chboxFrame.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chboxFrame.Location = new System.Drawing.Point(89, 27);
            this.chboxFrame.Name = "chboxFrame";
            this.chboxFrame.Size = new System.Drawing.Size(70, 21);
            this.chboxFrame.TabIndex = 11;
            this.chboxFrame.Text = "Frame";
            this.chboxFrame.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(93, 45);
            this.textBox2.MaxLength = 1;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(65, 22);
            this.textBox2.TabIndex = 12;
            this.textBox2.Text = ";";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "Delimeter";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(104, 176);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(109, 22);
            this.numericUpDown1.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 178);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 17);
            this.label1.TabIndex = 18;
            this.label1.Text = "Time offset:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(219, 178);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 17);
            this.label4.TabIndex = 19;
            this.label4.Text = "ms";
            // 
            // SaveResultView
            // 
            this.AcceptButton = this.button3;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button2;
            this.ClientSize = new System.Drawing.Size(322, 243);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SaveResultView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Save Result";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chboxHeader;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chboxY;
        private System.Windows.Forms.CheckBox chboxX;
        private System.Windows.Forms.CheckBox chboxFrame;
        private System.Windows.Forms.CheckBox chboxHeight;
        private System.Windows.Forms.CheckBox chboxWidth;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chboxTime;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
    }
}