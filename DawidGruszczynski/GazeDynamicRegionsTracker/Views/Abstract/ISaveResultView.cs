﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Tracking.Helpers.Helper;

namespace Eyetracker.Views.Abstract
{
   public interface ISaveResultView
    {
        void ShowView();
        CSVArgs CSVArgs { get; }
        bool Save { get; }
    }
}
