﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eyetracker.Views.Abstract
{
   public interface ISelectObjectView
    {
        List<string> Objects { set; }
        string SelectedObject { get; }
        bool Cancel { get; }
        void ShowView();
    }
}
