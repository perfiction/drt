﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eyetracker.Views.Abstract
{
    public interface ITypeObjectNameView
    {
         string ObjectName { get; }
        List<string> ObjectsName { set; }
         void ShowView();
    }
}
