﻿using Emgu.CV.Tracking;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tracking.Model.Settings;
using Tracking.Services.Concrete;
using static Tracking.Helpers.Helper;

namespace Eyetracker
{
    public interface IMainView
    {
        int CurrentFrame { get; set; }
        string FileName { set; }
        int MaxFrame { set; }
        bool ProcessFinish { set; }
        Bitmap Bitmap { get; set; }
        List<Rectangle> BoundingBoxes { get; set; }
        ObservableCollection<string> TrackedObjects { get; set; }
 
        event Action<string> Open;
        event Action<int> SelectedFrameChanged;
        event Action<Rectangle,long,ITrackerSettings> AddObject;
        event Action<string> RemoveObject;       
        event Action<Rectangle> Fix;
        event Action<Rectangle> AddModel;
        event Action<System.Windows.Forms.Form> Play; //TODO: złamanie MVP (JM)
        event Action Pause;
        event Action Stop;
        event Action<int> DetectionMethodChanged;
        event Action<object> ChangeTrackerService;
   
        void ShowMainView();
    }
}
