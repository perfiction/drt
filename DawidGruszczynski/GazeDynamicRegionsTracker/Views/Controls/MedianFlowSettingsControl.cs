﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV.Tracking;
using Emgu.CV.Structure;
using Tracking.Model.Settings;

namespace Eyetracker.Views.Controls
{
    public partial class MedianFlowSettingsControl : UserControl
    {
        public MedianFlowSettingsControl()
        {
            InitializeComponent();
        }
        public ITrackerSettings GetTrackerSettings()
        {
            MCvTermCriteria mCvTermCriteria = new MCvTermCriteria((int)MedianFlowtermCriteriaMaxIteration.Value, (double)MedianFlowtermCriteriaEpsilon.Value);
            var result = new MedianFlowSettings()
            {
                maxLevel = (int)MedianFlowMaxLevel.Value,
                pointsInGrid = (int)MedianFlowPointsInGrid.Value,
                termCriteria = mCvTermCriteria,
                maxMedianLengthOfDisplacementDifference = (double)MedianFlowMaxMedianLengthOfDisplacementDifference.Value,
                winSize = new Size((int)MedianFlowWinSizeWidth.Value, (int)MedianFlowWinSizeHeight.Value),
                winSizeNCC = new Size((int)MedianFlowWinSizeNCCWidth.Value, (int)MedianFlowWinSizeNCCHeight.Value)
            };

            return result;
        }
           
    }
}
