﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV.Tracking;
using Tracking.Model.Settings;

namespace Eyetracker.Views.Controls
{
    public partial class BoostingSettingsControl : UserControl
    {
        public BoostingSettingsControl()
        {
            InitializeComponent();
        }
        public ITrackerSettings GetTrackerSettings()
        {
            var result = new BoostingSettings()
            {
                featureSetNumFeatures = (int)BoostingFeatureSetNumFeatures.Value,
                iterationInit = (int)BoostingIterationInit.Value,
                numClassifiers = (int)BoostNumClassifiers.Value,
                samplerOverlap = (float)BoostingSamplerOverlap.Value,
                samplerSearchFactor = (float)BoostingSamplerSearchFactor.Value
            };
            return result;
        }
    }
}
