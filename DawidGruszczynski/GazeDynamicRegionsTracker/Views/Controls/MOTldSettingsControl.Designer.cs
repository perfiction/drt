﻿namespace Eyetracker.Views.Controls
{
    partial class MOTldSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.infoLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.unsureNud = new System.Windows.Forms.NumericUpDown();
            this.sureNud = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.heightNud = new System.Windows.Forms.NumericUpDown();
            this.widthNud = new System.Windows.Forms.NumericUpDown();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.unsureNud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sureNud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.heightNud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.widthNud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.unsureNud);
            this.groupBox1.Controls.Add(this.sureNud);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.heightNud);
            this.groupBox1.Controls.Add(this.widthNud);
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(258, 320);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // infoLabel
            // 
            this.infoLabel.BackColor = System.Drawing.SystemColors.Info;
            this.infoLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.infoLabel.Location = new System.Drawing.Point(24, 7);
            this.infoLabel.Name = "infoLabel";
            this.infoLabel.Size = new System.Drawing.Size(221, 32);
            this.infoLabel.TabIndex = 9;
            this.infoLabel.Text = "Settings are passed to Multi Object TLD algorithm only when first object is added" +
    ".";
            this.infoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 125);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Unsure threshold:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Sure threshold:";
            // 
            // unsureNud
            // 
            this.unsureNud.DecimalPlaces = 3;
            this.unsureNud.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.unsureNud.Location = new System.Drawing.Point(132, 123);
            this.unsureNud.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.unsureNud.Name = "unsureNud";
            this.unsureNud.Size = new System.Drawing.Size(120, 20);
            this.unsureNud.TabIndex = 5;
            this.unsureNud.Value = new decimal(new int[] {
            35,
            0,
            0,
            131072});
            // 
            // sureNud
            // 
            this.sureNud.DecimalPlaces = 3;
            this.sureNud.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.sureNud.Location = new System.Drawing.Point(132, 97);
            this.sureNud.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sureNud.Name = "sureNud";
            this.sureNud.Size = new System.Drawing.Size(120, 20);
            this.sureNud.TabIndex = 5;
            this.sureNud.Value = new decimal(new int[] {
            7,
            0,
            0,
            65536});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Internal height:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Internal width:";
            // 
            // heightNud
            // 
            this.heightNud.Location = new System.Drawing.Point(132, 45);
            this.heightNud.Maximum = new decimal(new int[] {
            2160,
            0,
            0,
            0});
            this.heightNud.Name = "heightNud";
            this.heightNud.Size = new System.Drawing.Size(120, 20);
            this.heightNud.TabIndex = 1;
            this.heightNud.Value = new decimal(new int[] {
            480,
            0,
            0,
            0});
            // 
            // widthNud
            // 
            this.widthNud.Location = new System.Drawing.Point(132, 19);
            this.widthNud.Maximum = new decimal(new int[] {
            3840,
            0,
            0,
            0});
            this.widthNud.Name = "widthNud";
            this.widthNud.Size = new System.Drawing.Size(120, 20);
            this.widthNud.TabIndex = 0;
            this.widthNud.Value = new decimal(new int[] {
            640,
            0,
            0,
            0});
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(11, 11);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(24, 24);
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Info;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.infoLabel);
            this.panel1.Location = new System.Drawing.Point(6, 266);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(246, 48);
            this.panel1.TabIndex = 11;
            // 
            // MOTldSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "MOTldSettingsControl";
            this.Size = new System.Drawing.Size(262, 325);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.unsureNud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sureNud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.heightNud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.widthNud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown heightNud;
        private System.Windows.Forms.NumericUpDown widthNud;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown unsureNud;
        private System.Windows.Forms.NumericUpDown sureNud;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label infoLabel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
    }
}
