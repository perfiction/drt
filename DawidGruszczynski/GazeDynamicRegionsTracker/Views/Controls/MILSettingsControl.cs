﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV.Tracking;
using Tracking.Model.Settings;

namespace Eyetracker.Views.Controls
{
    public partial class MILSettingsControl : UserControl
    {
        public MILSettingsControl()
        {
            InitializeComponent();
        }

        public ITrackerSettings GetTrackerSettings()
        {
            var result = new MILSettings()
            {
                SamplerInitInRadius = (float)MILSamplerInitInRadius.Value,
                SamplerInitMaxNegNum = (int)MILSamplerInitMaxNegNum.Value,
                SamplerSearchWinSize = (float)MILSamplerSearchWinSize.Value,
                SamplerTrackInRadius = (float)MILSamplerTrackInRadius.Value,
                SamplerTrackMaxPosNum = (int)MILSamplerTrackMaxPosNum.Value,
                FeatureSetNumFeatures = (int)MILFeatureSetNumFeatures.Value,
                SamplerTrackMaxNegNum = (int)MILSamplerTrackMaxNegNum.Value

            };
            return result;
        }
    }
}
