﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV.Tracking;
using Tracking.Model.Settings;

namespace Eyetracker.Views.Controls
{
    public partial class KCFSettingsControl : UserControl
    {
        public KCFSettingsControl()
        {
            InitializeComponent();
            KCFNpca.SelectedIndex = 0;
            KCFDescPca.SelectedIndex = 1;
        }

        public ITrackerSettings GetTrackerSettings()
        {
            TrackerKCF.Mode descPca = TrackerKCF.Mode.Cn;
            TrackerKCF.Mode descNpca = TrackerKCF.Mode.Gray;
            switch (KCFDescPca.SelectedValue)
            {
                case "Gray":
                    descPca = TrackerKCF.Mode.Gray;
                    break;
                case "CN":
                    descPca = TrackerKCF.Mode.Cn;
                    break;
                case "Custom":
                    descPca = TrackerKCF.Mode.Custom;
                    break;
            }
            switch (KCFNpca.SelectedValue)
            {
                case "Gray":
                    descNpca = TrackerKCF.Mode.Gray;
                    break;
                case "CN":
                    descNpca = TrackerKCF.Mode.Cn;
                    break;
                case "Custom":
                    descNpca = TrackerKCF.Mode.Custom;
                    break;
            }
            var result = new KCFSettings()
            {
                CompressedSize = (int)KCFCompressedSize.Value,
                CompressFeature = KCFCompressFeature.Checked,
                DescNpca = descNpca,
                DescPca = descPca,
                DetectThresh = (float)KCFThreshold.Value,
                Lambda = (float) KCFLambda.Value,
                InterpFactor = (float) KCFInterpFactor.Value,
                MaxPatchSize = (int) KCFMaxPatchSize.Value,
                OutputSigmaFactor = (float)KCFOutputSigmaFactor.Value,
                PcaLearningRate = (float)KCFPcaLearningRate.Value,
                Resize = KCFResize.Checked,
                Sigma = (float)KCFSigma.Value,
                SplitCoeff = KCFSplitCoeff.Checked,
                WrapKernel = KCFWrapKernel.Checked

            };
            return result;
        }
        public Tracker GetTracker()
        {
            TrackerKCF.Mode descPca = TrackerKCF.Mode.Cn;
            TrackerKCF.Mode descNpca = TrackerKCF.Mode.Gray;
            switch (KCFDescPca.SelectedValue)
            {
                case "Gray":
                    descPca = TrackerKCF.Mode.Gray;
                    break;
                case "CN":
                    descPca = TrackerKCF.Mode.Cn;
                    break;
                case "Custom":
                    descPca = TrackerKCF.Mode.Custom;
                    break;
            }
            switch (KCFNpca.SelectedValue)
            {
                case "Gray":
                    descNpca = TrackerKCF.Mode.Gray;
                    break;
                case "CN":
                    descNpca = TrackerKCF.Mode.Cn;
                    break;
                case "Custom":
                    descNpca = TrackerKCF.Mode.Custom;
                    break;
            }

            return new TrackerKCF((float)KCFThreshold.Value,(float)KCFSigma.Value,(float)KCFLambda.Value,(float)KCFInterpFactor.Value,(float)KCFOutputSigmaFactor.Value,(float)KCFPcaLearningRate.Value,KCFResize.Checked,KCFSplitCoeff.Checked,KCFWrapKernel.Checked,KCFCompressFeature.Checked,(int)KCFMaxPatchSize.Value,(int)KCFCompressedSize.Value, descPca,descNpca);
        }
        private void Label5_Click(object sender, EventArgs e)
        {

        }

        private void NumericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
