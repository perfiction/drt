﻿namespace Eyetracker.Views.Controls
{
    partial class BoostingSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SamplerOverlap = new System.Windows.Forms.Label();
            this.BoostingSamplerOverlap = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.BoostNumClassifiers = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.BoostingSamplerSearchFactor = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.BoostingIterationInit = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.BoostingFeatureSetNumFeatures = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.BoostingSamplerOverlap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoostNumClassifiers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoostingSamplerSearchFactor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoostingIterationInit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoostingFeatureSetNumFeatures)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // SamplerOverlap
            // 
            this.SamplerOverlap.AutoSize = true;
            this.SamplerOverlap.Location = new System.Drawing.Point(9, 70);
            this.SamplerOverlap.Name = "SamplerOverlap";
            this.SamplerOverlap.Size = new System.Drawing.Size(114, 17);
            this.SamplerOverlap.TabIndex = 7;
            this.SamplerOverlap.Text = "SamplerOverlap ";
            // 
            // BoostingSamplerOverlap
            // 
            this.BoostingSamplerOverlap.DecimalPlaces = 3;
            this.BoostingSamplerOverlap.Location = new System.Drawing.Point(12, 90);
            this.BoostingSamplerOverlap.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.BoostingSamplerOverlap.Name = "BoostingSamplerOverlap";
            this.BoostingSamplerOverlap.Size = new System.Drawing.Size(120, 22);
            this.BoostingSamplerOverlap.TabIndex = 6;
            this.BoostingSamplerOverlap.Value = new decimal(new int[] {
            99,
            0,
            0,
            131072});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "NumClassifiers";
            // 
            // BoostNumClassifiers
            // 
            this.BoostNumClassifiers.Location = new System.Drawing.Point(15, 40);
            this.BoostNumClassifiers.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.BoostNumClassifiers.Name = "BoostNumClassifiers";
            this.BoostNumClassifiers.Size = new System.Drawing.Size(120, 22);
            this.BoostNumClassifiers.TabIndex = 4;
            this.BoostNumClassifiers.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(189, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "SamplerSearchFactor ";
            // 
            // BoostingSamplerSearchFactor
            // 
            this.BoostingSamplerSearchFactor.DecimalPlaces = 2;
            this.BoostingSamplerSearchFactor.Location = new System.Drawing.Point(192, 37);
            this.BoostingSamplerSearchFactor.Name = "BoostingSamplerSearchFactor";
            this.BoostingSamplerSearchFactor.Size = new System.Drawing.Size(120, 22);
            this.BoostingSamplerSearchFactor.TabIndex = 8;
            this.BoostingSamplerSearchFactor.Value = new decimal(new int[] {
            18,
            0,
            0,
            65536});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(189, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "IterationInit";
            // 
            // BoostingIterationInit
            // 
            this.BoostingIterationInit.Location = new System.Drawing.Point(192, 90);
            this.BoostingIterationInit.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.BoostingIterationInit.Name = "BoostingIterationInit";
            this.BoostingIterationInit.Size = new System.Drawing.Size(120, 22);
            this.BoostingIterationInit.TabIndex = 10;
            this.BoostingIterationInit.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(171, 17);
            this.label4.TabIndex = 13;
            this.label4.Text = "FeatureSetNumFeatures  ";
            // 
            // BoostingFeatureSetNumFeatures
            // 
            this.BoostingFeatureSetNumFeatures.Location = new System.Drawing.Point(12, 151);
            this.BoostingFeatureSetNumFeatures.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.BoostingFeatureSetNumFeatures.Name = "BoostingFeatureSetNumFeatures";
            this.BoostingFeatureSetNumFeatures.Size = new System.Drawing.Size(120, 22);
            this.BoostingFeatureSetNumFeatures.TabIndex = 12;
            this.BoostingFeatureSetNumFeatures.Value = new decimal(new int[] {
            1050,
            0,
            0,
            0});
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.BoostingFeatureSetNumFeatures);
            this.groupBox1.Controls.Add(this.BoostingSamplerSearchFactor);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.SamplerOverlap);
            this.groupBox1.Controls.Add(this.BoostingSamplerOverlap);
            this.groupBox1.Controls.Add(this.BoostingIterationInit);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(344, 394);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // BoostSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BoostNumClassifiers);
            this.Controls.Add(this.groupBox1);
            this.Name = "BoostSettingsControl";
            this.Size = new System.Drawing.Size(350, 400);
            ((System.ComponentModel.ISupportInitialize)(this.BoostingSamplerOverlap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoostNumClassifiers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoostingSamplerSearchFactor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoostingIterationInit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoostingFeatureSetNumFeatures)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label SamplerOverlap;
        private System.Windows.Forms.NumericUpDown BoostingSamplerOverlap;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown BoostNumClassifiers;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown BoostingSamplerSearchFactor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown BoostingIterationInit;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown BoostingFeatureSetNumFeatures;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
