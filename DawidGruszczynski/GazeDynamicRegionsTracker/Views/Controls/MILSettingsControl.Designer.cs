﻿namespace Eyetracker.Views.Controls
{
    partial class MILSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.MILSamplerInitInRadius = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.MILSamplerInitMaxNegNum = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.MILSamplerSearchWinSize = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.MILSamplerTrackInRadius = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.MILSamplerTrackMaxPosNum = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.MILSamplerTrackMaxNegNum = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.MILFeatureSetNumFeatures = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.MILSamplerInitInRadius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MILSamplerInitMaxNegNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MILSamplerSearchWinSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MILSamplerTrackInRadius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MILSamplerTrackMaxPosNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MILSamplerTrackMaxNegNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MILFeatureSetNumFeatures)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(185, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 17);
            this.label3.TabIndex = 31;
            this.label3.Text = "SamplerInitInRadius";
            // 
            // MILSamplerInitInRadius
            // 
            this.MILSamplerInitInRadius.Location = new System.Drawing.Point(188, 166);
            this.MILSamplerInitInRadius.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.MILSamplerInitInRadius.Name = "MILSamplerInitInRadius";
            this.MILSamplerInitInRadius.Size = new System.Drawing.Size(120, 22);
            this.MILSamplerInitInRadius.TabIndex = 30;
            this.MILSamplerInitInRadius.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(185, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 17);
            this.label1.TabIndex = 33;
            this.label1.Text = "samplerInitMaxNegNum";
            // 
            // MILSamplerInitMaxNegNum
            // 
            this.MILSamplerInitMaxNegNum.Location = new System.Drawing.Point(188, 37);
            this.MILSamplerInitMaxNegNum.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.MILSamplerInitMaxNegNum.Name = "MILSamplerInitMaxNegNum";
            this.MILSamplerInitMaxNegNum.Size = new System.Drawing.Size(120, 22);
            this.MILSamplerInitMaxNegNum.TabIndex = 32;
            this.MILSamplerInitMaxNegNum.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(156, 17);
            this.label2.TabIndex = 35;
            this.label2.Text = "SamplerSearchWinSize";
            // 
            // MILSamplerSearchWinSize
            // 
            this.MILSamplerSearchWinSize.Location = new System.Drawing.Point(9, 103);
            this.MILSamplerSearchWinSize.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.MILSamplerSearchWinSize.Name = "MILSamplerSearchWinSize";
            this.MILSamplerSearchWinSize.Size = new System.Drawing.Size(120, 22);
            this.MILSamplerSearchWinSize.TabIndex = 34;
            this.MILSamplerSearchWinSize.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(185, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(151, 17);
            this.label4.TabIndex = 37;
            this.label4.Text = "SamplerTrackInRadius";
            // 
            // MILSamplerTrackInRadius
            // 
            this.MILSamplerTrackInRadius.Location = new System.Drawing.Point(188, 103);
            this.MILSamplerTrackInRadius.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.MILSamplerTrackInRadius.Name = "MILSamplerTrackInRadius";
            this.MILSamplerTrackInRadius.Size = new System.Drawing.Size(120, 22);
            this.MILSamplerTrackInRadius.TabIndex = 36;
            this.MILSamplerTrackInRadius.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 146);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(174, 17);
            this.label5.TabIndex = 39;
            this.label5.Text = "SamplerTrackMaxPosNum";
            // 
            // MILSamplerTrackMaxPosNum
            // 
            this.MILSamplerTrackMaxPosNum.Location = new System.Drawing.Point(9, 166);
            this.MILSamplerTrackMaxPosNum.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.MILSamplerTrackMaxPosNum.Name = "MILSamplerTrackMaxPosNum";
            this.MILSamplerTrackMaxPosNum.Size = new System.Drawing.Size(120, 22);
            this.MILSamplerTrackMaxPosNum.TabIndex = 38;
            this.MILSamplerTrackMaxPosNum.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 208);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(176, 17);
            this.label6.TabIndex = 41;
            this.label6.Text = "SamplerTrackMaxNegNum";
            // 
            // MILSamplerTrackMaxNegNum
            // 
            this.MILSamplerTrackMaxNegNum.Location = new System.Drawing.Point(9, 228);
            this.MILSamplerTrackMaxNegNum.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.MILSamplerTrackMaxNegNum.Name = "MILSamplerTrackMaxNegNum";
            this.MILSamplerTrackMaxNegNum.Size = new System.Drawing.Size(120, 22);
            this.MILSamplerTrackMaxNegNum.TabIndex = 40;
            this.MILSamplerTrackMaxNegNum.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 18);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(163, 17);
            this.label7.TabIndex = 43;
            this.label7.Text = "FeatureSetNumFeatures";
            // 
            // MILFeatureSetNumFeatures
            // 
            this.MILFeatureSetNumFeatures.Location = new System.Drawing.Point(9, 37);
            this.MILFeatureSetNumFeatures.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.MILFeatureSetNumFeatures.Name = "MILFeatureSetNumFeatures";
            this.MILFeatureSetNumFeatures.Size = new System.Drawing.Size(120, 22);
            this.MILFeatureSetNumFeatures.TabIndex = 42;
            this.MILFeatureSetNumFeatures.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.MILFeatureSetNumFeatures);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.MILSamplerTrackMaxNegNum);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.MILSamplerTrackMaxPosNum);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.MILSamplerInitInRadius);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.MILSamplerTrackInRadius);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.MILSamplerInitMaxNegNum);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.MILSamplerSearchWinSize);
            this.groupBox1.Location = new System.Drawing.Point(6, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(341, 394);
            this.groupBox1.TabIndex = 44;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // MILSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "MILSettingsControl";
            this.Size = new System.Drawing.Size(350, 400);
            ((System.ComponentModel.ISupportInitialize)(this.MILSamplerInitInRadius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MILSamplerInitMaxNegNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MILSamplerSearchWinSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MILSamplerTrackInRadius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MILSamplerTrackMaxPosNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MILSamplerTrackMaxNegNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MILFeatureSetNumFeatures)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown MILSamplerInitInRadius;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown MILSamplerInitMaxNegNum;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown MILSamplerSearchWinSize;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown MILSamplerTrackInRadius;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown MILSamplerTrackMaxPosNum;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown MILSamplerTrackMaxNegNum;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown MILFeatureSetNumFeatures;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
