﻿namespace Eyetracker.Views.Controls
{
    partial class KCFSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.KCFThreshold = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.KCFSigma = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.KCFLambda = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.KCFInterpFactor = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.KCFOutputSigmaFactor = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.KCFPcaLearningRate = new System.Windows.Forms.NumericUpDown();
            this.KCFResize = new System.Windows.Forms.CheckBox();
            this.KCFSplitCoeff = new System.Windows.Forms.CheckBox();
            this.KCFWrapKernel = new System.Windows.Forms.CheckBox();
            this.KCFCompressFeature = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.KCFMaxPatchSize = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.KCFCompressedSize = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.KCFDescPca = new System.Windows.Forms.ComboBox();
            this.KCFNpca = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.KCFThreshold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KCFSigma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KCFLambda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KCFInterpFactor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KCFOutputSigmaFactor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KCFPcaLearningRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KCFMaxPatchSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KCFCompressedSize)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // KCFThreshold
            // 
            this.KCFThreshold.DecimalPlaces = 3;
            this.KCFThreshold.Location = new System.Drawing.Point(15, 40);
            this.KCFThreshold.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.KCFThreshold.Name = "KCFThreshold";
            this.KCFThreshold.Size = new System.Drawing.Size(120, 22);
            this.KCFThreshold.TabIndex = 0;
            this.KCFThreshold.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Threshold";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Sigma";
            // 
            // KCFSigma
            // 
            this.KCFSigma.DecimalPlaces = 3;
            this.KCFSigma.Location = new System.Drawing.Point(15, 94);
            this.KCFSigma.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.KCFSigma.Name = "KCFSigma";
            this.KCFSigma.Size = new System.Drawing.Size(120, 22);
            this.KCFSigma.TabIndex = 2;
            this.KCFSigma.Value = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Lambda";
            // 
            // KCFLambda
            // 
            this.KCFLambda.DecimalPlaces = 3;
            this.KCFLambda.Location = new System.Drawing.Point(15, 150);
            this.KCFLambda.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.KCFLambda.Name = "KCFLambda";
            this.KCFLambda.Size = new System.Drawing.Size(120, 22);
            this.KCFLambda.TabIndex = 4;
            this.KCFLambda.Value = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 184);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "InterpFactor ";
            // 
            // KCFInterpFactor
            // 
            this.KCFInterpFactor.DecimalPlaces = 3;
            this.KCFInterpFactor.Location = new System.Drawing.Point(15, 204);
            this.KCFInterpFactor.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.KCFInterpFactor.Name = "KCFInterpFactor";
            this.KCFInterpFactor.Size = new System.Drawing.Size(120, 22);
            this.KCFInterpFactor.TabIndex = 6;
            this.KCFInterpFactor.Value = new decimal(new int[] {
            75,
            0,
            0,
            196608});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(208, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(130, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "OutputSigmaFactor";
            this.label5.Click += new System.EventHandler(this.Label5_Click);
            // 
            // KCFOutputSigmaFactor
            // 
            this.KCFOutputSigmaFactor.DecimalPlaces = 4;
            this.KCFOutputSigmaFactor.Location = new System.Drawing.Point(211, 38);
            this.KCFOutputSigmaFactor.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.KCFOutputSigmaFactor.Name = "KCFOutputSigmaFactor";
            this.KCFOutputSigmaFactor.Size = new System.Drawing.Size(120, 22);
            this.KCFOutputSigmaFactor.TabIndex = 8;
            this.KCFOutputSigmaFactor.Value = new decimal(new int[] {
            625,
            0,
            0,
            262144});
            this.KCFOutputSigmaFactor.ValueChanged += new System.EventHandler(this.NumericUpDown1_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(208, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(118, 17);
            this.label6.TabIndex = 11;
            this.label6.Text = "PcaLearningRate";
            // 
            // KCFPcaLearningRate
            // 
            this.KCFPcaLearningRate.DecimalPlaces = 3;
            this.KCFPcaLearningRate.Location = new System.Drawing.Point(211, 92);
            this.KCFPcaLearningRate.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.KCFPcaLearningRate.Name = "KCFPcaLearningRate";
            this.KCFPcaLearningRate.Size = new System.Drawing.Size(120, 22);
            this.KCFPcaLearningRate.TabIndex = 10;
            this.KCFPcaLearningRate.Value = new decimal(new int[] {
            15,
            0,
            0,
            131072});
            // 
            // KCFResize
            // 
            this.KCFResize.AutoSize = true;
            this.KCFResize.Checked = true;
            this.KCFResize.CheckState = System.Windows.Forms.CheckState.Checked;
            this.KCFResize.Location = new System.Drawing.Point(218, 280);
            this.KCFResize.Name = "KCFResize";
            this.KCFResize.Size = new System.Drawing.Size(73, 21);
            this.KCFResize.TabIndex = 17;
            this.KCFResize.Text = "Resize";
            this.KCFResize.UseVisualStyleBackColor = true;
            // 
            // KCFSplitCoeff
            // 
            this.KCFSplitCoeff.AutoSize = true;
            this.KCFSplitCoeff.Checked = true;
            this.KCFSplitCoeff.CheckState = System.Windows.Forms.CheckState.Checked;
            this.KCFSplitCoeff.Location = new System.Drawing.Point(218, 244);
            this.KCFSplitCoeff.Name = "KCFSplitCoeff";
            this.KCFSplitCoeff.Size = new System.Drawing.Size(90, 21);
            this.KCFSplitCoeff.TabIndex = 18;
            this.KCFSplitCoeff.Text = "SplitCoeff";
            this.KCFSplitCoeff.UseVisualStyleBackColor = true;
            // 
            // KCFWrapKernel
            // 
            this.KCFWrapKernel.AutoSize = true;
            this.KCFWrapKernel.Location = new System.Drawing.Point(15, 282);
            this.KCFWrapKernel.Name = "KCFWrapKernel";
            this.KCFWrapKernel.Size = new System.Drawing.Size(105, 21);
            this.KCFWrapKernel.TabIndex = 19;
            this.KCFWrapKernel.Text = "WrapKernel";
            this.KCFWrapKernel.UseVisualStyleBackColor = true;
            // 
            // KCFCompressFeature
            // 
            this.KCFCompressFeature.AutoSize = true;
            this.KCFCompressFeature.Checked = true;
            this.KCFCompressFeature.CheckState = System.Windows.Forms.CheckState.Checked;
            this.KCFCompressFeature.Location = new System.Drawing.Point(15, 246);
            this.KCFCompressFeature.Name = "KCFCompressFeature";
            this.KCFCompressFeature.Size = new System.Drawing.Size(142, 21);
            this.KCFCompressFeature.TabIndex = 20;
            this.KCFCompressFeature.Text = "CompressFeature";
            this.KCFCompressFeature.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(208, 128);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 17);
            this.label7.TabIndex = 22;
            this.label7.Text = "MaxPatchSize ";
            // 
            // KCFMaxPatchSize
            // 
            this.KCFMaxPatchSize.Location = new System.Drawing.Point(210, 148);
            this.KCFMaxPatchSize.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.KCFMaxPatchSize.Name = "KCFMaxPatchSize";
            this.KCFMaxPatchSize.Size = new System.Drawing.Size(120, 22);
            this.KCFMaxPatchSize.TabIndex = 21;
            this.KCFMaxPatchSize.Value = new decimal(new int[] {
            6400,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(208, 182);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(118, 17);
            this.label8.TabIndex = 24;
            this.label8.Text = "CompressedSize ";
            // 
            // KCFCompressedSize
            // 
            this.KCFCompressedSize.Location = new System.Drawing.Point(211, 202);
            this.KCFCompressedSize.Name = "KCFCompressedSize";
            this.KCFCompressedSize.Size = new System.Drawing.Size(120, 22);
            this.KCFCompressedSize.TabIndex = 23;
            this.KCFCompressedSize.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 312);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 17);
            this.label9.TabIndex = 25;
            this.label9.Text = "DescPca";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(207, 312);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 17);
            this.label10.TabIndex = 26;
            this.label10.Text = "DescNpca ";
            // 
            // KCFDescPca
            // 
            this.KCFDescPca.FormattingEnabled = true;
            this.KCFDescPca.Items.AddRange(new object[] {
            "Gray",
            "CN",
            "Custom"});
            this.KCFDescPca.Location = new System.Drawing.Point(12, 332);
            this.KCFDescPca.Name = "KCFDescPca";
            this.KCFDescPca.Size = new System.Drawing.Size(121, 24);
            this.KCFDescPca.TabIndex = 27;
            // 
            // KCFNpca
            // 
            this.KCFNpca.FormattingEnabled = true;
            this.KCFNpca.Items.AddRange(new object[] {
            "Gray",
            "CN",
            "Custom"});
            this.KCFNpca.Location = new System.Drawing.Point(210, 332);
            this.KCFNpca.Name = "KCFNpca";
            this.KCFNpca.Size = new System.Drawing.Size(121, 24);
            this.KCFNpca.TabIndex = 28;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.KCFNpca);
            this.groupBox1.Controls.Add(this.KCFCompressedSize);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.KCFMaxPatchSize);
            this.groupBox1.Controls.Add(this.KCFDescPca);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.KCFSplitCoeff);
            this.groupBox1.Controls.Add(this.KCFOutputSigmaFactor);
            this.groupBox1.Controls.Add(this.KCFResize);
            this.groupBox1.Controls.Add(this.KCFPcaLearningRate);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(344, 394);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // KCFSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.KCFCompressFeature);
            this.Controls.Add(this.KCFWrapKernel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.KCFInterpFactor);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.KCFLambda);
            this.Controls.Add(this.KCFSigma);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.KCFThreshold);
            this.Controls.Add(this.groupBox1);
            this.Name = "KCFSettingsControl";
            this.Size = new System.Drawing.Size(350, 400);
            ((System.ComponentModel.ISupportInitialize)(this.KCFThreshold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KCFSigma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KCFLambda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KCFInterpFactor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KCFOutputSigmaFactor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KCFPcaLearningRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KCFMaxPatchSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KCFCompressedSize)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown KCFThreshold;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown KCFSigma;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown KCFLambda;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown KCFInterpFactor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown KCFOutputSigmaFactor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown KCFPcaLearningRate;
        private System.Windows.Forms.CheckBox KCFResize;
        private System.Windows.Forms.CheckBox KCFSplitCoeff;
        private System.Windows.Forms.CheckBox KCFWrapKernel;
        private System.Windows.Forms.CheckBox KCFCompressFeature;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown KCFMaxPatchSize;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown KCFCompressedSize;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox KCFDescPca;
        private System.Windows.Forms.ComboBox KCFNpca;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
