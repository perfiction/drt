﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tracking.Model.Settings;

namespace Eyetracker.Views.Controls
{
    public partial class CSRTSettingsControl : UserControl
    {
        public CSRTSettingsControl()
        {
            InitializeComponent();
        }
        public ITrackerSettings GetTrackerSettings()
        {
            var settings = new CSRTSettings()
            {
                admmIterations = (int)admmIterations.Value,
                backgroundRatio = (int)backgroundRatio.Value,
                chebAttenuation = (float)chebAttenuation.Value,
                filterLr = (float)filterLr.Value,
                gslSigma = (float)gslSigma.Value,
                histogramBins = (int)histogramBins.Value,
                histogramLr = (float)histogramLr.Value,
                hogClip = (float)hogClip.Value,
                hogOrientations = (float)hogOrientations.Value,
                kaiserAlpha = (float)kaiserAlpha.Value,
                numberOfScales = (int)numberOfScales.Value,
                numHogChannelsUsed = (int)numHogChannelsUsed.Value,
                padding = (float)padding.Value,
                scaleLr = (float)scaleLr.Value,
                scaleModelMaxArea = (float)scaleModelMaxArea.Value,
                scaleSigmaFactor = (float)scaleSigmaFactor.Value,
                scaleStep = (float)scaleStep.Value,
                templateSize = (float)templateSize.Value,
                useChannelWeights = useChannelWeights.Checked,
                useColorNames = useColorNames.Checked,
                useGray = useGray.Checked,
                useHog = useHog.Checked,
                useRgb = useRgb.Checked,
                useSegmentation = useSegmentation.Checked,
                weightsLr = (float)weightsLr.Value,
                windowFunction = null
            };
            return settings;
        }
        }
}
