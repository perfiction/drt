﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV.Tracking;
using Tracking.Model.Settings;

namespace Eyetracker.Views.Controls
{
    public partial class TLDSettingsControl : UserControl
    {
        public TLDSettingsControl()
        {
            InitializeComponent();
        }
        public ITrackerSettings GetTrackerSettings()
        {
            return new TLDSettings();
        }
    }
}
