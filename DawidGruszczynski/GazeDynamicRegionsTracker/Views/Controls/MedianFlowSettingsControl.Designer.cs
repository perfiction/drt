﻿namespace Eyetracker.Views.Controls
{
    partial class MedianFlowSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.MedianFlowPointsInGrid = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.MedianFlowWinSizeWidth = new System.Windows.Forms.NumericUpDown();
            this.MedianFlowWinSizeHeight = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.MedianFlowMaxLevel = new System.Windows.Forms.NumericUpDown();
            this.MedianFlowWinSizeNCCHeight = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.MedianFlowWinSizeNCCWidth = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.MedianFlowMaxMedianLengthOfDisplacementDifference = new System.Windows.Forms.NumericUpDown();
            this.MedianFlowtermCriteriaMaxIteration = new System.Windows.Forms.NumericUpDown();
            this.MedianFlowtermCriteriaEpsilon = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.MedianFlowPointsInGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MedianFlowWinSizeWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MedianFlowWinSizeHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MedianFlowMaxLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MedianFlowWinSizeNCCHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MedianFlowWinSizeNCCWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MedianFlowMaxMedianLengthOfDisplacementDifference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MedianFlowtermCriteriaMaxIteration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MedianFlowtermCriteriaEpsilon)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 138);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(131, 17);
            this.label9.TabIndex = 30;
            this.label9.Text = "TermCriteriaMaxIter";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 17);
            this.label3.TabIndex = 29;
            this.label3.Text = "PointsInGrid";
            // 
            // MedianFlowPointsInGrid
            // 
            this.MedianFlowPointsInGrid.Location = new System.Drawing.Point(22, 42);
            this.MedianFlowPointsInGrid.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.MedianFlowPointsInGrid.Name = "MedianFlowPointsInGrid";
            this.MedianFlowPointsInGrid.Size = new System.Drawing.Size(120, 22);
            this.MedianFlowPointsInGrid.TabIndex = 28;
            this.MedianFlowPointsInGrid.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(217, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 17);
            this.label1.TabIndex = 33;
            this.label1.Text = "WinSize";
            // 
            // MedianFlowWinSizeWidth
            // 
            this.MedianFlowWinSizeWidth.Location = new System.Drawing.Point(220, 39);
            this.MedianFlowWinSizeWidth.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.MedianFlowWinSizeWidth.Name = "MedianFlowWinSizeWidth";
            this.MedianFlowWinSizeWidth.Size = new System.Drawing.Size(56, 22);
            this.MedianFlowWinSizeWidth.TabIndex = 32;
            this.MedianFlowWinSizeWidth.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // MedianFlowWinSizeHeight
            // 
            this.MedianFlowWinSizeHeight.Location = new System.Drawing.Point(282, 39);
            this.MedianFlowWinSizeHeight.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.MedianFlowWinSizeHeight.Name = "MedianFlowWinSizeHeight";
            this.MedianFlowWinSizeHeight.Size = new System.Drawing.Size(56, 22);
            this.MedianFlowWinSizeHeight.TabIndex = 34;
            this.MedianFlowWinSizeHeight.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 17);
            this.label2.TabIndex = 36;
            this.label2.Text = "MaxLevel";
            // 
            // MedianFlowMaxLevel
            // 
            this.MedianFlowMaxLevel.Location = new System.Drawing.Point(22, 100);
            this.MedianFlowMaxLevel.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.MedianFlowMaxLevel.Name = "MedianFlowMaxLevel";
            this.MedianFlowMaxLevel.Size = new System.Drawing.Size(120, 22);
            this.MedianFlowMaxLevel.TabIndex = 35;
            this.MedianFlowMaxLevel.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // MedianFlowWinSizeNCCHeight
            // 
            this.MedianFlowWinSizeNCCHeight.Location = new System.Drawing.Point(282, 97);
            this.MedianFlowWinSizeNCCHeight.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.MedianFlowWinSizeNCCHeight.Name = "MedianFlowWinSizeNCCHeight";
            this.MedianFlowWinSizeNCCHeight.Size = new System.Drawing.Size(56, 22);
            this.MedianFlowWinSizeNCCHeight.TabIndex = 39;
            this.MedianFlowWinSizeNCCHeight.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(217, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 17);
            this.label4.TabIndex = 38;
            this.label4.Text = "WinSizeNCC";
            // 
            // MedianFlowWinSizeNCCWidth
            // 
            this.MedianFlowWinSizeNCCWidth.Location = new System.Drawing.Point(220, 97);
            this.MedianFlowWinSizeNCCWidth.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.MedianFlowWinSizeNCCWidth.Name = "MedianFlowWinSizeNCCWidth";
            this.MedianFlowWinSizeNCCWidth.Size = new System.Drawing.Size(56, 22);
            this.MedianFlowWinSizeNCCWidth.TabIndex = 37;
            this.MedianFlowWinSizeNCCWidth.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 203);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(223, 17);
            this.label5.TabIndex = 41;
            this.label5.Text = "MaxMedianLengthOfDisplacement";
            // 
            // MedianFlowMaxMedianLengthOfDisplacementDifference
            // 
            this.MedianFlowMaxMedianLengthOfDisplacementDifference.DecimalPlaces = 3;
            this.MedianFlowMaxMedianLengthOfDisplacementDifference.Location = new System.Drawing.Point(19, 223);
            this.MedianFlowMaxMedianLengthOfDisplacementDifference.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.MedianFlowMaxMedianLengthOfDisplacementDifference.Name = "MedianFlowMaxMedianLengthOfDisplacementDifference";
            this.MedianFlowMaxMedianLengthOfDisplacementDifference.Size = new System.Drawing.Size(120, 22);
            this.MedianFlowMaxMedianLengthOfDisplacementDifference.TabIndex = 40;
            this.MedianFlowMaxMedianLengthOfDisplacementDifference.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // MedianFlowtermCriteriaMaxIteration
            // 
            this.MedianFlowtermCriteriaMaxIteration.Location = new System.Drawing.Point(19, 158);
            this.MedianFlowtermCriteriaMaxIteration.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.MedianFlowtermCriteriaMaxIteration.Name = "MedianFlowtermCriteriaMaxIteration";
            this.MedianFlowtermCriteriaMaxIteration.Size = new System.Drawing.Size(120, 22);
            this.MedianFlowtermCriteriaMaxIteration.TabIndex = 42;
            this.MedianFlowtermCriteriaMaxIteration.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // MedianFlowtermCriteriaEpsilon
            // 
            this.MedianFlowtermCriteriaEpsilon.DecimalPlaces = 3;
            this.MedianFlowtermCriteriaEpsilon.Location = new System.Drawing.Point(218, 158);
            this.MedianFlowtermCriteriaEpsilon.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.MedianFlowtermCriteriaEpsilon.Name = "MedianFlowtermCriteriaEpsilon";
            this.MedianFlowtermCriteriaEpsilon.Size = new System.Drawing.Size(120, 22);
            this.MedianFlowtermCriteriaEpsilon.TabIndex = 44;
            this.MedianFlowtermCriteriaEpsilon.Value = new decimal(new int[] {
            3,
            0,
            0,
            65536});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(215, 138);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 17);
            this.label6.TabIndex = 43;
            this.label6.Text = "TermCriteriaEps";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.MedianFlowtermCriteriaEpsilon);
            this.groupBox1.Controls.Add(this.MedianFlowWinSizeHeight);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.MedianFlowWinSizeWidth);
            this.groupBox1.Controls.Add(this.MedianFlowtermCriteriaMaxIteration);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.MedianFlowMaxMedianLengthOfDisplacementDifference);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.MedianFlowWinSizeNCCWidth);
            this.groupBox1.Controls.Add(this.MedianFlowWinSizeNCCHeight);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(344, 394);
            this.groupBox1.TabIndex = 45;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // MedianFlowSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.MedianFlowMaxLevel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.MedianFlowPointsInGrid);
            this.Controls.Add(this.groupBox1);
            this.Name = "MedianFlowSettingsControl";
            this.Size = new System.Drawing.Size(350, 400);
            ((System.ComponentModel.ISupportInitialize)(this.MedianFlowPointsInGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MedianFlowWinSizeWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MedianFlowWinSizeHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MedianFlowMaxLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MedianFlowWinSizeNCCHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MedianFlowWinSizeNCCWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MedianFlowMaxMedianLengthOfDisplacementDifference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MedianFlowtermCriteriaMaxIteration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MedianFlowtermCriteriaEpsilon)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown MedianFlowPointsInGrid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown MedianFlowWinSizeWidth;
        private System.Windows.Forms.NumericUpDown MedianFlowWinSizeHeight;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown MedianFlowMaxLevel;
        private System.Windows.Forms.NumericUpDown MedianFlowWinSizeNCCHeight;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown MedianFlowWinSizeNCCWidth;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown MedianFlowMaxMedianLengthOfDisplacementDifference;
        private System.Windows.Forms.NumericUpDown MedianFlowtermCriteriaMaxIteration;
        private System.Windows.Forms.NumericUpDown MedianFlowtermCriteriaEpsilon;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
