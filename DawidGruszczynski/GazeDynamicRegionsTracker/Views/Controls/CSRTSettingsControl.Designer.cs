﻿namespace Eyetracker.Views.Controls
{
    partial class CSRTSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.useHog = new System.Windows.Forms.CheckBox();
            this.useColorNames = new System.Windows.Forms.CheckBox();
            this.useGray = new System.Windows.Forms.CheckBox();
            this.useRgb = new System.Windows.Forms.CheckBox();
            this.useChannelWeights = new System.Windows.Forms.CheckBox();
            this.useSegmentation = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.kaiserAlpha = new System.Windows.Forms.NumericUpDown();
            this.chebAttenuation = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.templateSize = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.gslSigma = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.hogOrientations = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.hogClip = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.padding = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.filterLr = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.weightsLr = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.numHogChannelsUsed = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.admmIterations = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.histogramBins = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.histogramLr = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.backgroundRatio = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.numberOfScales = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.scaleSigmaFactor = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.scaleModelMaxArea = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.scaleLr = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.scaleStep = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.kaiserAlpha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chebAttenuation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gslSigma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hogOrientations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hogClip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.padding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterLr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.weightsLr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHogChannelsUsed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.admmIterations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.histogramBins)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.histogramLr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backgroundRatio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfScales)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scaleSigmaFactor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scaleModelMaxArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scaleLr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scaleStep)).BeginInit();
            this.SuspendLayout();
            // 
            // useHog
            // 
            this.useHog.AutoSize = true;
            this.useHog.Checked = true;
            this.useHog.CheckState = System.Windows.Forms.CheckState.Checked;
            this.useHog.Location = new System.Drawing.Point(3, 30);
            this.useHog.Name = "useHog";
            this.useHog.Size = new System.Drawing.Size(81, 21);
            this.useHog.TabIndex = 0;
            this.useHog.Text = "UseHog";
            this.useHog.UseVisualStyleBackColor = true;
            // 
            // useColorNames
            // 
            this.useColorNames.AutoSize = true;
            this.useColorNames.Checked = true;
            this.useColorNames.CheckState = System.Windows.Forms.CheckState.Checked;
            this.useColorNames.Location = new System.Drawing.Point(3, 3);
            this.useColorNames.Name = "useColorNames";
            this.useColorNames.Size = new System.Drawing.Size(134, 21);
            this.useColorNames.TabIndex = 1;
            this.useColorNames.Text = "useColorNames ";
            this.useColorNames.UseVisualStyleBackColor = true;
            // 
            // useGray
            // 
            this.useGray.AutoSize = true;
            this.useGray.Checked = true;
            this.useGray.CheckState = System.Windows.Forms.CheckState.Checked;
            this.useGray.Location = new System.Drawing.Point(240, 30);
            this.useGray.Name = "useGray";
            this.useGray.Size = new System.Drawing.Size(84, 21);
            this.useGray.TabIndex = 2;
            this.useGray.Text = "useGray";
            this.useGray.UseVisualStyleBackColor = true;
            // 
            // useRgb
            // 
            this.useRgb.AutoSize = true;
            this.useRgb.Location = new System.Drawing.Point(272, 3);
            this.useRgb.Name = "useRgb";
            this.useRgb.Size = new System.Drawing.Size(79, 21);
            this.useRgb.TabIndex = 3;
            this.useRgb.Text = "useRgb";
            this.useRgb.UseVisualStyleBackColor = true;
            // 
            // useChannelWeights
            // 
            this.useChannelWeights.AutoSize = true;
            this.useChannelWeights.Checked = true;
            this.useChannelWeights.CheckState = System.Windows.Forms.CheckState.Checked;
            this.useChannelWeights.Location = new System.Drawing.Point(129, 3);
            this.useChannelWeights.Name = "useChannelWeights";
            this.useChannelWeights.Size = new System.Drawing.Size(137, 21);
            this.useChannelWeights.TabIndex = 4;
            this.useChannelWeights.Text = "useChanWeights";
            this.useChannelWeights.UseVisualStyleBackColor = true;
            // 
            // useSegmentation
            // 
            this.useSegmentation.AutoSize = true;
            this.useSegmentation.Checked = true;
            this.useSegmentation.CheckState = System.Windows.Forms.CheckState.Checked;
            this.useSegmentation.Location = new System.Drawing.Point(90, 30);
            this.useSegmentation.Name = "useSegmentation";
            this.useSegmentation.Size = new System.Drawing.Size(144, 21);
            this.useSegmentation.TabIndex = 5;
            this.useSegmentation.Text = "useSegmentation ";
            this.useSegmentation.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "kaiserAlpha";
            // 
            // kaiserAlpha
            // 
            this.kaiserAlpha.DecimalPlaces = 2;
            this.kaiserAlpha.Location = new System.Drawing.Point(19, 74);
            this.kaiserAlpha.Name = "kaiserAlpha";
            this.kaiserAlpha.Size = new System.Drawing.Size(78, 22);
            this.kaiserAlpha.TabIndex = 7;
            this.kaiserAlpha.Value = new decimal(new int[] {
            375,
            0,
            0,
            131072});
            // 
            // chebAttenuation
            // 
            this.chebAttenuation.DecimalPlaces = 2;
            this.chebAttenuation.Location = new System.Drawing.Point(129, 74);
            this.chebAttenuation.Name = "chebAttenuation";
            this.chebAttenuation.Size = new System.Drawing.Size(78, 22);
            this.chebAttenuation.TabIndex = 9;
            this.chebAttenuation.Value = new decimal(new int[] {
            45,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(126, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "chebAttenuation";
            // 
            // templateSize
            // 
            this.templateSize.DecimalPlaces = 2;
            this.templateSize.Location = new System.Drawing.Point(252, 74);
            this.templateSize.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.templateSize.Name = "templateSize";
            this.templateSize.Size = new System.Drawing.Size(78, 22);
            this.templateSize.TabIndex = 11;
            this.templateSize.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(249, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 17);
            this.label3.TabIndex = 10;
            this.label3.Text = "templateSize";
            // 
            // gslSigma
            // 
            this.gslSigma.DecimalPlaces = 2;
            this.gslSigma.Location = new System.Drawing.Point(19, 119);
            this.gslSigma.Name = "gslSigma";
            this.gslSigma.Size = new System.Drawing.Size(78, 22);
            this.gslSigma.TabIndex = 13;
            this.gslSigma.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 17);
            this.label4.TabIndex = 12;
            this.label4.Text = "gslSigma";
            // 
            // hogOrientations
            // 
            this.hogOrientations.DecimalPlaces = 2;
            this.hogOrientations.Location = new System.Drawing.Point(129, 119);
            this.hogOrientations.Name = "hogOrientations";
            this.hogOrientations.Size = new System.Drawing.Size(78, 22);
            this.hogOrientations.TabIndex = 15;
            this.hogOrientations.Value = new decimal(new int[] {
            9,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(126, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 17);
            this.label5.TabIndex = 14;
            this.label5.Text = "hogOrientations";
            // 
            // hogClip
            // 
            this.hogClip.DecimalPlaces = 2;
            this.hogClip.Location = new System.Drawing.Point(252, 119);
            this.hogClip.Name = "hogClip";
            this.hogClip.Size = new System.Drawing.Size(78, 22);
            this.hogClip.TabIndex = 17;
            this.hogClip.Value = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(249, 99);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 17);
            this.label6.TabIndex = 16;
            this.label6.Text = "hogClip";
            // 
            // padding
            // 
            this.padding.DecimalPlaces = 2;
            this.padding.Location = new System.Drawing.Point(19, 164);
            this.padding.Name = "padding";
            this.padding.Size = new System.Drawing.Size(78, 22);
            this.padding.TabIndex = 19;
            this.padding.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 144);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 17);
            this.label7.TabIndex = 18;
            this.label7.Text = "padding";
            // 
            // filterLr
            // 
            this.filterLr.DecimalPlaces = 2;
            this.filterLr.Location = new System.Drawing.Point(129, 164);
            this.filterLr.Name = "filterLr";
            this.filterLr.Size = new System.Drawing.Size(78, 22);
            this.filterLr.TabIndex = 21;
            this.filterLr.Value = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(126, 144);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 17);
            this.label8.TabIndex = 20;
            this.label8.Text = "filterLr";
            // 
            // weightsLr
            // 
            this.weightsLr.DecimalPlaces = 2;
            this.weightsLr.Location = new System.Drawing.Point(252, 164);
            this.weightsLr.Name = "weightsLr";
            this.weightsLr.Size = new System.Drawing.Size(78, 22);
            this.weightsLr.TabIndex = 23;
            this.weightsLr.Value = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(249, 144);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 17);
            this.label9.TabIndex = 22;
            this.label9.Text = "weightsLr";
            // 
            // numHogChannelsUsed
            // 
            this.numHogChannelsUsed.Location = new System.Drawing.Point(20, 209);
            this.numHogChannelsUsed.Name = "numHogChannelsUsed";
            this.numHogChannelsUsed.Size = new System.Drawing.Size(78, 22);
            this.numHogChannelsUsed.TabIndex = 25;
            this.numHogChannelsUsed.Value = new decimal(new int[] {
            18,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(17, 189);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(153, 17);
            this.label10.TabIndex = 24;
            this.label10.Text = "numHogChannelsUsed";
            // 
            // admmIterations
            // 
            this.admmIterations.Location = new System.Drawing.Point(196, 209);
            this.admmIterations.Name = "admmIterations";
            this.admmIterations.Size = new System.Drawing.Size(78, 22);
            this.admmIterations.TabIndex = 27;
            this.admmIterations.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(193, 189);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(104, 17);
            this.label11.TabIndex = 26;
            this.label11.Text = "admmIterations";
            // 
            // histogramBins
            // 
            this.histogramBins.Location = new System.Drawing.Point(134, 363);
            this.histogramBins.Name = "histogramBins";
            this.histogramBins.Size = new System.Drawing.Size(78, 22);
            this.histogramBins.TabIndex = 29;
            this.histogramBins.Value = new decimal(new int[] {
            16,
            0,
            0,
            0});
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(131, 343);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(97, 17);
            this.label12.TabIndex = 28;
            this.label12.Text = "histogramBins";
            // 
            // histogramLr
            // 
            this.histogramLr.DecimalPlaces = 2;
            this.histogramLr.Location = new System.Drawing.Point(20, 254);
            this.histogramLr.Name = "histogramLr";
            this.histogramLr.Size = new System.Drawing.Size(78, 22);
            this.histogramLr.TabIndex = 31;
            this.histogramLr.Value = new decimal(new int[] {
            4,
            0,
            0,
            131072});
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(17, 234);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 17);
            this.label13.TabIndex = 30;
            this.label13.Text = "histogramLr";
            // 
            // backgroundRatio
            // 
            this.backgroundRatio.Location = new System.Drawing.Point(118, 254);
            this.backgroundRatio.Name = "backgroundRatio";
            this.backgroundRatio.Size = new System.Drawing.Size(78, 22);
            this.backgroundRatio.TabIndex = 33;
            this.backgroundRatio.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(115, 234);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(116, 17);
            this.label14.TabIndex = 32;
            this.label14.Text = "backgroundRatio";
            // 
            // numberOfScales
            // 
            this.numberOfScales.Location = new System.Drawing.Point(237, 254);
            this.numberOfScales.Name = "numberOfScales";
            this.numberOfScales.Size = new System.Drawing.Size(78, 22);
            this.numberOfScales.TabIndex = 35;
            this.numberOfScales.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(234, 234);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(113, 17);
            this.label15.TabIndex = 34;
            this.label15.Text = "numberOfScales";
            // 
            // scaleSigmaFactor
            // 
            this.scaleSigmaFactor.DecimalPlaces = 2;
            this.scaleSigmaFactor.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.scaleSigmaFactor.Location = new System.Drawing.Point(20, 309);
            this.scaleSigmaFactor.Name = "scaleSigmaFactor";
            this.scaleSigmaFactor.Size = new System.Drawing.Size(78, 22);
            this.scaleSigmaFactor.TabIndex = 37;
            this.scaleSigmaFactor.Value = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(17, 289);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(120, 17);
            this.label16.TabIndex = 36;
            this.label16.Text = "scaleSigmaFactor";
            // 
            // scaleModelMaxArea
            // 
            this.scaleModelMaxArea.DecimalPlaces = 2;
            this.scaleModelMaxArea.Location = new System.Drawing.Point(134, 309);
            this.scaleModelMaxArea.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.scaleModelMaxArea.Name = "scaleModelMaxArea";
            this.scaleModelMaxArea.Size = new System.Drawing.Size(78, 22);
            this.scaleModelMaxArea.TabIndex = 39;
            this.scaleModelMaxArea.Value = new decimal(new int[] {
            512,
            0,
            0,
            0});
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(131, 289);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(134, 17);
            this.label17.TabIndex = 38;
            this.label17.Text = "scaleModelMaxArea";
            // 
            // scaleLr
            // 
            this.scaleLr.DecimalPlaces = 3;
            this.scaleLr.Location = new System.Drawing.Point(269, 309);
            this.scaleLr.Name = "scaleLr";
            this.scaleLr.Size = new System.Drawing.Size(78, 22);
            this.scaleLr.TabIndex = 41;
            this.scaleLr.Value = new decimal(new int[] {
            25,
            0,
            0,
            196608});
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(266, 289);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(54, 17);
            this.label18.TabIndex = 40;
            this.label18.Text = "scaleLr";
            // 
            // scaleStep
            // 
            this.scaleStep.DecimalPlaces = 2;
            this.scaleStep.Location = new System.Drawing.Point(20, 363);
            this.scaleStep.Name = "scaleStep";
            this.scaleStep.Size = new System.Drawing.Size(78, 22);
            this.scaleStep.TabIndex = 43;
            this.scaleStep.Value = new decimal(new int[] {
            102,
            0,
            0,
            131072});
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(17, 343);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(70, 17);
            this.label19.TabIndex = 42;
            this.label19.Text = "scaleStep";
            // 
            // CSRTSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.scaleStep);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.scaleLr);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.scaleModelMaxArea);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.scaleSigmaFactor);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.numberOfScales);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.backgroundRatio);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.histogramLr);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.histogramBins);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.admmIterations);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.numHogChannelsUsed);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.weightsLr);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.filterLr);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.padding);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.hogClip);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.hogOrientations);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.gslSigma);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.templateSize);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.chebAttenuation);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.kaiserAlpha);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.useSegmentation);
            this.Controls.Add(this.useChannelWeights);
            this.Controls.Add(this.useRgb);
            this.Controls.Add(this.useGray);
            this.Controls.Add(this.useColorNames);
            this.Controls.Add(this.useHog);
            this.Name = "CSRTSettingsControl";
            this.Size = new System.Drawing.Size(350, 400);
            ((System.ComponentModel.ISupportInitialize)(this.kaiserAlpha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chebAttenuation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gslSigma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hogOrientations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hogClip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.padding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterLr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.weightsLr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHogChannelsUsed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.admmIterations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.histogramBins)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.histogramLr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backgroundRatio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfScales)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scaleSigmaFactor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scaleModelMaxArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scaleLr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scaleStep)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox useHog;
        private System.Windows.Forms.CheckBox useColorNames;
        private System.Windows.Forms.CheckBox useGray;
        private System.Windows.Forms.CheckBox useRgb;
        private System.Windows.Forms.CheckBox useChannelWeights;
        private System.Windows.Forms.CheckBox useSegmentation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown kaiserAlpha;
        private System.Windows.Forms.NumericUpDown chebAttenuation;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown templateSize;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown gslSigma;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown hogOrientations;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown hogClip;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown padding;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown filterLr;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown weightsLr;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numHogChannelsUsed;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown admmIterations;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown histogramBins;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown histogramLr;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown backgroundRatio;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown numberOfScales;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown scaleSigmaFactor;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown scaleModelMaxArea;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown scaleLr;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown scaleStep;
        private System.Windows.Forms.Label label19;
    }
}
