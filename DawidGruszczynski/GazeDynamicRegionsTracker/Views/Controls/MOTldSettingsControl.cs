﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tracking.Model.Settings;

namespace Eyetracker.Views.Controls
{
    public partial class MOTldSettingsControl : UserControl
    {
        public MOTldSettingsControl()
        {
            InitializeComponent();

            Icon infoIcon = new Icon(SystemIcons.Information, 32, 32);
            pictureBox1.Image = new Bitmap(infoIcon.ToBitmap(), 24, 24);
        }

        public ITrackerSettings GetTrackerSettings()
        {
            var result = new MOTldSettings()
            {
                InternalWidth = (int)widthNud.Value,
                InternalHeight = (int)heightNud.Value,
                SureThreshold = (float)sureNud.Value,
                UnsureThreshold = (float)unsureNud.Value
            };
            return result;
        }
    }
}
