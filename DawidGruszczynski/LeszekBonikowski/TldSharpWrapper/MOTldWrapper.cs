﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

namespace TldSharpWrapper
{
    #region Marshallowane typy/struktury z MOTLD

    [StructLayout(LayoutKind.Sequential)]
    public struct ObjectBox
    {
        public float x;
        public float y;
        public float width;
        public float height;
        public int objectId;
    }

    public enum TldStatus
    {
        Lost,
        Unsure,
        Ok
    }

    #endregion

    public class MOTldWrapper : IDisposable
    {
        internal static class NativeMethods
        {
            [DllImport("MOTLD.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern bool init(int width, int height);
            
            [DllImport("MOTLD.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern bool finalize();

            [DllImport("MOTLD.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern int addObject(ObjectBox b);

            [DllImport("MOTLD.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern bool fixObject(int id, ObjectBox b);

            [DllImport("MOTLD.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern bool updateTolerances(float sure, float unsure);

            [DllImport("MOTLD.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern void processFrame(byte[] frame);

            [DllImport("MOTLD.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern void enableLearning(bool enable);

            [DllImport("MOTLD.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern int getStatus(int objId);

            [DllImport("MOTLD.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern int getObjectCount();

            [DllImport("MOTLD.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern IntPtr getObjectBoxes();

            [DllImport("MOTLD.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern ObjectBox getObjectBox(int objId);

        }

        public int Width { get; private set; }
        public int Height { get; private set; }

        private int _baseWidth, _baseHeight;

        public MOTldWrapper(int width, int height)
        {
            Width = width;
            Height = height;
            _baseWidth = 0;
            _baseHeight = 0;
            bool result = true;

            try
            {
                result = NativeMethods.init(width, height);
            }
            catch (DllNotFoundException e)
            {
                throw new DllNotFoundException("MOTLD.dll not found. Compile the \"MOTLD\" project and copy the output library to Eyetracker.exe directory.\nStack Trace:\n" + e.StackTrace);
            }

            if (!result)
                throw new ExternalException("Could not create MOTLD instance (already initialized)");
        }

        public int AddObject(ObjectBox b)
        {
            return NativeMethods.addObject(b);
        }

        public int AddObject(float x, float y, float width, float height)
        {
            float ratioX = Width / (float)_baseWidth;
            float ratioY = Height / (float)_baseHeight;

            ObjectBox box = new ObjectBox
            {
                x = x * ratioX,
                y = y * ratioY,
                width = width * ratioX,
                height = height * ratioY,
                objectId = 0,
            };

            return NativeMethods.addObject(box);
        }

        public bool FixObject(int id, ObjectBox b)
        {
            return NativeMethods.fixObject(id, b);
        }

        public bool FixObject(int id, float x, float y, float width, float height)
        {
            float ratioX = Width / (float)_baseWidth;
            float ratioY = Height / (float)_baseHeight;

            ObjectBox box = new ObjectBox
            {
                x = x * ratioX,
                y = y * ratioY,
                width = width * ratioX,
                height = height * ratioY,
                objectId = 0,
            };

            return FixObject(id, box);
        }

        public void UpdateTolerances(float sureTolerance, float unsureTolerance)
        {
            bool result = NativeMethods.updateTolerances(sureTolerance, unsureTolerance);
            if(!result)
                throw new ExternalException("Attempted to update tolerances, but TLD instance is not created yet");
        }

        public void ProcessFrame(byte[] frame)
        {
            NativeMethods.processFrame(frame);
        }

        public void ProcessFrame(Mat frame)
        {
            if (_baseWidth == 0 && _baseHeight == 0)
                SaveMatDimensions(frame.Width, frame.Height);

            Mat resizedFrame = new Mat(Width, Height, DepthType.Cv8U, 3);
            CvInvoke.Resize(frame, resizedFrame, new Size(Width, Height));
            Image<Bgr, byte> image = resizedFrame.ToImage<Bgr, byte>();
            int size = Width * Height;

            // (LB) Array in [RRRR GGGG BBBB] format
            byte[] img = new byte[(long)resizedFrame.Total * resizedFrame.NumberOfChannels];

            for (int y = 0; y < resizedFrame.Height; y++)
            {
                for (int x = 0; x < resizedFrame.Width; x++)
                {
                    img[(y * resizedFrame.Width + x)] = image.Data[y, x, 2];
                    img[(y * resizedFrame.Width + x) + size] = image.Data[y, x, 1];
                    img[(y * resizedFrame.Width + x) + (2 * size)] = image.Data[y, x, 0];
                }
            }

            NativeMethods.processFrame(img);
        }

        public void EnableLearning(bool enable = true)
        {
            NativeMethods.enableLearning(enable);
        }

        public TldStatus GetStatus(int objId)
        {
            return (TldStatus)NativeMethods.getStatus(objId);
        }

        public int GetObjectCount()
        {
            return NativeMethods.getObjectCount();
        }

        public List<ObjectBox> GetObjectBoxes()
        {
            int count = GetObjectCount();
            List<ObjectBox> output = new List<ObjectBox>(count);

            int structSize = Marshal.SizeOf(typeof(ObjectBox));
            IntPtr ptr = NativeMethods.getObjectBoxes();

            float ratioX = _baseWidth / (float)Width;
            float ratioY = _baseHeight / (float)Height;

            for (int i = 0; i < count; i++)
            {
                IntPtr structPtr = new IntPtr(ptr.ToInt64() + structSize * i);
                ObjectBox tmp = (ObjectBox)Marshal.PtrToStructure(structPtr, typeof(ObjectBox));

                //scale to original dimensions
                output.Add(new ObjectBox()
                {
                    x = ratioX * tmp.x,
                    y = ratioY * tmp.y,
                    width = ratioX * tmp.width,
                    height = ratioY * tmp.height,
                    objectId = tmp.objectId
                });
            }

            return output;
        }

        public ObjectBox GetObjectBox(int objId)
        {
            float ratioX = _baseWidth / (float)Width;
            float ratioY = _baseHeight / (float)Height;

            var b = NativeMethods.getObjectBox(objId);

            return new ObjectBox()
            {
                x = ratioX * b.x,
                y = ratioY * b.y,
                width = ratioX * b.width,
                height = ratioY * b.height,
                objectId = b.objectId
            };
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                bool result = NativeMethods.finalize();
                if (!result)
                    throw new ExternalException("Could not delete TLD instance (already disposed)");
            }

            _baseWidth = _baseHeight = 0;
        }

        ~MOTldWrapper()
        {
            Dispose(false);
        }

        private void SaveMatDimensions(int width, int height)
        {
            _baseWidth = width;
            _baseHeight = height;
        }
    }
}