// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include "MultiObjectTLD.h"

#define DllExport __declspec(dllexport)

static MultiObjectTLD* tldInstance;

extern "C" DllExport bool init(int width, int height);
extern "C" DllExport bool finalize();
extern "C" DllExport int addObject(ObjectBox b);
extern "C" DllExport bool fixObject(int id, ObjectBox b);
extern "C" DllExport bool updateTolerances(float sure, float unsure);
extern "C" DllExport void processFrame(unsigned char* img);
extern "C" DllExport void enableLearning(bool enable);
extern "C" DllExport int getStatus(int objId);
extern "C" DllExport int getObjectCount();
extern "C" DllExport ObjectBox * getObjectBoxes();
extern "C" DllExport ObjectBox getObjectBox(int objId);


BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
	default:
		break;
	}
	return TRUE;
}

extern "C" DllExport bool init(int width, int height)
{
	if (tldInstance != nullptr)
		return false;

	MOTLDSettings settings(COLOR_MODE_RGB);
	settings.useColor = true;

	tldInstance = new MultiObjectTLD(width, height, settings);

	return true;
}

extern "C" DllExport bool finalize()
{
	if (tldInstance == nullptr)
		return false;

	delete tldInstance;
	tldInstance = nullptr;

	return true;
}

extern "C" DllExport int addObject(ObjectBox b)
{
	return tldInstance->addObject(b);
}

extern "C" DllExport bool fixObject(int id, ObjectBox b)
{
	return tldInstance->fixObject(id, b);
}

extern "C" DllExport bool updateTolerances(float sure, float unsure)
{
	if (tldInstance) {
		tldInstance->sureTolerance = sure;
		tldInstance->unsureTolerance = unsure;
		return true;
	}
	else
		return false;
}

extern "C" DllExport void processFrame(unsigned char* img)
{
	tldInstance->processFrame(img);
}

extern "C" DllExport void enableLearning(bool enable = true)
{
	tldInstance->enableLearning(enable);
}

extern "C" DllExport int getStatus(int objId = 0)
{
	return tldInstance->getStatus(objId);
}

extern "C" DllExport int getObjectCount()
{
	return tldInstance->getObjectCount();
}

extern "C" DllExport ObjectBox * getObjectBoxes()
{
	std::vector<ObjectBox> boxesVect = tldInstance->getObjectBoxes();
	const int count = boxesVect.size();
	int i;

	ObjectBox* output = (ObjectBox*)malloc(count * sizeof(ObjectBox));

	for (i = 0; i < count; i++)
		output[i] = boxesVect.at(i);

	return output;
}

extern "C" DllExport ObjectBox getObjectBox(int objId)
{
	return tldInstance->getObjectBox(objId);
}