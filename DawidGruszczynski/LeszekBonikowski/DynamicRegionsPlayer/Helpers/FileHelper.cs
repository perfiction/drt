﻿using GameLab.Eyetracking.Data.DynamicRegions;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;

namespace DynamicRegionsPlayer.Helpers
{
    public static class FileHelper
    {
        public static List<RectangleF>[] LoadCoordinates(string filePath)
        {
            if (filePath.EndsWith(".csv"))
            {
                return FromDynamicRegions(filePath);
            }
            else if (filePath.EndsWith(".txt"))
            {
                return ParseTxtFile(filePath);
            }
            else throw new FileLoadException("Unsupported file format: " + filePath.Substring(filePath.Length - 4, 4));
        }

        private static RectangleF FromGameLabPrimitive(GameLab.Eyetracking.Geometry.Rectangle r)
        {
            return new RectangleF(r.Left, r.Top, r.Width, r.Height);
        }

        public static List<RectangleF>[] FromDynamicRegions(string filePath)
        {
            string errorMsg = string.Empty;
            List<DynamicRegion> regions = GazeDynamicRegionsHelper.LoadDynamicRegionsFromFile(filePath, ';', ref errorMsg, System.Globalization.CultureInfo.InvariantCulture);

            List<RectangleF>[] boxes = new List<RectangleF>[regions.Count];

            for (int i = 0; i < regions.Count; i++)
            {
                boxes[i] = new List<RectangleF>();
                foreach (var frame in regions[i].Frames)
                {
                    boxes[i].Add(FromGameLabPrimitive(frame.Border));
                }

            }

            return boxes;
        }

        public static List<RectangleF>[] ParseTxtFile(string filePath)
        {
            CultureInfo culture = CultureInfo.CurrentCulture;
            CultureInfo.CurrentCulture = CultureInfo.InvariantCulture;

            List<RectangleF>[] output = null;

            if (CheckValueCount(filePath, 4) || CheckValueCount(filePath, 5))
                output = ParseTldFile(filePath);

            CultureInfo.CurrentCulture = culture;

            if(output != null)
                return output;

            throw new InvalidDataException("Unsupported file format. Supported formats:\n\n- GDE .csv\n- TLD Dataset file (4 comma-separated values)");
        }

        private static bool CheckValueCount(string filePath, int count)
        {
            // left, top, right, bottom
            using (StreamReader reader = new StreamReader(filePath))
            {
                string line = reader.ReadLine();
                return line.Split(',').Length == count;
            }
        }

        private static List<RectangleF>[] ParseTldFile(string filePath)
        {
            List<RectangleF>[] boxes = new List<RectangleF>[1];
            boxes[0] = new List<RectangleF>();

            using (StreamReader reader = new StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    string[] coordsStr = line.Split(',');

                    float x = float.Parse(coordsStr[0]);
                    if (float.IsNaN(x)) x = 0f;

                    float y = float.Parse(coordsStr[1]);
                    if (float.IsNaN(y)) y = 0f;

                    float width = float.Parse(coordsStr[2]) - x;
                    if (float.IsNaN(width)) width = 0f;

                    float height = float.Parse(coordsStr[3]) - y;
                    if (float.IsNaN(height)) height = 0f;

                    boxes[0].Add(new RectangleF(x, y, width, height));
                }
            }

            return boxes;
        }

        private static List<RectangleF>[] ParseOtbFile(string filePath)
        {
            List<List<RectangleF>> boxes = new List<List<RectangleF>>();

            using (StreamReader reader = new StreamReader(filePath))
            {
                string line;
                while((line = reader.ReadLine()) != null)
                {
                    string[] coordStr = line.Split(',');
                    int objId = int.Parse(coordStr[0]);

                    float x = float.Parse(coordStr[1]);
                    float y = float.Parse(coordStr[2]);
                    float width = float.Parse(coordStr[3]);
                    float height = float.Parse(coordStr[4]);

                    if (boxes.Count < objId)
                        boxes.Add(new List<RectangleF>());

                    boxes[objId - 1].Add(new RectangleF(x, y, width, height));
                }
            }

            return boxes.ToArray();
        }


        private static List<RectangleF>[] ParseMotFile(string filePath)
        {
            List<List<RectangleF>> boxes = new List<List<RectangleF>>();
            List<(int, RectangleF)> inits = new List<(int, RectangleF)>();
            int framesCount = 0;

            using (StreamReader reader = new StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    string[] coordStr = line.Split(',');

                    int frame = int.Parse(coordStr[0]);
                    int objId = int.Parse(coordStr[1]);
                    if(frame > framesCount) framesCount = frame;

                    float x = float.Parse(coordStr[2]);
                    float y = float.Parse(coordStr[3]);
                    float width = float.Parse(coordStr[4]);
                    float height = float.Parse(coordStr[5]);

                    if (boxes.Count < objId)
                        boxes.Add(new List<RectangleF>());

                    if(inits.Count < objId)
                        inits.Add((frame - 1, new RectangleF(x, y, width, height)));

                    while (boxes[objId - 1].Count < frame - 1)
                        boxes[objId - 1].Add(new RectangleF(0f, 0f, 0f, 0f));

                    boxes[objId - 1].Add(new RectangleF(x, y, width, height));
                }
            }

            // (LB) Put zeroes in place of missing coordinates, it doesn't impact calculations
            foreach(List<RectangleF> b in boxes)
            {
                while(b.Count < framesCount)
                    b.Add(new RectangleF(0f, 0f, 0f, 0f));
            }

            return boxes.ToArray();
        }
    }
}
