﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace DynamicRegionsPlayer
{
    public static class EvaluationHelper
    {
        public static List<(float, float, float)> Evaluate(List<List<RectangleF>[]> boxes, List<RectangleF>[] groundTruth, float intersection)
        {
            if (boxes[0][0].Count != groundTruth[0].Count)
                throw new ArgumentException("Ground truth and output bounding box collections must have the same length.\n" +
                    "(Ground truth count: " + groundTruth[0].Count + ", Output count: " + boxes[0][0].Count + ")");

            // (LB) Metrics based on chapter 6.3 (p. 11) of the TLD publication
            // Metrics are averaged when multiple objects are tracked
            List<(float, float, float)> evaluation = new List<(float, float, float)>();

            foreach (var method in boxes)
            {
                float totalPrecision = 0f, totalRecall = 0f, totalFmeasure = 0f;
                int objectCount = method.Length;

                for (int i = 0; i < objectCount; i++)
                {
                    float precision = 0f, recall = 0f, fMeasure = 0f;

                    var obj = method[i];
                    int responses = 0, truePositives = 0, shouldDetect = 0;
                    for (int j = 0; j < obj.Count; j++)
                    {
                        if (IsVisible(groundTruth[i][j]))
                            shouldDetect++;
                        if (IsVisible(obj[j]))
                            responses++;
                        if (IsVisible(obj[j]) && IsVisible(groundTruth[i][i]) && IntersectionRatio(obj[j], groundTruth[i][j]) > intersection)
                            truePositives++;
                    }

                    precision = truePositives / (float)responses;
                    recall = truePositives / (float)shouldDetect;
                    fMeasure = (2 * precision * recall) / (precision + recall);

                    totalPrecision += precision;
                    totalRecall += recall;
                    totalFmeasure += float.IsNaN(fMeasure) ? 0 : fMeasure;
                }

                evaluation.Add((totalPrecision / objectCount, totalRecall / objectCount, totalFmeasure / objectCount));
            }

            return evaluation;
        }



        private static bool IsVisible(RectangleF r)
        {
            return !(r.X == 0 && r.Y == 0 && r.Width == 0 && r.Height == 0);
        }

        private static float IntersectionRatio(RectangleF r1, RectangleF r2)
        {
            RectangleF intersection = new RectangleF(r1.X, r1.Y, r1.Width, r1.Height);
            intersection.Intersect(r2);

            return (intersection.Width * intersection.Height) / (r2.Width * r2.Height);
        }
    }
}
