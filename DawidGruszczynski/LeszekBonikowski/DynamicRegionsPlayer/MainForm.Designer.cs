﻿namespace DynamicRegionsPlayer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openVideoFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openGroundTruthFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openRegionsFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.performanceEvaluationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.playbackPictureBox = new System.Windows.Forms.PictureBox();
            this.playPauseButton = new System.Windows.Forms.Button();
            this.stopButton = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.playbackSpeedNud = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.prevFrameBtn = new System.Windows.Forms.Button();
            this.nextFrameBtn = new System.Windows.Forms.Button();
            this.evalFromcsvToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playbackPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playbackSpeedNud)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.performanceEvaluationToolStripMenuItem,
            this.evalFromcsvToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(984, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openVideoFileToolStripMenuItem,
            this.openGroundTruthFileToolStripMenuItem,
            this.openRegionsFileToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openVideoFileToolStripMenuItem
            // 
            this.openVideoFileToolStripMenuItem.Name = "openVideoFileToolStripMenuItem";
            this.openVideoFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openVideoFileToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.openVideoFileToolStripMenuItem.Text = "1. Open video file";
            this.openVideoFileToolStripMenuItem.Click += new System.EventHandler(this.openVideoFileToolStripMenuItem_Click);
            // 
            // openGroundTruthFileToolStripMenuItem
            // 
            this.openGroundTruthFileToolStripMenuItem.Enabled = false;
            this.openGroundTruthFileToolStripMenuItem.Name = "openGroundTruthFileToolStripMenuItem";
            this.openGroundTruthFileToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.openGroundTruthFileToolStripMenuItem.Text = "2. Open ground truth file";
            this.openGroundTruthFileToolStripMenuItem.Click += new System.EventHandler(this.openGroundTruthFileToolStripMenuItem_Click);
            // 
            // openRegionsFileToolStripMenuItem
            // 
            this.openRegionsFileToolStripMenuItem.Enabled = false;
            this.openRegionsFileToolStripMenuItem.Name = "openRegionsFileToolStripMenuItem";
            this.openRegionsFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.openRegionsFileToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.openRegionsFileToolStripMenuItem.Text = "3. Open regions file";
            this.openRegionsFileToolStripMenuItem.Click += new System.EventHandler(this.openRegionsFileToolStripMenuItem_Click);
            // 
            // performanceEvaluationToolStripMenuItem
            // 
            this.performanceEvaluationToolStripMenuItem.Enabled = false;
            this.performanceEvaluationToolStripMenuItem.Name = "performanceEvaluationToolStripMenuItem";
            this.performanceEvaluationToolStripMenuItem.Size = new System.Drawing.Size(145, 20);
            this.performanceEvaluationToolStripMenuItem.Text = "Performance Evaluation";
            this.performanceEvaluationToolStripMenuItem.Click += new System.EventHandler(this.performanceEvaluationToolStripMenuItem_Click);
            // 
            // playbackPictureBox
            // 
            this.playbackPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.playbackPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.playbackPictureBox.Location = new System.Drawing.Point(12, 27);
            this.playbackPictureBox.Name = "playbackPictureBox";
            this.playbackPictureBox.Size = new System.Drawing.Size(960, 540);
            this.playbackPictureBox.TabIndex = 1;
            this.playbackPictureBox.TabStop = false;
            this.playbackPictureBox.SizeChanged += new System.EventHandler(this.playbackPictureBox_SizeChanged);
            this.playbackPictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.playbackPictureBox_Paint);
            // 
            // playPauseButton
            // 
            this.playPauseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.playPauseButton.Enabled = false;
            this.playPauseButton.Location = new System.Drawing.Point(12, 582);
            this.playPauseButton.Name = "playPauseButton";
            this.playPauseButton.Size = new System.Drawing.Size(85, 23);
            this.playPauseButton.TabIndex = 2;
            this.playPauseButton.Text = "Play";
            this.playPauseButton.UseVisualStyleBackColor = true;
            this.playPauseButton.Click += new System.EventHandler(this.playPauseButton_Click);
            // 
            // stopButton
            // 
            this.stopButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.stopButton.Enabled = false;
            this.stopButton.Location = new System.Drawing.Point(887, 582);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(85, 23);
            this.stopButton.TabIndex = 3;
            this.stopButton.Text = "Stop";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // playbackSpeedNud
            // 
            this.playbackSpeedNud.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.playbackSpeedNud.Location = new System.Drawing.Point(287, 3);
            this.playbackSpeedNud.Maximum = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.playbackSpeedNud.Minimum = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.playbackSpeedNud.Name = "playbackSpeedNud";
            this.playbackSpeedNud.Size = new System.Drawing.Size(107, 20);
            this.playbackSpeedNud.TabIndex = 5;
            this.playbackSpeedNud.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.playbackSpeedNud.ValueChanged += new System.EventHandler(this.playbackSpeedNud_ValueChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(178, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 23);
            this.label1.TabIndex = 4;
            this.label1.Text = "Playback speed (%):";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.playbackSpeedNud, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(312, 582);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(569, 23);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // prevFrameBtn
            // 
            this.prevFrameBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.prevFrameBtn.Enabled = false;
            this.prevFrameBtn.Location = new System.Drawing.Point(130, 582);
            this.prevFrameBtn.Name = "prevFrameBtn";
            this.prevFrameBtn.Size = new System.Drawing.Size(85, 23);
            this.prevFrameBtn.TabIndex = 7;
            this.prevFrameBtn.Text = "Previous frame";
            this.prevFrameBtn.UseVisualStyleBackColor = true;
            this.prevFrameBtn.Click += new System.EventHandler(this.prevFrameBtn_Click);
            // 
            // nextFrameBtn
            // 
            this.nextFrameBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.nextFrameBtn.Enabled = false;
            this.nextFrameBtn.Location = new System.Drawing.Point(221, 582);
            this.nextFrameBtn.Name = "nextFrameBtn";
            this.nextFrameBtn.Size = new System.Drawing.Size(85, 23);
            this.nextFrameBtn.TabIndex = 8;
            this.nextFrameBtn.Text = "Next frame";
            this.nextFrameBtn.UseVisualStyleBackColor = true;
            this.nextFrameBtn.Click += new System.EventHandler(this.nextFrameBtn_Click);
            // 
            // evalFromcsvToolStripMenuItem
            // 
            this.evalFromcsvToolStripMenuItem.BackColor = System.Drawing.Color.Yellow;
            this.evalFromcsvToolStripMenuItem.Name = "evalFromcsvToolStripMenuItem";
            this.evalFromcsvToolStripMenuItem.Size = new System.Drawing.Size(182, 20);
            this.evalFromcsvToolStripMenuItem.Text = "[DEBUG] Evaluation for .csv file";
            this.evalFromcsvToolStripMenuItem.Click += new System.EventHandler(this.evalFromcsvToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 617);
            this.Controls.Add(this.nextFrameBtn);
            this.Controls.Add(this.prevFrameBtn);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.stopButton);
            this.Controls.Add(this.playPauseButton);
            this.Controls.Add(this.playbackPictureBox);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(680, 476);
            this.Name = "MainForm";
            this.Text = "Dynamic Regions Player";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playbackPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playbackSpeedNud)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openRegionsFileToolStripMenuItem;
        private System.Windows.Forms.PictureBox playbackPictureBox;
        private System.Windows.Forms.Button playPauseButton;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.ToolStripMenuItem openVideoFileToolStripMenuItem;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.NumericUpDown playbackSpeedNud;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ToolStripMenuItem performanceEvaluationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openGroundTruthFileToolStripMenuItem;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Button prevFrameBtn;
        private System.Windows.Forms.Button nextFrameBtn;
        private System.Windows.Forms.ToolStripMenuItem evalFromcsvToolStripMenuItem;
    }
}

