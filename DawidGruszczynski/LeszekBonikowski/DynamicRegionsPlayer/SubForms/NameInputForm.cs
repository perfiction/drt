﻿using System;
using System.Windows.Forms;

namespace DynamicRegionsPlayer
{
    public partial class NameInputForm : Form
    {
        public string MethodName { get; set; }

        public NameInputForm()
        {
            InitializeComponent();
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(methodNameTb.Text)) {
                MessageBox.Show(this, "Method name cannot be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            MethodName = methodNameTb.Text;

            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
