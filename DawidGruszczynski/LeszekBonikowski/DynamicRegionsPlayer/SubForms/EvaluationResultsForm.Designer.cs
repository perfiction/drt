﻿namespace DynamicRegionsPlayer
{
    partial class EvaluationResultsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EvaluationResultsForm));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.methodName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precision50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.recall50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fMeasure50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precision75 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.recall75 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fMeasure75 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.methodName,
            this.precision50,
            this.recall50,
            this.fMeasure50,
            this.precision75,
            this.recall75,
            this.fMeasure75});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(960, 540);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.SizeChanged += new System.EventHandler(this.dataGridView1_SizeChanged);
            // 
            // methodName
            // 
            this.methodName.HeaderText = "Method Name";
            this.methodName.Name = "methodName";
            this.methodName.ReadOnly = true;
            // 
            // precision50
            // 
            this.precision50.HeaderText = "Precision (50% TP)";
            this.precision50.Name = "precision50";
            this.precision50.ReadOnly = true;
            // 
            // recall50
            // 
            this.recall50.HeaderText = "Recall (50% TP)";
            this.recall50.Name = "recall50";
            this.recall50.ReadOnly = true;
            // 
            // fMeasure50
            // 
            this.fMeasure50.HeaderText = "F-measure (50% TP)";
            this.fMeasure50.Name = "fMeasure50";
            this.fMeasure50.ReadOnly = true;
            // 
            // precision75
            // 
            this.precision75.HeaderText = "Precision (75% TP)";
            this.precision75.Name = "precision75";
            this.precision75.ReadOnly = true;
            // 
            // recall75
            // 
            this.recall75.HeaderText = "Recall (75% TP)";
            this.recall75.Name = "recall75";
            this.recall75.ReadOnly = true;
            // 
            // fMeasure75
            // 
            this.fMeasure75.HeaderText = "F-Measure (75% TP)";
            this.fMeasure75.Name = "fMeasure75";
            this.fMeasure75.ReadOnly = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 518);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(960, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(215, 17);
            this.toolStripStatusLabel1.Text = "Press Ctrl + S to export table to .csv file.";
            // 
            // EvaluationResultsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(960, 540);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.dataGridView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "EvaluationResultsForm";
            this.Text = "Evaluation results";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn methodName;
        private System.Windows.Forms.DataGridViewTextBoxColumn precision50;
        private System.Windows.Forms.DataGridViewTextBoxColumn recall50;
        private System.Windows.Forms.DataGridViewTextBoxColumn fMeasure50;
        private System.Windows.Forms.DataGridViewTextBoxColumn precision75;
        private System.Windows.Forms.DataGridViewTextBoxColumn recall75;
        private System.Windows.Forms.DataGridViewTextBoxColumn fMeasure75;
    }
}