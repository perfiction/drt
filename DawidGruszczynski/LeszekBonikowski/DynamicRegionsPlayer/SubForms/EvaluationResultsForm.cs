﻿using DynamicRegionsPlayer.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DynamicRegionsPlayer
{
    public partial class EvaluationResultsForm : Form
    {
        public EvaluationResultsForm(Dictionary<int, string> methodNames, List<(float, float, float)> results50, List<(float, float, float)> results75)
        {
            InitializeComponent();

            dataGridView1.Enabled = false;
            dataGridView1.DefaultCellStyle.SelectionBackColor = dataGridView1.DefaultCellStyle.BackColor;
            dataGridView1.DefaultCellStyle.SelectionForeColor = dataGridView1.DefaultCellStyle.ForeColor;

            dataGridView1_SizeChanged(dataGridView1, EventArgs.Empty);
            LoadData(methodNames, results50, results75);
        }

        private void LoadData(Dictionary<int, string> methodNames, List<(float precision, float recall, float fMeasure)> results50, List<(float precision, float recall, float fMeasure)> results75)
        {
            for (int i = 0; i < methodNames.Count; i++)
            {
                dataGridView1.Rows.Add(methodNames[i], results50[i].precision, results50[i].recall, results50[i].fMeasure, results75[i].precision, results75[i].recall, results75[i].fMeasure);
            }

            SetColors();
        }

        private void dataGridView1_SizeChanged(object sender, EventArgs e)
        {
            int columns = dataGridView1.Columns.Count;
            for (int i = 0; i < columns; i++)
            {
                dataGridView1.Columns[i].Width = dataGridView1.Width / columns;
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Control | Keys.S))
            {
                SaveFileDialog saveDialog = new SaveFileDialog();
                saveDialog.Filter = ".csv file|*.csv|Text file|*.txt|All files (*.*)|*.*";

                if (saveDialog.ShowDialog() == DialogResult.OK)
                {
                    CultureInfo culture = CultureInfo.CurrentCulture;
                    CultureInfo.CurrentCulture = CultureInfo.InvariantCulture;

                    var sb = new StringBuilder();

                    var headers = dataGridView1.Columns.Cast<DataGridViewColumn>();
                    sb.AppendLine(string.Join(";", headers.Select(column => column.HeaderText).ToArray()));

                    foreach (DataGridViewRow row in dataGridView1.Rows)
                    {
                        var cells = row.Cells.Cast<DataGridViewCell>();
                        sb.AppendLine(string.Join(";", cells.Select(cell => cell.Value).ToArray()));
                    }

                    using (StreamWriter sw = new StreamWriter(saveDialog.FileName))
                    {
                        sw.Write(sb.ToString());
                    }

                    CultureInfo.CurrentCulture = culture;
                }
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void SetColors()
        {
            Color c;

            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                float fMeasure50 = (float)dataGridView1.Rows[i].Cells[3].Value;
                c = GetColor(fMeasure50);
                dataGridView1.Rows[i].Cells[1].Style.BackColor = c;
                dataGridView1.Rows[i].Cells[2].Style.BackColor = c;
                dataGridView1.Rows[i].Cells[3].Style.BackColor = c;

                float fMeasure75 = (float)dataGridView1.Rows[i].Cells[6].Value;
                c = GetColor(fMeasure75);
                dataGridView1.Rows[i].Cells[4].Style.BackColor = c;
                dataGridView1.Rows[i].Cells[5].Style.BackColor = c;
                dataGridView1.Rows[i].Cells[6].Style.BackColor = c;
            }
        }

        private static Color GetColor(float fMeasure)
        {
            if (fMeasure < 0.5) return Color.Tomato;
            if (fMeasure < 0.625) return Color.Orange;
            if (fMeasure < 0.75) return Color.Yellow;
            if (fMeasure < 0.875) return Color.GreenYellow;
            return Color.LimeGreen;
        }
    }
}
