﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Emgu.CV;
using DynamicRegionsPlayer.Helpers;
using System.Globalization;
using System.IO;

namespace DynamicRegionsPlayer
{
    public partial class MainForm : Form
    {
        List<RectangleF>[] groundTruth;
        List<List<RectangleF>[]> boxes;
        Dictionary<int, string> names;
        Dictionary<int, Color> colors;
        VideoCapture video;
        int videoFrameCount;
        int playerTick;
        int frameNum = 0;
        float scaleX, scaleY;
        Bitmap scaledImage;
        Font drawFont = new Font("Arial", 10, FontStyle.Bold);

        public MainForm()
        {
            InitializeComponent();

            boxes = new List<List<RectangleF>[]>();
            names = new Dictionary<int, string>();
            colors = new Dictionary<int, Color>();
        }

        private void playPauseButton_Click(object sender, EventArgs e)
        {
            stopButton.Enabled = true;
            if (!timer1.Enabled)
            {
                playPauseButton.Text = "Pause";
                timer1.Start();
                prevFrameBtn.Enabled = true;
            }
            else
            {
                playPauseButton.Text = "Play";
                timer1.Stop();
            }
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled)
                playPauseButton_Click(playPauseButton, EventArgs.Empty);
            else
                playPauseButton.Text = "Play";

            frameNum = 0;
            video.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.PosFrames, 0);

            timer1_Tick(timer1, null);

            stopButton.Enabled = false;
        }

        private void prevFrameBtn_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled) playPauseButton_Click(playPauseButton, EventArgs.Empty);

            frameNum--;
            video.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.PosFrames, frameNum);
            timer1_Tick(timer1, null);

            if (frameNum == 0)
                prevFrameBtn.Enabled = false;
            if (!nextFrameBtn.Enabled)
                nextFrameBtn.Enabled = true;
        }

        private void nextFrameBtn_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled) playPauseButton_Click(playPauseButton, EventArgs.Empty);

            frameNum++;
            video.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.PosFrames, frameNum);
            timer1_Tick(timer1, null);

            if (frameNum == videoFrameCount - 1)
                nextFrameBtn.Enabled = false;
            if (!prevFrameBtn.Enabled)
                prevFrameBtn.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (scaledImage != null) scaledImage.Dispose();
            Mat frame = video.QueryFrame();

            if (frame == null)
                stopButton_Click(stopButton, EventArgs.Empty);
            else
            {
                if (e != null)
                    frameNum++;

                if (frameNum == videoFrameCount) stopButton_Click(stopButton, EventArgs.Empty);

                if (playbackPictureBox.Width == 0 && playbackPictureBox.Height == 0)
                    return; // zminimalizowane okno - nie wyswietlaj obrazu

                scaledImage = new Bitmap(frame.Bitmap, playbackPictureBox.Width, playbackPictureBox.Height);
                playbackPictureBox.Image = scaledImage;

                frame.Bitmap.Dispose();
                frame.Dispose();
            }
        }

        private void playbackPictureBox_SizeChanged(object sender, EventArgs e)
        {
            if (video != null)
            {
                scaleX = playbackPictureBox.Width / (float)video.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth);
                scaleY = playbackPictureBox.Height / (float)video.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight);
            }
        }

        private void playbackSpeedNud_ValueChanged(object sender, EventArgs e)
        {
            if (video == null) return;
            timer1.Interval = (int)(playerTick / ((float)playbackSpeedNud.Value / 100f));
        }

        private void playbackPictureBox_Paint(object sender, PaintEventArgs e)
        {
            if (groundTruth != null)
            {
                foreach (var gt in groundTruth)
                {
                    var scaledGtBox = new RectangleF(gt[frameNum].X * scaleX, gt[frameNum].Y * scaleY, gt[frameNum].Width * scaleX, gt[frameNum].Height * scaleY);
                    e.Graphics.DrawRectangles(new Pen(Brushes.Yellow, 2.0f), new[] { scaledGtBox });
                    e.Graphics.DrawString("GT", drawFont, Brushes.Yellow, new PointF(scaledGtBox.X, scaledGtBox.Y - drawFont.Height));
                }
            }

            if (boxes != null && boxes.Count > 0)
            {
                for (int i = 0; i < boxes.Count; i++)
                {
                    foreach (var objBox in boxes[i])
                    {
                        Brush brush = new SolidBrush(colors[i]);
                        var scaledBox = new RectangleF(objBox[frameNum].X * scaleX, objBox[frameNum].Y * scaleY, objBox[frameNum].Width * scaleX, objBox[frameNum].Height * scaleY);
                        e.Graphics.DrawRectangles(new Pen(brush, 2.0f), new[] { scaledBox });
                        e.Graphics.DrawString(names[i], drawFont, brush, new PointF(scaledBox.X, scaledBox.Y - drawFont.Height));
                    }
                }
            }
        }

        private void openVideoFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Open video file";
            openFileDialog.Filter = "Video files|*.avi;*.mkv;*.3g2;*.3gp;*.asf;*.asx;*.flv;*.mov;*.mp4;*.mpg;*.rm;*.swf;*.vob;*.wma;*.wmv;*.m2ts|All files (*.*)|*.*";
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (names != null) names.Clear();
                if (colors != null) colors.Clear();
                if (boxes != null) boxes.Clear();
                if (video != null) video.Dispose();

                video = new VideoCapture(openFileDialog.FileName);
                videoFrameCount = (int)video.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameCount);
                timer1.Interval = playerTick = 1000 / (int)video.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.Fps);

                playPauseButton.Enabled = true;
                openVideoFileToolStripMenuItem.Checked = true;
                openGroundTruthFileToolStripMenuItem.Enabled = true;
                openRegionsFileToolStripMenuItem.Enabled = true;
                openRegionsFileToolStripMenuItem.Checked = false;
                openGroundTruthFileToolStripMenuItem.Checked = false;
                performanceEvaluationToolStripMenuItem.Enabled = false;

                playbackPictureBox_SizeChanged(playbackPictureBox, EventArgs.Empty);

                if (MessageBox.Show(this, "Do you want to open ground truth file for \"" + openFileDialog.SafeFileName + "\"?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    openGroundTruthFileToolStripMenuItem_Click(openGroundTruthFileToolStripMenuItem, EventArgs.Empty);
                }
            }
        }

        private void openGroundTruthFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = ("Open ground truth file");
            openFileDialog.Filter = "Region files|*.csv;*.txt|All files (*.*)|*.*";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    groundTruth = FileHelper.LoadCoordinates(openFileDialog.FileName);
                }
                catch(Exception exc)
                {
                    MessageBox.Show(this, exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (videoFrameCount != groundTruth[0].Count)
                {
                    MessageBox.Show(this, "Ground truth file contains data about " + groundTruth[0].Count + " frames while selected video has " + videoFrameCount + " frames.\n\nMake sure you selected proper file.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                openGroundTruthFileToolStripMenuItem.Checked = true;
                if (openRegionsFileToolStripMenuItem.Checked) performanceEvaluationToolStripMenuItem.Enabled = true;
                if(boxes.Count != 0) return;

                if (MessageBox.Show(this, "Do you want to open regions file for \"" + openFileDialog.SafeFileName + "\"?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    openRegionsFileToolStripMenuItem_Click(openRegionsFileToolStripMenuItem, EventArgs.Empty);
                }
            }
        }

        private void openRegionsFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Open regions file";
            openFileDialog.Filter = "Region files|*.csv;*.txt|All files (*.*)|*.*";
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                List<RectangleF>[] loaded;

                try
                {
                     loaded = FileHelper.LoadCoordinates(openFileDialog.FileName);
                }
                catch(Exception exc)
                {
                    MessageBox.Show(this, exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                while(loaded[0].Count < videoFrameCount)
                    loaded[0].Add(new RectangleF(0, 0, 0, 0));

                if (videoFrameCount != loaded[0].Count)
                {
                    MessageBox.Show(this, "Regions file contains data about " + loaded[0].Count + " frames while selected video has " + videoFrameCount + " frames.\n\nMake sure you selected proper file.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (groundTruth != null && groundTruth.Length != loaded.Length)
                {
                    MessageBox.Show(this, "Regions file contains data about " + loaded.Length + " objects while ground truth contains " + groundTruth.Length + ".\n\nMake sure you selected proper file.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                NameInputForm nameInputForm = new NameInputForm();
                nameInputForm.StartPosition = FormStartPosition.CenterScreen;
                if (nameInputForm.ShowDialog() != DialogResult.OK)
                    return;

                names[boxes.Count] = nameInputForm.MethodName;


                if (colorDialog1.ShowDialog() != DialogResult.OK)
                    return;

                colors[boxes.Count] = colorDialog1.Color;

                boxes.Add(loaded);

                openRegionsFileToolStripMenuItem.Checked = true;
                if (openGroundTruthFileToolStripMenuItem.Checked) performanceEvaluationToolStripMenuItem.Enabled = true;
                playPauseButton.Enabled = true;
                nextFrameBtn.Enabled = true;

                if (MessageBox.Show(this, "Do you want to open another regions file?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    openRegionsFileToolStripMenuItem_Click(openRegionsFileToolStripMenuItem, EventArgs.Empty);
                }
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (timer1.Enabled)
                timer1.Stop();
        }

        private void evalFromcsvToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();

            if(openDialog.ShowDialog() == DialogResult.OK)
            {
                CultureInfo culture = CultureInfo.CurrentCulture;
                CultureInfo.CurrentCulture = CultureInfo.InvariantCulture;

                List<(float, float, float)> results50 = new List<(float, float, float)>();
                List<(float, float, float)> results75 = new List<(float, float, float)>();

                using(StreamReader sr = new StreamReader(openDialog.FileName))
                {
                    sr.ReadLine(); // header
                    names = new Dictionary<int, string>();
                    string line;
                    int i = 0;
                    while((line = sr.ReadLine()) != null)
                    {
                        string[] split = line.Split(';');
                        names.Add(i, split[0]);
                        results50.Add((float.Parse(split[1]), float.Parse(split[2]), float.Parse(split[3])));
                        results75.Add((float.Parse(split[4]), float.Parse(split[5]), float.Parse(split[6])));
                        i++;
                    }
                }

                CultureInfo.CurrentCulture = culture;

                EvaluationResultsForm resultsForm = new EvaluationResultsForm(names, results50, results75);
                resultsForm.ShowDialog();
            }
        }

        private void performanceEvaluationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (groundTruth != null && boxes != null)
            {
                List<(float precision, float recall, float fMeasure)> results50 = EvaluationHelper.Evaluate(boxes, groundTruth, 0.5f);
                List<(float precision, float recall, float fMeasure)> results75 = EvaluationHelper.Evaluate(boxes, groundTruth, 0.75f);
                EvaluationResultsForm resultsForm = new EvaluationResultsForm(names, results50, results75);
                resultsForm.ShowDialog();
            }
            else
            {
                MessageBox.Show(this, "Evaluation requires both regions file and ground truth file to be loaded.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
