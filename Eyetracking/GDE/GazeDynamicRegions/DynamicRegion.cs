﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLab.Eyetracking.Data.DynamicRegions
{
    using GameLab.Eyetracking.Geometry;

    public class DynamicRegionFrame
    {
        public Point CenterPosition;
        public Size Size;

        public DynamicRegionFrame(Point centerPosition, Size size)
        {
            this.CenterPosition = centerPosition;
            this.Size = size;
        }

        public long StartTimeMicroseconds { get; protected set; }
        public long EndTimeMicroseconds { get; protected set; }
        public long DwellTimeMicroseconds
        {
            get
            {
                return EndTimeMicroseconds - StartTimeMicroseconds;
            }
        }

        public void SetInterval(long startTimeMicroseconds, long endTimeMicroseconds)
        {
            this.StartTimeMicroseconds = startTimeMicroseconds;
            this.EndTimeMicroseconds = endTimeMicroseconds;
        }

        public Rectangle Border
        {
            get
            {
                Size halfSize = new Size((int)(Size.Width / 2.0), (int)(Size.Height / 2.0));
                Point położenieLewegoGórnegoRogu = new Point(CenterPosition.X - halfSize.Width, CenterPosition.Y - halfSize.Height);
                return new Rectangle(położenieLewegoGórnegoRogu, Size);
            }
        }
    }

    public abstract class DynamicRegion
    {
        public string Name { get; protected set; }
                
        public DynamicRegionFrame[] Frames;

        public DynamicRegion(string name, DynamicRegionFrame[] frames)
        {
            this.Name = name;
            this.Frames = frames;
        }

        protected DynamicRegionFrame getFrame(long timeMs)
        {
            //TODO: tu można bardzo przyspieszyć wyznaczając przybliżoną klatkę na bazie fps
            //TODO: fps można policzyć z czasu ostatniej klamki podzielonej przez liczbę klatek
            return Frames.Where(f => timeMs >= f.StartTimeMicroseconds && timeMs <= f.EndTimeMicroseconds).First();
        }

        public abstract bool IsPointInsideRegion(Point point, long timeMs);

        public Rectangle GetCurrentBorder(long timeMs)
        {
            DynamicRegionFrame frame = getFrame(timeMs);
            return frame.Border;
        }

        public abstract double GetCurrentArea(long timeMs);

        public override string ToString()
        {
            return Name + ", frames: " + Frames.Length;
        }
    }

    public class RectangularDynamicRegion : DynamicRegion
    {
        public RectangularDynamicRegion(string name, DynamicRegionFrame[] frames)
            : base(name, frames)
        { }

        public override bool IsPointInsideRegion(Point point, long timeMs)
        {
            DynamicRegionFrame frame = getFrame(timeMs);
            Point centerPosition = frame.CenterPosition;
            Size size = frame.Size;
            Size halfSize = new Size(size.Width / 2, size.Height / 2);
            return (
                (point.X >= centerPosition.X - halfSize.Width) && (point.X <= centerPosition.X + halfSize.Width) &&
                (point.Y >= centerPosition.Y - halfSize.Height) && (point.Y <= centerPosition.Y + halfSize.Height));
        }

        public override double GetCurrentArea(long timeMs)
        {
            DynamicRegionFrame frame = getFrame(timeMs);            
            Size size = frame.Size;
            return size.Width * size.Height;
        }
    }
}
