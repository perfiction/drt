﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace GameLab.Eyetracking.Data.DynamicRegions
{
    using JacekMatulewski.Csv;

    public static class GazeDynamicRegionsHelper
    {
        public static List<DynamicRegion> LoadDynamicRegionsFromFile(string dynamicRegionsFilePath, char separator, ref string message, IFormatProvider formatProvider)
        {
            List<DynamicRegion> regions = new List<DynamicRegion>();
            try
            {
                string extension = Path.GetExtension(dynamicRegionsFilePath);
                switch (extension)
                {
                    default:
                        throw new ArgumentException("Unrecognized file type (extension: " + extension + ")");
                    //MessageBox.Show("Unrecognized file type (extension: " + extension + ")", Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //break;
                    case ".csv":
                        CsvDocument<CsvDynamicRegionFrameRecord> csvDynamicRegionFrames = CsvDocument<CsvDynamicRegionFrameRecord>.Load(dynamicRegionsFilePath, separator, formatProvider: formatProvider);
                        List<CsvDynamicRegionFrameRecord> frameList = csvDynamicRegionFrames.ToList();
                        
                        Dictionary<string, List<DynamicRegionFrame>> regionFrames = new Dictionary<string, List<DynamicRegionFrame>>();
                        foreach(CsvDynamicRegionFrameRecord frame in frameList)
                        {
                            if(!regionFrames.ContainsKey(frame.DynamicRegionName)) regionFrames.Add(frame.DynamicRegionName, new List<DynamicRegionFrame>());
                            regionFrames[frame.DynamicRegionName].Add(frame.DynamicRegionFrame);
                        }

                        foreach(string regionName in regionFrames.Keys)
                        {
                            DynamicRegionFrame[] regionFrameArray = regionFrames[regionName].ToArray();
                            DynamicRegion dynamicRegion = new RectangularDynamicRegion(regionName, regionFrameArray);
                            regions.Add(dynamicRegion);
                        }
                        break;
                }
                return regions;
            }
            catch (Exception exc)
            {
                message = exc.Message;
                return null;
            }
        }

        public static void SaveDynamicRegionsToFile(string dynamicRegionsFilePath, char separator, List<DynamicRegion> dynamicRegions, IFormatProvider formatProvider)
        {
            List<CsvDynamicRegionFrameRecord> csvDynamicRegionFrameList = new List<CsvDynamicRegionFrameRecord>();
            foreach (DynamicRegion dynamicRegion in dynamicRegions)
            {
                foreach (DynamicRegionFrame frame in dynamicRegion.Frames)
                {
                    csvDynamicRegionFrameList.Add(new CsvDynamicRegionFrameRecord(dynamicRegion.Name, frame));
                }
            }

            CsvDocument<CsvDynamicRegionFrameRecord> csvDynamicRegionsFrames = new CsvDocument<CsvDynamicRegionFrameRecord>(csvDynamicRegionFrameList, formatProvider: formatProvider);
            csvDynamicRegionsFrames.SaveAs(dynamicRegionsFilePath, separator, "Dynamic Regions (DAOIs) from Gaze Data Explorer, exported " + DateTime.Now.ToString(), CsvDynamicRegionFrameRecord.ColumnNames);
        }

        public static List<DynamicRegion> ImportDynamicRegionsFromTracker(string[] filePaths, char separator, ref string message, IFormatProvider formatProvider)
        {
            try
            {
                List<DynamicRegion> dynamicRegions = new List<DynamicRegion>();
                foreach(string filePath in filePaths)
                {
                    string regionName = Path.GetFileNameWithoutExtension(Path.GetFileName(filePath));
                    string[] lines = File.ReadAllLines(filePath);
                    List<DynamicRegionFrame> regionFrames = new List<DynamicRegionFrame>();
                    foreach(string line in lines)
                    {
                        if(line[0] != '#')
                        {
                            string[] values = line.Split(separator);
                            double startTime = double.Parse(values[0].Trim(), formatProvider);
                            double frameIndex = int.Parse(values[1].Trim(), formatProvider);
                            int left = int.Parse(values[2].Trim(), formatProvider);
                            int top = int.Parse(values[3].Trim(), formatProvider);
                            int width = int.Parse(values[4].Trim(), formatProvider);
                            int height = int.Parse(values[5].Trim(), formatProvider);
                            DynamicRegionFrame frame = new DynamicRegionFrame(new Geometry.Point(left + width / 2, top + height / 2), new Geometry.Size(width, height));
                            regionFrames.Add(frame);
                        }
                    }
                    DynamicRegion dynamicRegion = new RectangularDynamicRegion(regionName, regionFrames.ToArray());
                    dynamicRegions.Add(dynamicRegion);
                }
                return dynamicRegions;
            }
            catch (Exception exc)
            {
                message = exc.Message;
                return null;
            }
        }
    }
}
