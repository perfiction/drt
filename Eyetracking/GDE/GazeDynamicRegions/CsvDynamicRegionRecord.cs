﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLab.Eyetracking.Data.DynamicRegions
{
    using JacekMatulewski.Csv;
    using GameLab.Eyetracking.Geometry;

    class CsvDynamicRegionFrameRecord : ICsvRecord
    {
        public static readonly string ColumnNames = "name; shape; left; top; width; height; startTime; endTime";

        public string DynamicRegionName { get; set; }
        public DynamicRegionFrame DynamicRegionFrame { get; set; }

        public CsvDynamicRegionFrameRecord()
        {
            this.DynamicRegionName = null;
            this.DynamicRegionFrame = null;
        }

        public CsvDynamicRegionFrameRecord(string dynamicRegionName, DynamicRegionFrame dynamicRegionFrame)
        {
            this.DynamicRegionName = dynamicRegionName;
            this.DynamicRegionFrame = dynamicRegionFrame;
        }

        public void ParseValues(string[] values, CsvRecordParam param, IFormatProvider formatProvider)
        {
            string name = values[0];
            DynamicRegionName = name;

            int shape = int.Parse(values[1], formatProvider); //TODO: zapisuję na przyszłość, ale obsługuję tylko Rectangle
            int left = int.Parse(values[2], formatProvider);
            int top = int.Parse(values[3], formatProvider);
            int width = int.Parse(values[4], formatProvider);
            int height = int.Parse(values[5], formatProvider);
            Point centerPosition = new Point(left + width / 2, top + height / 2);
            Size size = new Size(width, height);

            DynamicRegionFrame = new DynamicRegionFrame(centerPosition, size); //TODO: tu mógłby być switch na kształt

            long startTimeMicroseconds = long.Parse(values[6], formatProvider);
            long endTimeMicroseconds = long.Parse(values[7], formatProvider);
            DynamicRegionFrame.SetInterval(startTimeMicroseconds, endTimeMicroseconds);
        }

        public string[] ToValues(CsvRecordParam param, IFormatProvider formatProvider)
        {
            int shape = 0;
            Rectangle border = DynamicRegionFrame.Border;

            string[] values = new string[9];
            values[0] = DynamicRegionName;
            values[1] = shape.ToString(formatProvider);
            values[2] = border.Left.ToString(formatProvider);
            values[3] = border.Top.ToString(formatProvider);
            values[4] = border.Width.ToString(formatProvider);
            values[5] = border.Height.ToString(formatProvider);            
            values[6] = DynamicRegionFrame.StartTimeMicroseconds.ToString(formatProvider);
            values[7] = DynamicRegionFrame.EndTimeMicroseconds.ToString(formatProvider);
            return values;
        }
    }
}
