﻿using System;
using System.Collections.Generic;
using System.Text;

//Powtórzenie kodu - to jest tłumaczenie pliku Bianka\Bianka.Elementy\ProstaGeometria.cs
//TODO: dlaczego nie jest używany Gamelab.Core z przestrzenią GameLab.Geometry;
namespace GameLab.Eyetracking.Geometry
{
    public struct Point
    {
        public int X, Y;

        public Point(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public static Point operator *(int s, Point p1)
        {
            return new Point(s * p1.X, s * p1.Y);
        }

        public static Point operator /(Point p1, int s)
        {
            return new Point(p1.X / s, p1.Y / s);
        }

        public static Point operator+(Point p1, Point p2)
        {
            return new Point(p1.X + p2.X, p1.Y + p2.Y);
        }

        public static Point operator-(Point p1, Point p2)
        {
            return new Point(p1.X - p2.X, p1.Y - p2.Y);
        }

        public static Point operator+(Point p, Size r)
        {
            return new Point(p.X + r.Width, p.Y + r.Height);
        }

        public static Point operator-(Point p, Size r)
        {
            return new Point(p.X - r.Width, p.Y - r.Height);
        }

        public static bool operator==(Point p1, Point p2)
        {
            return (p1.X == p2.X) && (p1.Y == p2.Y);
        }

        public static bool operator !=(Point p1, Point p2)
        {
            return !(p1==p2);
        }

        public override bool Equals(object obj)
        {
            return obj is Point && this == (Point)obj;
        }

        public override int GetHashCode()
        {
            return X.GetHashCode() ^ Y.GetHashCode();
        }

        public override string ToString()
        {
            return "(" + X.ToString() + "," + Y.ToString() + ")";
        }

        public static readonly Point Zero;
    }

    public struct PointF
    {
        public float X, Y;

        public PointF(float x, float y)
        {
            this.X = x;
            this.Y = y;
        }

        public static PointF operator+(PointF p1, PointF p2)
        {
            return new PointF(p1.X + p2.X, p1.Y + p2.Y);
        }

        public static PointF operator-(PointF p1, PointF p2)
        {
            return new PointF(p1.X - p2.X, p1.Y - p2.Y);
        }

        public static PointF operator+(PointF p, Size r)
        {
            return new PointF(p.X + r.Width, p.Y + r.Height);
        }

        public static PointF operator-(PointF p, Size r)
        {
            return new PointF(p.X - r.Width, p.Y - r.Height);
        }
    }

    public struct PointD
    {
        public double X, Y;

        public PointD(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        public static PointD operator +(PointD p1, PointD p2)
        {
            return new PointD(p1.X + p2.X, p1.Y + p2.Y);
        }

        public static PointD operator -(PointD p1, PointD p2)
        {
            return new PointD(p1.X - p2.X, p1.Y - p2.Y);
        }

        public static PointD operator +(PointD p, Size r)
        {
            return new PointD(p.X + r.Width, p.Y + r.Height);
        }

        public static PointD operator -(PointD p, Size r)
        {
            return new PointD(p.X - r.Width, p.Y - r.Height);
        }

        public static PointD operator /(PointD p, double s)
        {
            return new PointD(p.X / s, p.Y / s);
        }

        public double Length
        {
            get
            {
                return Math.Sqrt(X * X + Y * Y);
            }
        }
    }

    public struct Size
    {
        public int Width, Height;

        public Size(int width, int height)
        {
            this.Width = width;
            this.Height = height;
        }

        public static Size operator/(Size size, float divisor)
        {
            int width = (int)Math.Round(size.Width/divisor);
            int height = (int)Math.Round(size.Height/divisor);
            return new Size(width, height);
        }

        public override string ToString()
        {
            return "[" + Width.ToString() + "," + Height.ToString() + "]";
        }
    }

    public struct Rectangle
    {
        public Point Position;
        public Size Size;

        public Rectangle(Point position, Size size)
        {
            this.Position = position;
            this.Size = size;
        }

        public Rectangle(int left, int top, int right, int bottom)
            :this(new Point(left, top), new Size(right-left, Math.Abs(bottom - top))) //abs -> nie ma znaczenia w którą stronę jest oś Y
        {
        }

        public int Left
        {
            get
            {
                return Position.X;
            }
        }

        public int Right
        {
            get
            {
                return Position.X + Size.Width;
            }
        }

        public int Top
        {
            get
            {
                return Position.Y;
            }
        }

        public int Bottom
        {
            get
            {
                return Position.Y + Size.Height;
            }
        }

        public int Width
        {
            get
            {
                return Size.Width;
            }
        }

        public int Height
        {
            get
            {
                return Size.Height;
            }
        }

        public override string ToString()
        {
            return "(x: " + Left + ", y: " + Top + ", w: " + Size.Width + ", h: " + Size.Height + ")";
        } 
    }
}